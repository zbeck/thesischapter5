# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 20:04:53 2015

@author: Zoli
"""
from constants import FW_PLAN_TURN_RADIUS, EPSILON
from agent import angle_rng_pi, dist
from fixedsimulator import follow_points, FixedWing
import numpy as np
import scipy.io

class NoSweepPlanner:
    def __init__(self, configurations, areaSize, areaOffset = np.array([0., 0.])):
        self.configurations = configurations
        self.areaSize = float(areaSize)
        self.areaOffset = np.asarray(areaOffset, np.float)    
        
    def cellDistance(self, diff):
        return np.sqrt(diff[0]**2 + diff[1]**2) * self.areaSize
    
    def agentDistance(self, agentPos, areaPos):
        """Gives a distance estimate for an agent to approach a given cell.
        
        Parameters
        ----------
        agentPos: np.ndarray of float [x,y]
            agent position global cartesian coordinates
        areaPos: np.ndarray of int [x,y]
            target area position in cell coordinates
            
        Returns
        -------
        double distance estimate"""
        agentCoord = (np.asarray(agentPos, np.float) - self.areaOffset)
        diff = agentCoord - areaPos*self.areaSize
        return np.sqrt(diff[0]**2 + diff[1]**2)
        
    def nextSweepTile(self, shortPlan, startPos):
        """Tells the points of the next sweep according to a short term plan
        
        Returns
        -------
        [[x,y,heading], ...]
            a list of the points of the next area sweep"""
        plan = np.asarray(shortPlan).reshape(-1,2)
        startPos = np.asarray(startPos)
        endPos = self.cellCoord2World(plan[0])
        best_config = self.findBestConfig(plan, startPos)
        endHeading = self.configurations[best_config[0]]
        return list([np.concatenate((endPos, [endHeading]))])
        
    def findBestConfig(self, shortPlan, startPos):
        optimisedepth = min(3, len(shortPlan))
#        print(shortPlan.shape, optimisedepth)
        if optimisedepth < 1:
            return []
        targets = [self.cellCoord2World(p) for p in shortPlan]
        possibilities = np.empty([len(self.configurations)]*optimisedepth, np.float)
        it = np.nditer(possibilities, flags=['multi_index'], op_flags=['writeonly'])
        while not it.finished:
            endPos = np.concatenate((targets[0], [self.configurations[it.multi_index[0]]]))
            _, path_len = FixedWing.navigateTo(startPos, endPos)
            for i in range(len(it.multi_index)-1):
                sp = np.concatenate((targets[i], [self.configurations[it.multi_index[i]]]))
                ep = np.concatenate((targets[i+1], [self.configurations[it.multi_index[i+1]]]))
                _, pl = FixedWing.navigateTo(sp, ep)
                path_len += pl
            it[0] = path_len
            it.iternext()
        best_config = np.argmin(possibilities)
        best_config = np.unravel_index(best_config, possibilities.shape)
#        print(possibilities.shape)
#        print(best_config)
        return best_config
        
    def getSweepLen(self):
        return 0.
        
    def cellCoord2World(self, cellcoord):
        return np.asarray(cellcoord, np.float)*self.areaSize + self.areaOffset

class SweepPlanner:
    def __init__(self, areaSize, maxSpacing, nSections, fullCoverage = True, areaOffset = np.array([0., 0.])):
        """SweepPlanner class constructor. Creates a class responsible for coming up with sweep paths in given rectangular areas.
        
        Parameters
        ----------
        areaSize: double
            the size of a search area (areaSize x areaSize rectangles)
        maxSpacing: double
            maximum spacing between sweep paths, the real spacing can be read from the SweepPlanner.range property that is half the spacing
        nSections: int
            number of waypoints in the straight part of the sweep
        fullCoverage: bool
            if true, it will create an optimised patter in a way that every point of the search rectangle
            is within SweepPlanner.range at some point of the sweep path, default value is True
        areaOffset: np.ndarray of float [x,y]
            the centerpoint of the 0,0 cell in global cartesian coordinates, default value is [0,0]"""
        areaSize = float(areaSize)
        self.areaSize = areaSize
        if maxSpacing < FW_PLAN_TURN_RADIUS:
            raise SweepPlanner.spacingException
        npaths = np.ceil(areaSize/maxSpacing)
        if npaths % 2 == 0:
            npaths += 1
        self.range = areaSize/npaths/2
        self.initTurn(fullCoverage, maxSpacing)
        self.initSweep(areaSize, npaths, nSections)
        self.initPatterns()
        self.areaOffset = np.asarray(areaOffset, np.float)
        
    def initTurn(self, fullCoverage, maxSpacing):
        if not fullCoverage:    #shortest turn
            if self.range > FW_PLAN_TURN_RADIUS:
                self.turnPrimitive90 = self.PathPrimitive([[0,0,0], [self.range-FW_PLAN_TURN_RADIUS,0,0], [self.range, self.range, np.pi/2]])
                self.turnPrimitive180 = self.turnPrimitive90 + (self.turnPrimitive90.rotate(1) + [self.range, self.range])
            else:
                start = [0,0,0]
                end90 = [self.range, self.range, np.pi/2]
                end180 = [0, 2*self.range, np.pi]
                turnpoints = np.concatenate(([start], FixedWing.navigateTo(start, end90)[0], [end90]))
                self.turnPrimitive90 = self.PathPrimitive(turnpoints)
                turnpoints = np.concatenate(([start], FixedWing.navigateTo(start, end180)[0], [end180]))
                self.turnPrimitive180 = self.PathPrimitive(turnpoints)
        else:   #covering corners turn
            if maxSpacing < FW_PLAN_TURN_RADIUS:
                print('WANRING! Full coverage is not ensured for sweep search.')
            forwardTurn = self.optimiseTurn([[2*self.range, -self.range, np.pi/4], [(2 - 1/np.sqrt(2))*self.range, (-1 + 1/np.sqrt(2))*self.range, np.pi/4]])
            reversemax = max(self.range, FW_PLAN_TURN_RADIUS*(1+1/np.sqrt(2)))
            reverseTurn = self.optimiseTurn([[self.range +reversemax, -reversemax, -np.pi*3/4], [(2 - 1/np.sqrt(2))*self.range, (-1 + 1/np.sqrt(2))*self.range, -np.pi*3/4]])
            turnEnd = [self.range, self.range, np.pi/2]
            if reverseTurn[1] < forwardTurn[1]:
                turnpoints = np.concatenate((reverseTurn[0], FixedWing.navigateTo(reverseTurn[0][-1], turnEnd)[0], [turnEnd]))
            else:
                turnpoints = np.concatenate((forwardTurn[0], FixedWing.navigateTo(forwardTurn[0][-1], turnEnd)[0], [turnEnd]))
            tp = self.PathPrimitive(turnpoints)
            #now tp is 90° turn
            self.turnPrimitive90 = tp
            self.turnPrimitive180 = self.turnPrimitive90 + (self.turnPrimitive90.rotate(1) + [self.range, self.range])
        
    def initSweep(self, areasize, npaths, nsections):
        shift = np.array([-areasize/2, -areasize/2], np.float) 
        self.acrossPattern = self.makeSweepPath(areasize, npaths, nsections)+shift
        #path to turn back to corner 1 from corner 2
        turndown = (self.turnPrimitive90.mirror(True)+[areasize-2*self.range, areasize-self.range])
        godown = self.PathPrimitive(np.linspace(0, -areasize+4*self.range, nsections)[:,np.newaxis] * [0,1,0] + [0,0,-np.pi/2]) + [areasize-self.range, areasize-2*self.range]
        end = self.PathPrimitive([[areasize-self.range, 2*self.range, -np.pi/2], [areasize-self.range, 0, -np.pi/2]])
        self.nearPattern = (self.makeSweepPath(areasize-2*self.range, npaths, nsections) + turndown + godown + end)+shift
        
    def initPatterns(self):
        self.tileMatrix = [[self.getTile(i,j) for j in range(4)] for i in range(4)]
        
    def getSweepLen(self):
        return sum(self.acrossPattern.get_route_length() + self.nearPattern.get_route_length())/2
        
    def cellDistance(self, diff):
        return np.sqrt(min(abs(abs(diff[0])-1), abs(diff[0]))**2 + min(abs(abs(diff[1])-1), abs(diff[1]))**2) * self.areaSize + 2*self.range
        
    def cellCoord2World(self, cellcoord):
        return np.asarray(cellcoord)*self.areaSize + self.areaOffset
    
    def agentDistance(self, agentPos, areaPos):
        """Gives a distance estimate for an agent to approach a given cell.
        
        Parameters
        ----------
        agentPos: np.ndarray of float [x,y]
            agent position global cartesian coordinates
        areaPos: np.ndarray of int [x,y]
            target area position in cell coordinates
            
        Returns
        -------
        double distance estimate"""
        agentCoord = (np.asarray(agentPos, np.float) - self.areaOffset) / self.areaSize
        return np.sqrt((abs(agentCoord[0]-areaPos[0])-0.5)**2 + (abs(agentCoord[1]-areaPos[1])-0.5)**2) * self.areaSize
        
    def getTile(self, startCorner, endCorner):
        diff = (endCorner - startCorner)%4
        if diff==1:
            pattern = self.nearPattern
        elif diff==2:
            pattern = self.acrossPattern
        elif diff==3:
            pattern = self.nearPattern.mirror(False).rotate(-1)
        else:
            pattern = self.nearPattern
        return pattern.rotate(startCorner)
    
    def makeSweepPath(self, arealen, npaths, nsections):
        if npaths == 1:
            return self.PathPrimitive(np.linspace(0, arealen, nsections)[:,np.newaxis] * [1,0,0]) + [0.0, self.range]
        
        straightSection = self.PathPrimitive(np.linspace(0, arealen-4*self.range, nsections)[:,np.newaxis] * [1,0,0]) + [2*self.range, self.range]
        backwardStraightSection = straightSection.reverse() + [0, 2*self.range]
        turnSection = self.turnPrimitive180 + [arealen-2*self.range, self.range]
        backwardTurnSection = self.turnPrimitive180.mirror(False) + [2*self.range, 3*self.range]
        start = self.PathPrimitive([[0, self.range, 0], [2*self.range, self.range, 0]])
        end = start + [arealen-2*self.range, (-2+npaths*2)*self.range]
        p = start + straightSection
        npaths -= 1
        straightSection += [0, 4*self.range]
        while npaths > 0:
            p += turnSection + backwardStraightSection
            npaths -= 1
            p += backwardTurnSection + straightSection
            npaths -= 1
            straightSection += [0, 4*self.range]
            turnSection += [0, 4*self.range]
            backwardStraightSection += [0, 4*self.range]
            backwardTurnSection += [0, 4*self.range]
        p += end
        return p
        
    def planSweep(self, areaPoints, startPos):
        """Fills the given cells with sweep patterns and returns the full path
        
        Parameters
        ----------
        areaPoints: np.ndarray of int [[x,y], [x,y], ...]
            the coordinates of the cells on the grid
        startPos: numpy.ndarray of float [x, y, heading]
                start position of the path
                
        Returns
        -------
        SweepPlanner.PathPrimitive of the complete path from startPos sweep searching all the cells"""
        areaPoints = np.asarray(areaPoints)
        startPos = np.asarray(startPos)
        path_primitives = self._get_primitives_for_plan(areaPoints, startPos)
        path = path_primitives[0]+(areaPoints[0] * self.areaSize + self.areaOffset)
        for i in range(1,len(areaPoints)):
            path += path_primitives[i]+(areaPoints[i] * self.areaSize + self.areaOffset)
        return path.addInitialPath(startPos)
        
    def nextSweepTile(self, shortPlan, startPos):
        """Tells the points of the next sweep according to a short term plan
        
        Returns
        -------
        [[x,y,heading], ...]
            a list of the points of the next area sweep"""
        plan = np.asarray(shortPlan).reshape(-1,2)
        startPos = np.asarray(startPos)
        path_primitives = self._get_primitives_for_plan(plan, startPos)
        tile = path_primitives[0]+(plan[0] * self.areaSize + self.areaOffset)
        return list(tile.pathpoints)
        
    def _get_primitives_for_plan(self, plan, startPos):
        diff = (plan[0][0]-(startPos[0]-self.areaOffset[0])/self.areaSize, plan[0][1]-(startPos[1]-self.areaOffset[1])/self.areaSize)
        startjoint = self.getTransitionCorners(diff)
        cornerJoints = []
        for i in range(1,len(plan)):
            diff = (plan[i][0]-plan[i-1][0], plan[i][1]-plan[i-1][1])
            cornerJoints.append(self.getTransitionCorners(diff))
        path_primitives = []
        currentEntryCorner = startjoint[0][1]
        for j in cornerJoints:
            if (len(j) > 1) and (j[0][0] == currentEntryCorner):
                chosenJoint = j[1]
            else:
                chosenJoint = j[0]
            path_primitives.append(self.tileMatrix[currentEntryCorner][chosenJoint[0]])
            currentEntryCorner = chosenJoint[1]
        path_primitives.append(self.tileMatrix[currentEntryCorner][(currentEntryCorner+2)%4])
        return path_primitives
        
    @staticmethod
    def getTransitionCorners(diff):
        if abs(diff[0]) < EPSILON:
            if diff[1] > 0:
                joint = [(2,1), (3,0)]
            else:
                joint = [(1,2), (0,3)]
        elif abs(diff[1]) < EPSILON:
            if diff[0] > 0:
                joint = [(1,0), (2,3)]
            else:
                joint = [(0,1), (3,2)]
        else:
            if diff[0]>0 and diff[1]>0:
                joint = [(2,0)]
            elif diff[0]<0 and diff[1]<0:
                joint = [(0,2)]
            elif diff[0]>0 and diff[1]<0:
                joint = [(1,3)]
            else:
                joint = [(3,1)]
        return joint
    
    @staticmethod
    def optimiseTurn(posinterval, resolution=21, iterations=3):
        startpos = [0,0,0]
        pint = np.asarray(posinterval)
        intervalVector = np.array(pint[1]-pint[0], np.float)
        endposArray = pint[0] + (intervalVector * np.linspace(0, 1, resolution)[:,np.newaxis])
        possibleTurns = np.asarray([FixedWing.navigateTo(startpos, endpos) for endpos in endposArray])
        bestTurn = np.argmin(possibleTurns[:,1])
        #print(possibleTurns[:,1])
        #print(bestTurn, possibleTurns)
        if iterations > 1:
            newInterval = endposArray[[max(0,bestTurn-1), min(len(endposArray)-1, bestTurn+1)]]
            return SweepPlanner.optimiseTurn(newInterval, resolution, iterations-1)
        else:
            return (np.concatenate(([startpos], possibleTurns[bestTurn,0], [endposArray[bestTurn]])), possibleTurns[bestTurn,1])
        
    class PathPrimitive: 
        def __init__(self, pathPoints_with_headings):
            self.pathpoints = self.range_headings(pathPoints_with_headings)
            self.startPos = self.pathpoints[0]
            self.endPos = self.pathpoints[-1]
            
        @staticmethod
        def range_headings(pathPoints_with_headings):
            return np.array([[p[0], p[1], angle_rng_pi(p[2])] for p in pathPoints_with_headings], np.float)
            
        def get_route_length(self):
            return FixedWing.pathLength(self.pathpoints)
            
        def get_route_points(self):
            return self.pathpoints[:,:2]
            
        def addInitialPath(self, startPos):
            """Adds an initial rute to navigate to the path from the given position
            
            Parameters
            ----------
            startPos: numpy.ndarray of float [x, y, heading]
                start position of the path
                
            Returns
            -------
            SweepPlanner.PathPrimitive with the initial route added"""
            route = np.concatenate(([startPos], FixedWing.navigateTo(startPos, self.startPos)[0], self.pathpoints))
            return SweepPlanner.PathPrimitive(route)
                
        def rotate(self, times):
            rot = int(times)%4
            if rot%2 == 1:
                points = np.array([[-p[1], p[0], p[2]] for p in self.pathpoints])
            else:
                points = np.array(self.pathpoints)
            if rot > 1:
                points *= [-1, -1, 1]
            points += [0,0,rot*np.pi/2]
            return SweepPlanner.PathPrimitive(points)
            
        def mirror(self, axisX):
            if axisX:
                adder = [0,0,0]
                multiplier = [1, -1, -1]
            else:
                adder = [0,0,np.pi]
                multiplier = [-1, 1, -1]
            pts = np.array(self.pathpoints)
            points = (pts + adder) * multiplier
            return SweepPlanner.PathPrimitive(points)
            
        def mirrorAngle(self, angle):
            unitVec = np.array([np.cos(angle), np.sin(angle)], np.float)
            coords = np.array(self.pathpoints[:,:2])
            n=len(coords)
            coords = np.dot(np.dot(coords, unitVec).reshape(n,1), unitVec.reshape(1,2))*2-coords
            angles = -self.pathpoints[:,2] + 2*angle
            points = np.concatenate((coords, angles), axis=1)
            return SweepPlanner.PathPrimitive(points)
            
        def reverse(self):
            points = self.pathpoints[::-1] + [0,0,np.pi]
            return SweepPlanner.PathPrimitive(points)
        
        def move(self, centerPoint):
            points = self.pathpoints + [centerPoint[0], centerPoint[1], 0]
            return SweepPlanner.PathPrimitive(points)
            
        def __add__(self, other):
            if isinstance(other, SweepPlanner.PathPrimitive):
                otherpath = other
            else:
                try:
                    a = np.asarray(other, np.float)
                    a.shape = (2,)
                except:
                    raise TypeError('PathPrimitive is incompatible with %s for opeartor +' % (other,))
                return self.move(a)
            #if end mathches other start
            if (abs(self.endPos[2] - otherpath.startPos[2]) < EPSILON) and (dist(self.endPos[0:2], otherpath.startPos[0:2]) < EPSILON):
                points = np.concatenate((self.pathpoints, otherpath.pathpoints[1:]))
            else:
                nav = FixedWing.navigateTo(self.endPos, otherpath.startPos)
                points = np.concatenate((self.pathpoints, nav[0], otherpath.pathpoints))
            return SweepPlanner.PathPrimitive(points)

#TODO!! obsolete
def test_optimiseturn():
    paths = []
    actualRanges = np.logspace(np.log10(FW_PLAN_TURN_RADIUS/2), np.log10(10*FW_PLAN_TURN_RADIUS), 20)
    turns = []
    for actualRange in actualRanges:
        print(actualRange)
        forwardTurn = SweepPlanner.optimiseTurn([[2*actualRange, -actualRange, np.pi/4], [(2 - 1/np.sqrt(2))*actualRange, (-1 + 1/np.sqrt(2))*actualRange, np.pi/4]])
        reversemax = max(actualRange, FW_PLAN_TURN_RADIUS*(1+1/np.sqrt(2)))
        reverseTurn = SweepPlanner.optimiseTurn([[actualRange +reversemax, -reversemax, -np.pi*3/4], [(2 - 1/np.sqrt(2))*actualRange, (-1 + 1/np.sqrt(2))*actualRange, -np.pi*3/4]])
        if reverseTurn[1] < forwardTurn[1]:
            turnpts = reverseTurn[0]
        else:
            turnpts = forwardTurn[0]
        turn = SweepPlanner.PathPrimitive(turnpts, 0, np.pi/4)
        turn += -(turn.mirrorAngle(-np.pi/4) + [actualRange, actualRange])
        turn += turn.rotate(1) + [actualRange, actualRange]
        turn += [0, -actualRange]
        turns.append(turn.get_route_points())
        paths.append(follow_points(turn.get_route_points(), 0))
    scipy.io.savemat('matlab/optimiseTurn.mat', dict(pos = paths, points = turns))
    
def test_sweep_pattern(areaSize, maxSpacing, nSections, fullCoverage, isAcrossPattern):
    sp = SweepPlanner(areaSize, maxSpacing, nSections, fullCoverage)
    if isAcrossPattern:
        pattern = sp.acrossPattern
    else:
        pattern = sp.nearPattern
    waypoints = pattern.pathpoints
    pos = follow_points(waypoints)
    scipy.io.savemat('matlab/sweeppattern.mat', dict(pos = pos, points = pattern.pathpoints))
    
def test_sweep_cover():
    startpos = [0,0,0]
    plan = [[0,0],[1,-2],[0,-5],[-2,-2],[-3,-3]]
    sweeper = SweepPlanner(1300, 220, 3, False, [800, 800])
    spath = sweeper.planSweep(plan, startpos)
    waypoints = spath.pathpoints
    pos = follow_points(waypoints)
    shortsweep = sweeper.nextSweepTile(plan[1:3], startpos)
    pos2 = follow_points(shortsweep)
    scipy.io.savemat('matlab/sweeppattern.mat', dict(pos = pos, points = spath.pathpoints, nextTilePos = pos2))
    
def test_sweep_length():
    data = []
    for areasize in np.linspace(500,5000,5):
        for spacing in [100, 150, 200, 250]:
            sweeper = SweepPlanner(areasize, spacing, 3, True)
            lenacross0 = sweeper.acrossPattern.get_route_length()
            lennear0 = sweeper.nearPattern.get_route_length()
            sweeper = SweepPlanner(areasize, spacing, 3, False)
            lenacross1 = sweeper.acrossPattern.get_route_length()
            lennear1 = sweeper.nearPattern.get_route_length()
            data.append([areasize, spacing, sum(lenacross0), sum(lennear0), sum(lenacross1), sum(lennear1)])
    scipy.io.savemat('matlab/sweeplen.mat', dict(data=data))

if __name__ == "__main__":
    test_sweep_pattern(1050, 550, 3, False, True)
#    test_sweep_cover()
#    test_sweep_length()
    
    