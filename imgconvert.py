# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 17:49:05 2015

@author: Zoli
"""


from scipy import misc
from scipy import ndimage
import scipy.io
import numpy as np
from ProblemFactory import Distribution2D
import matplotlib.pyplot as plt

def block_mean(ar, fact):
    assert isinstance(fact, int), type(fact)
    sx, sy = ar.shape
    sx -= sx % fact
    sy -= sy % fact
    print(sx,sy)
    ar_cut = ar[:sx, :sy]
    X, Y = np.ogrid[0:sx, 0:sy]
    regions = sy/fact * (X/fact) + Y/fact
    res = ndimage.mean(ar_cut, labels=regions, index=np.arange(regions.max() + 1))
    res.shape = (sx/fact, sy/fact)
    return res

#filename = 'pre_haiti10_v-20_c.1'
filename = 'pre_ajka6_10pc'
#filename = 'gt_haiti10_2-4-10-40-125_E-61.062_1pxPm'
#filename = 'gt_ajka6_10pc'

density = misc.imread(filename + '.png', flatten=True)
dens2 = np.array(density, np.float)
plt.imshow(density)
plt.show()
res_dens = block_mean(density, 80)#misc.imresize(dens2, (10,10), 'nearest')
plt.imshow(res_dens)
plt.show()
print(res_dens)
print(res_dens.dtype, res_dens.shape)

#density = density.transpose()
#plt.imshow(density)
#plt.show()
#print(density.shape, density.dtype, np.max(density))
#dist = Distribution2D(density)
#samples = []
#for i in range(500):
#    [rndx, rndy] = np.random.uniform(size=2)
#    samples.append(dist.sample(rndx, rndy)*np.array([1,-1], np.float))
#
#plt.imshow(density)
#plt.show()
#density.tofile(filename + '.npa')
##scipy.io.savemat('samples.mat', {'samples': samples})
#density2 = np.fromfile(filename + '.npa', dtype=np.float32).reshape((1388L, 1612L))
#plt.imshow(density2)
#plt.show()