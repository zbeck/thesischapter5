# -*- coding: utf-8 -*-
"""
Created on Fri May 22 15:31:32 2015

@author: Zoli
"""
from searchgroup import SearchGroup
from rescuegroup import RescueGroup
from agent import CommFactory
from sensor_sim import SensorSim
from constants import RW_SEARCH_SPEED
import numpy as np
import scipy.io
import sys

class Commander:
    def __init__(self, connaddr, mode, search_coords_headings, search_tasks, search_area_size, search_task_offset, rescue_coords, density,
                 gradient=None, search_sensor_simulator=None, nSamples=104, port_start=None, debug=0):
        if port_start is None:
            port_start = 8880
        self.debug = debug
        next_level_debug = max(0, self.debug-1)
        self.comm = CommFactory(connaddr, port_start, next_level_debug)
        self.log = dict()
        self.log['timing'] = [[self.TimingActions.Init_start, time.time()]]
        assert(isinstance(mode, Commander.Mode))
        self.logging = True
        self.time = 0.0
        self.ticks = 0
        self.negotiate_tick = 0
        self.manvre_tick = mode.manvre_interval
        nagents = len(rescue_coords)
        self.mode = mode
        self.indiv_samples = np.ceil(float(nSamples)/nagents)
        self.overall_samples = self.indiv_samples*nagents
        if not self.mode.hindsight:
            self.indiv_samples = 1
            self.overall_samples = 1
        self.rescue = RescueGroup(rescue_coords, self.comm, gradient, next_level_debug)
        if self.mode.negotiation.isIndependent():
            rescueStates = []
        else:
            rescueStates = self.rescue.agent_states
        self.search = SearchGroup(search_coords_headings, self.comm, self.mode.search, self.mode.manvre, self.mode.orientation_count, search_tasks, search_area_size, 
                search_task_offset, density, rescueStates, search_sensor_simulator, self.overall_samples, debug=next_level_debug)
        if self.mode.negotiation.isIndependent():
            searchStates = []
        else:
            searchStates = self.search.agent_states
        self.rescue.init_search_comm(searchStates)
        self.init_manvre_comm(self.search.manvre_states)
        
        self.rescueTimer = RescueTimeDeterminer(self.mode.rescue, search_sensor_simulator.target_locations, self.search)
        self.rescue.set_rescue_timer(self.rescueTimer)
        self.log['rpos'] = self.rescue.log['pos']
        self.log['rtaskDone'] = self.rescue.log['taskDone']
        self.log['rtaskStart'] = self.rescue.log['taskStart']
        self.log['spos'] = self.search.log['pos']
        self.log['staskDone'] = self.search.log['taskDone']
        self.log['timing_names'] = self.TimingActions.get_timing_names(self.mode.negotiation.itercount())
        self.log['timing'].append([self.TimingActions.Init_end, time.time()])
        
    class TimingActions:
        Init_start, Init_end, Step_start, Manvre_start, Manvre_end, Observe_start, Observe_end, Simulate, Negotiate_init, Negotiate_step = range(10)
        @staticmethod
        def get_timing_names(itercount):
            return ['Init start', 'Init end', 'Step', 'Manvre start', 'Manvre end', 'Observe start', 'Observe end', 'Simulate', 'Neg init'] + ['Neg step%d' % (i,) for i in range(itercount+1)]
            
    def init_manvre_comm(self, manvre_states):
        self.comm.bcast('init_comm_manvre', ''.join([s.tostring() for s in manvre_states]))
        
    def negotiate(self):
        self.log['timing'].append([self.TimingActions.Negotiate_init, time.time()])
        if self.debug:
            print('Negotiation START')
        self.rescue.refresh_agent_positions()
        #self.search.refresh_agent_positions()   #automatically happening before each negotiation step anyways
        rtasks = self.search.get_rescue_tasks()
        density = self.search.get_density()
        self.rescue.update_problem(rtasks, density, self.mode.hindsight)
        if self.debug:
            print('\t%d rescue tasks' % (len(rtasks),))
        itercount = self.mode.negotiation.itercount()
        indep = self.mode.negotiation.isIndependent()
        search2manvre = SearchGroup.SearchMode.is_manvre_on(self.mode.search)
        self.log['timing'].append([self.TimingActions.Negotiate_step+self.mode.negotiation.itercount()-itercount, time.time()])
        if self.mode.negotiation.isTopStart():
            if self.debug:
                print('\tSearch neg, %d left... %s' % (itercount, time.asctime()))
            self.search.run_negotiation(not indep, search2manvre, itercount<3)
            itercount -= 1
            self.log['timing'].append([self.TimingActions.Negotiate_step+self.mode.negotiation.itercount()-itercount, time.time()])
        while itercount > 0:
            if self.debug:
                print('\tRescue neg, %d left... %s' % (itercount, time.asctime()))
            self.rescue.run_negotiation(self.indiv_samples, self.overall_samples)
            itercount -= 1
            self.log['timing'].append([self.TimingActions.Negotiate_step+self.mode.negotiation.itercount()-itercount, time.time()])
            if itercount > 0:
                if self.debug:
                    print('\tSearch neg, %d left... %s' % (itercount, time.asctime()))
                self.search.run_negotiation(not indep, search2manvre, itercount<3)
                search2manvre &= self.mode.manvre.resend_tardiness
                itercount -= 1
                self.log['timing'].append([self.TimingActions.Negotiate_step+self.mode.negotiation.itercount()-itercount, time.time()])
        if self.debug:
            print('Negotiation STOP')
            print(time.asctime())
                
    def manvre(self, observation_only=False):
        if self.debug:
            print('Manoeuvre START\n\tobservation only: %r'% (observation_only,))
        if observation_only:
            self.search.belief_update(self.mode.search == SearchGroup.SearchMode.debug)
        else:
            self.search.run_observe_manvre(self.mode.negotiation.isIndependent())
        if self.debug:
            print('Manoeuvre STOP')
            
    def restart_rescues(self):
        tasks = self.search.get_ongoing_tasks()
        if len(tasks) > 0:
            self.rescue.restart_rescues(tasks)
    
    def simulate(self):
        self.time += self.mode.time_unit
        if self.debug:
            print('Simulation START\n\tt=%d'% (self.time,))
            
        started_rescues, completed_rescues = self.rescue.run_simulation(self.time)
        for t in started_rescues:
            self.search.remove_rescue_task(t, True)
        for t in completed_rescues:
            if self.debug:
                print('\tRescue task completed: %s' % (t,))
            self.search.remove_rescue_task(t, False)
            
        self.search.run_simulation(self.time)
        if self.debug:
            print('Simulation STOP')
                
    def __call__(self):
        self.log['timing'].append([self.TimingActions.Step_start, time.time()])
        print(self.ticks, self.time)
        
        if self.ticks >= self.negotiate_tick:
            self.restart_rescues()
            self.negotiate()
            self.negotiate_tick += self.mode.negotiation_interval
        if self.ticks >= self.manvre_tick:
            self.log['timing'].append([self.TimingActions.Manvre_start, time.time()])
            self.manvre(observation_only=False)
            self.log['timing'].append([self.TimingActions.Manvre_end, time.time()])
            self.manvre_tick += self.mode.manvre_interval
        else:
            self.log['timing'].append([self.TimingActions.Observe_start, time.time()])
            self.manvre(observation_only=True)
            self.log['timing'].append([self.TimingActions.Observe_end, time.time()])
        self.log['timing'].append([self.TimingActions.Simulate, time.time()])
        self.simulate()
        self.ticks += 1
        return  not self.search.check_finished() or \
                not self.rescue.check_finished()
        
    def comm_stop(self):
        self.search.comm_stop()
        self.rescue.comm_stop()
        self.log['rescueLog'] = self.rescue.log
        self.log['searchLog'] = self.search.log
        
    class NegotiationMode:
        def __init__(self, modeparam):
            self.param = int(modeparam)
        def isTopStart(self):
            return self.param > 0
        def isBottomStart(self):
            return self.param < 0
        def isIndependent(self):
            return self.param == 0
        def itercount(self):
            return abs(self.param)+2
        
    class Mode:
        def __init__(self, negotiation_mode, search_mode, rescue_mode, manvre_mode, hindsight_mode=True,
                orientation_count=4, time_unit=1.0, manvre_interval=1, negotiation_interval=3):
            if not isinstance(negotiation_mode, Commander.NegotiationMode):
                try:
                    negotiation_mode = Commander.NegotiationMode(negotiation_mode)
                except (ValueError, TypeError):
                    raise AttributeError('negotiation_mode attribute is invalid')
            try:
                search_mode = int(search_mode)
            except (ValueError, TypeError):
                raise AttributeError('search_mode attribute is invalid')
            assert(search_mode < SearchGroup.SearchMode._size and search_mode >= 0)
            assert(isinstance(manvre_mode, SearchGroup.ManvreMode))
            self.negotiation = negotiation_mode
            self.search = int(search_mode)
            self.rescue = int(rescue_mode)
            self.manvre = manvre_mode
            self.hindsight = bool(hindsight_mode)
            self.time_unit = float(time_unit)
            self.manvre_interval = int(manvre_interval)
            self.negotiation_interval = int(negotiation_interval)
            self.orientation_count = int(orientation_count)
        def __str__(self):
            return 'n%ds%dr%dm%d(%s)h%dn%dd%d' % (self.negotiation.param, self.search, self.rescue,
                            self.manvre_interval, self.manvre, int(self.hindsight), self.negotiation_interval, self.orientation_count)
                            
class RescueTimeDeterminer:
    def __init__(self, mode, rtasks, searchgroup):
        if mode == 0:   #by distance
            self.mode = 0
        else:           #by area
            self.mode = 1
        self.rescueTasks = rtasks
        self.search = searchgroup
        
    def get_rescue_time(self, task):
        taskID = None
        time = 0.
        for taskid in self.search.rescue_tasks_in_plan:
            if all(task[:2] == self.search.rescue_tasks_in_plan[taskid][:2]):
                taskID = taskid
        if not taskID is None:
            if self.mode == 0:
                dist2 = np.sum((task[:2] - self.rescueTasks[taskID])**2)
                time = dist2*np.pi / RW_SEARCH_SPEED
            else:
                time = self.search.rescue_tasks_in_plan[taskID][2] / RW_SEARCH_SPEED
        return time, taskID
        
        
def savelog(cmd, rescue_tasks, search_tasks, logfname, is_test):
    cmd.comm_stop()
    cmd.log['resqTasks'] = rescue_tasks
    cmd.log['searchTasks'] = search_tasks
    print('Finished. Writing into ' + logfname)
    simout_path = 'simout/'
    if is_test:
        simout_path += 'test/'
    if not os.path.exists(simout_path):
        os.makedirs(simout_path)
    try:
        scipy.io.savemat(simout_path+logfname, cmd.log)
    except:
        scipy.io.savemat(logfname, cmd.log)
    #command took 0:40:29.03 (2429.03s total)
        
if __name__ == "__main__":
    from ProblemFactory import Distribution2D, Density
    import os
    import time
    import argparse
    
    print('Current dir:')
    print(os.getcwd())
    connaddr = b'tcp://127.0.0.1:'
    if os.getcwd()[1:3].lower() != ':\\':
        feedaddr = '/home/zb1f12/feeds/'
        if not os.path.exists(feedaddr):
            os.makedirs(feedaddr)
        connaddr = 'ipc://'+feedaddr
        os.chdir('/home/zb1f12/dec_dualsar/')
        
    parser = argparse.ArgumentParser(description='Start dual SAR realistic simulation.')
    parser.add_argument('runID', nargs='?', default=0, type=int)
    parser.add_argument('mode', nargs='?', default=1, type=int)
    parser.add_argument('inputFile', nargs='?', default='pre_haiti20.mat')
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--group', action='store_true')
    parser.add_argument('--console', action='store_true')
    parser.add_argument('--fix_loc', action='store_true')
    argv = []
    for ar in sys.argv:
        if ar == '>':
            break
        else:
            argv.append(ar)
    args = parser.parse_args(argv[1:])
    runID = args.runID
    mode = args.mode
    inputfile = args.inputFile
    is_test = args.test
    group_rescue_start_loc = args.group
    fix_rescue_start_loc = args.fix_loc
    
    if not args.console:
        printaddr = 'printout/'
        if is_test:
            printaddr += 'test/'
        if not os.path.exists(printaddr):
            time.sleep(runID%10)
            if not os.path.exists(printaddr):
                os.makedirs(printaddr)
        printfileaddr = printaddr + 'out%d-%d.txt' % (runID, mode)
        print('redirecting output to ' + printfileaddr)
        sys.stdout = open(printfileaddr, 'w')
        
    print('Parameters:\n\tID:%d, mode:%d, file:%s' % (runID, mode, inputfile))
        
    config_input = scipy.io.loadmat(inputfile)
    print('mat file loaded')
    rescue_tasks = np.fromfile(config_input['outcome_folder'][0] + '/rlocs%d.npa' % (runID,), np.float64).reshape(-1,2)
    agent_locs = None
    agent_loc_filename = config_input['outcome_folder'][0] + '/alocs%d.npa' % (runID,)
    if os.path.isfile(agent_loc_filename):
        agent_locs = np.fromfile(agent_loc_filename, np.float64).reshape(-1, 3)
        #format: [[loc_X, loc_Y, orientation_angle],...]
    search_tasks = config_input['search_tasks']
    offset_to_density = config_input['offset'][0]
    scaler = config_input['scaler'][0,0]
    grid_size = config_input['grid_size'][0,0]
    offset_to_task = np.min(search_tasks, 0)
    search_task_coords = (search_tasks-offset_to_task) / grid_size
    density = config_input['density']
    gradient_dir =  np.fromfile(config_input['outcome_folder'][0] + '/gradient.npa', np.float64).reshape(density.shape)
    global_gradient_dir = Density(gradient_dir, scaler, offset_to_density)
    dstr = Distribution2D(Density(density, scaler, offset_to_density))
    dstr.setExpectedVal(config_input['entasks'][0,0])
    density = dstr.getDensity()
    
    #rescue task time based on location error
    RESCUE_MODE = 0
    #rescue taks time based on sensing model only
#    RESCUE_MODE = 1
    manvre_mode_def = SearchGroup.ManvreMode(True, 2.0, 5)
    manvre_mode_var = SearchGroup.ManvreMode(True, 2.0, 5, True)
    
    if mode == 0:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.Scherry,
                RESCUE_MODE, manvre_mode_def, negotiation_interval=10)
    elif mode == 1:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.NoMan,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10)
    elif mode == 2:
        cmode = Commander.Mode(Commander.NegotiationMode(5), SearchGroup.SearchMode.NoMan,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10)
    elif mode == 3:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.MCTS,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10)
    elif mode == 4:
        cmode = Commander.Mode(Commander.NegotiationMode(5), SearchGroup.SearchMode.MCTS,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10)
    elif mode == 5:
        cmode = Commander.Mode(Commander.NegotiationMode(-7), SearchGroup.SearchMode.MCTS,
                RESCUE_MODE, manvre_mode_var, orientation_count=8, negotiation_interval=10)
    elif mode == 6:
        cmode = Commander.Mode(Commander.NegotiationMode(-7), SearchGroup.SearchMode.NoMan,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10)
    elif mode == 7:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.Scherry,
                RESCUE_MODE, manvre_mode_def, negotiation_interval=10, hindsight_mode=False)
    elif mode == 8:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.NoMan,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10, hindsight_mode=False)
    elif mode == 9:
        cmode = Commander.Mode(Commander.NegotiationMode(0), SearchGroup.SearchMode.MCTS,
                RESCUE_MODE, manvre_mode_def, orientation_count=8, negotiation_interval=10, hindsight_mode=False)
    else:
        exit()
    print('Current mode:"%s"' % (cmode,))
    if agent_locs is None or fix_rescue_start_loc:
        #locations:
        university_rw_locations = np.array([[776000,2050500], [776000-10,2050540], [776000-50, 2050560], [776000-80, 2050580]], np.float)
        #first rotary wing near early task for testing
#        university_rw_locations = np.array([[768300,2052000], [776000-10,2050540], [776000-50, 2050560], [776000-80, 2050580]], np.float)
        college_rw_locations = np.array([[772450, 2051950], [772490, 2051950], [772450, 2052000], [772490, 2052000]], np.float)
        stadium_rw_locations = np.array([[773750, 2051400], [773800, 2051400], [773760, 2051450], [773810, 2051450]], np.float)
        fw_locations = np.array([[768000,2052400,-0.2], [777500,2051750,np.pi+0.3]])
        rw_locations = np.concatenate((university_rw_locations, college_rw_locations, stadium_rw_locations))
    else:
        fw_locations = agent_locs[:2]
        if not group_rescue_start_loc:
            rw_locations = agent_locs[2:14, :2]
        else:
            # rotary wings in 3 groups of 4 in a 40m rectangle
            loc_offset = np.array([[-20, -20], [20, -20], [-20, 20], [20, 20]], np.float)
            rw_locations = agent_locs[2:5,np.newaxis,:2] + loc_offset[np.newaxis,:,:]
            rw_locations = np.concatenate(rw_locations[:])
    cmd = Commander(connaddr, cmode, fw_locations, search_task_coords, grid_size,
                    offset_to_task, rw_locations, density, global_gradient_dir,
                    SensorSim(rescue_tasks), nSamples=108, port_start=8880+20*runID+mode*3000, debug = 0)
    t = time.time()
    timelimit = 130000  #40h = 144000s
    if is_test:
        timelimit = 5000  #83.3 min
    print('time limited test simulation (limit = %d s).'%(timelimit,))
    i = 0
    logfname = 'dual_log%d-%s.mat' % (runID,mode)
    while True:
#    for _ in range(1):
        try:
            still_running = cmd()
        except:
            savelog(cmd, rescue_tasks, search_tasks, logfname, is_test)
            raise
        if not still_running:
            break
        print('#######sim step %d done, time: %d' % (i,time.time()-t))
        i += 1
        if time.time()-t > timelimit:
            break
    savelog(cmd, rescue_tasks, search_tasks, logfname, is_test)