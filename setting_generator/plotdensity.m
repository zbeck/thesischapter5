%% gaussian tile
tilesize = 15;
mu = [0 0];
Sigma = [9.07 9.07];
x1 = -tilesize:tilesize; x2 = -tilesize:tilesize;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
surf(x1,x2,F);
caxis([min(F(:))-.5*range(F(:)),max(F(:))]);
axis([-tilesize tilesize -tilesize tilesize 0 max(max(F))])
xlabel('x1'); ylabel('x2'); zlabel('Probability Density');

%% creating pattern
load('filtered_haiti_data31.mat', 'buildings')
factor = 2.5;
weights = [1 factor factor^2 factor^3 factor^4];
w_adjust = 200/sum(sum(weights(buildings(:,3))));
weights = weights*w_adjust;
img = zeros(ceil(area_bounds.x_range(2)-area_bounds.x_range(1))+2*tilesize, ceil(area_bounds.y_range(2)-area_bounds.y_range(1))+2*tilesize);
for idx = 1:size(buildings,1)
    posx = int32(buildings(idx,1)-area_bounds.x_range(1))+tilesize+1;
    posy = int32(buildings(idx,2)-area_bounds.y_range(1))+tilesize+1;
    %if posx<tilesize || posy<tilesize
    img(posx-tilesize:posx+tilesize, posy-tilesize:posy+tilesize) = ...
        img(posx-tilesize:posx+tilesize, posy-tilesize:posy+tilesize) +...
        weights(buildings(idx,3)) * F;
end

%% rescale
scaler = 5;
imgs = zeros(floor(size(img)/scaler));
for x = 1:size(imgs,1)
    for y = 1:size(imgs,2)
        imgs(x,y) = sum(sum(img((scaler*(x-1)+1):(scaler*x),...
            (scaler*(y-1)+1):(scaler*y))))/(scaler^2);
    end
end
maxdens = max(max(imgs));
figure(2)
imshow(imgs, [0 maxdens])
colormap(flipud(gray))
colorbar

maxval = max(max(imgs));
imgs = imgs/maxval;
imwrite(imgs, 'gt_haiti31.png')