nconfig = 128;
entasks = 200;
load('filtered_haiti_data31.mat')
resq_tasks = cell(nconfig,1);
agent_loc = cell(nconfig,1);
nagents = 14;
for index = 1:nconfig
    resq_tasks{index} = sample_locations(entasks);
    agent_loc{index} = unifrnd(repmat([area_bounds.x_range(1), area_bounds.y_range(1), -pi], [nagents, 1]),...
        repmat([area_bounds.x_range(2), area_bounds.y_range(2), pi], [nagents, 1]));
end
save('rescue_locations_haiti31.mat', 'resq_tasks', 'agent_loc')
% figure
% hold off
% plot(taskpoints_gt(:,1), taskpoints_gt(:,2), 'bo')
% axis equal
% hold on
% rtasks = sample_locations(200);
% plot(rtasks(:,1), rtasks(:,2), 'rx')

%% densitymap
scale = 10;
sigma_multiplier = 20;
ratio_in_uniform = 0.5;
nreports = 50;

sigma = 9.07 * sigma_multiplier / scale;
half_window = ceil(3*sigma)*2;

area = ones([ceil((area_bounds.x_range(2)-area_bounds.x_range(1))/scale)+2*half_window,...
    ceil((area_bounds.y_range(2)-area_bounds.y_range(1))/scale)+2*half_window]);
area_shift = [half_window, half_window] - [area_bounds.x_range(1), area_bounds.y_range(1)]/scale;

gaussian_layer = zeros(size(area));
reports = sample_locations(nreports, [], 0);
g = fspecial('gaussian', 2*half_window, sigma);
for index = 1:size(reports,1)
    c = round(reports(index, :))/scale + area_shift;
    gaussian_layer((c(1)-half_window+1):(c(1)+half_window),...
        (c(2)-half_window+1):(c(2)+half_window)) =...
        gaussian_layer((c(1)-half_window+1):(c(1)+half_window),...
        (c(2)-half_window+1):(c(2)+half_window)) + g;
end

area(1:(half_window), :) = 0;
area(:, 1:(half_window)) = 0;
area((end - half_window+1):end, :) = 0;
area(:,(end - half_window+1):end) = 0;
gaussian_layer(1:(half_window), :) = 0;
gaussian_layer(:, 1:(half_window)) = 0;
gaussian_layer((end - half_window+1):end, :) = 0;
gaussian_layer(:,(end - half_window+1):end) = 0;

for index=1:size(area_bounds.exclude_topleft, 1)
    ex = area_bounds.exclude_topleft(index,:)/scale + area_shift;
    area(1:ex(1), ex(2):end) = 0;
    gaussian_layer(1:ex(1), ex(2):end) = 0;
end
for index=1:size(area_bounds.exclude_topright, 1)
    ex = area_bounds.exclude_topright(index,:)/scale + area_shift;
    area(ex(1):end, ex(2):end) = 0;
    gaussian_layer(ex(1):end, ex(2):end) = 0;
end
for index=1:size(area_bounds.exclude_botleft, 1)
    ex = area_bounds.exclude_botleft(index,:)/scale + area_shift;
    area(1:ex(1), 1:ex(2)) = 0;
    gaussian_layer(1:ex(1), 1:ex(2)) = 0;
end
for index=1:size(area_bounds.exclude_botright, 1)
    ex = area_bounds.exclude_botright(index,:)/scale + area_shift;
    area(ex(1):end, 1:ex(2)) = 0;
    gaussian_layer(ex(1):end, 1:ex(2)) = 0;
end

img = ratio_in_uniform * area/sum(sum(area)) + (1-ratio_in_uniform) * gaussian_layer/sum(sum(gaussian_layer));
img = img/max(max(img));
density = img;

%% order search tasks
half_task_size = rect_size/scale;
ntasks = size(taskpoints_pre, 1);
exp_val = zeros([ntasks, 1]);
for index = 1:ntasks
   pos = taskpoints_pre(index,:)/scale + area_shift;
   area = img(floor(pos(1)-half_task_size+1):floor(pos(1)+half_task_size),...
       floor(pos(2)-half_task_size+1):floor(pos(2)+half_task_size));
   exp_val(index) = sum(sum(area));
end
[sorted, idx] = sort(exp_val,'descend');
search_tasks = taskpoints_pre(idx, :);
p = search_tasks/scale + repmat(area_shift, [ntasks, 1]);

%% visulaise
hold off
imshow(img')
hold on
plot(p(:,1), p(:,2), 'rx')
labels = cellstr(num2str([1:ntasks]'));
%saveas(gcf, 'pre_haiti31_task_locs.png')
text(p(:,1), p(:,2), labels, 'VerticalAlignment','bottom', ...
                            'HorizontalAlignment','right','Color','red')

%% make output files
img = rot90(img);
%img = img';
outcome_folder = 'haiti31';
pre_image_file = 'pre_haiti31.png';
imwrite(img, pre_image_file)
scaler = scale;
offset = -area_shift*scale;
grid_size = rect_size;
%save('pre_haiti31.mat', 'density', 'scaler', 'offset', 'search_tasks',...
%    'entasks', 'grid_size', 'pre_image_file', 'outcome_folder')