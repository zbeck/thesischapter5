# -*- coding: utf-8 -*-
"""
Created on Tue Mar 01 17:06:41 2016

@author: Zoli
"""
import scipy.io
import os
import numpy as np
import matplotlib.pyplot as plt

EPSILON = 1./128

def get_gradient(img, loc):
    if len(loc) < 2:
        raise
    surr = [-1, 0, 1]
    grid = np.meshgrid(surr, surr)
    posgrid = [loc[0]+grid[0], loc[1]+grid[1]]
    grad_data = img[posgrid]
    grad_y_mul = np.array([[-1, -2, -1],[0, 0, 0],[1, 2, 1]])
    grad_x_mul = np.transpose(grad_y_mul)
    return (np.sum(grad_data*grad_x_mul), np.sum(grad_data*grad_y_mul))
    
def get_grad_dir(img, loc):
    grad = get_gradient(img, loc)
    if abs(grad[0]) < EPSILON and abs(grad[1]) < EPSILON:
        return None
    else:
        return np.arctan2(grad[1], grad[0])
        
def closest_index(indices, loc):
    dist2 = (indices[0]-loc[0])**2 + (indices[1]-loc[1])**2
    closest = np.argmin(dist2)
    return (indices[0][closest], indices[1][closest])
    
def index_dir(index, loc):
    return np.arctan2(index[1]-loc[1][0], index[0]-loc[0][0])
    
def find_mean(img):
    exp_val = np.sum(img)
    x_margin = np.sum(img, axis=1)
    y_margin = np.sum(img, axis=0)
    x_mean = np.sum(x_margin * np.arange(x_margin.size))/exp_val
    y_mean = np.sum(y_margin * np.arange(y_margin.size))/exp_val
    return (x_mean, y_mean)
    
def boundary_x(indices):
    boundary = []
    sorted_idx = np.lexsort(indices)
    sorted_indices = [indices[0][sorted_idx], indices[1][sorted_idx]]
    for i in range(len(sorted_indices[0])-3):
        x = sorted_indices[0][i]
        y = sorted_indices[1][i]
        if not (sorted_indices[0][i+1]==x+1 and sorted_indices[0][i+2]==x+2 and
            sorted_indices[1][i+1]==y and sorted_indices[1][i+2]==y):
            boundary.append(i+1)
    return [sorted_indices[0][boundary], sorted_indices[1][boundary]]
    
def boundary_y(indices):
    indices_t = [indices[1], indices[0]]
    boundary = boundary_x(indices_t)
    return [boundary[1], boundary[0]]
    
def get_nonzero_boundary_indices(img):
    nonzero_idx = np.nonzero(img)
    nonzero_boundary_y = boundary_y(nonzero_idx)
    nonzero_boundary_x = boundary_x(nonzero_idx)
    return [np.concatenate((nonzero_boundary_y[0], nonzero_boundary_x[0])),
            np.concatenate((nonzero_boundary_y[1], nonzero_boundary_x[1]))]
    
    
if __name__ == "__main__":   
    locdata = scipy.io.loadmat('rescue_locations_haiti31.mat')
    rlocs = locdata['resq_tasks']
    alocs = locdata['agent_loc']
    matfile = scipy.io.loadmat('pre_haiti31.mat')
    folder = matfile['outcome_folder'][0]
    #if not os.path.exists(folder+'/'):
    #    os.makedirs(folder+'/')
    ##print([len(a[0]) for a in rlocs])
    #index = 0
    #for e in rlocs:
    #    e[0].tofile(folder + '/rlocs%d.npa' % (index,))
    #    index += 1
    #index = 0
    #for e in alocs:
    #    e[0].tofile(folder + '/alocs%d.npa' % (index,))
    #    index += 1
        
    img = matfile['density']
    gradient = np.zeros(img.shape, dtype=np.float64)
    mean_idx = find_mean(img)
    nonzero_idx = get_nonzero_boundary_indices(img)
    x_vals = np.arange(img.shape[0])
    y_vals = np.arange(img.shape[1])
    for x in x_vals:
        for y in y_vals:
            loc = [np.array([x]), np.array([y])]
            if img[x,y] == 0.:
                current_dir = index_dir(closest_index(nonzero_idx, loc), loc)
            else:
                current_dir = get_grad_dir(img, loc)
                if current_dir is None:
                    current_dir = index_dir(mean_idx, loc)
            gradient[x,y] = current_dir
    gradient.tofile(folder + '/gradient.npa')
    x_vals = np.arange(img.shape[0], step=10)
    y_vals = np.arange(img.shape[1], step=10)
    X,Y = np.meshgrid(x_vals, y_vals)
    U = np.cos(gradient[[X,Y]])
    V = np.sin(gradient[[X,Y]])
    
    fig = plt.figure(num=1)
    ax=fig.add_subplot(111)
    ax.imshow(np.transpose(img), origin='lower')
    ax.quiver(X,Y, U, V)
    plt.show()
    
        
    #(((M[0, 0] + (2.0 * M[1, 0])) + M[2, 0]) - ((M[0, 2] + (2.0 * M[1, 2])) + M[2, 2]))
    #(((M[0, 0] + (2.0 * M[0, 1])) + M[0, 2]) - ((M[2, 0] + (2.0 * M[2, 1])) + M[2, 2]))