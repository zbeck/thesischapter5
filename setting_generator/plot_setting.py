# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 20:40:26 2016

@author: Zoli
"""

import scipy.io
import scipy.ndimage
import os
import numpy as np
import matplotlib.pyplot as plt
from convert_rescue_locs import get_nonzero_boundary_indices

if __name__ == "__main__":
    locdata = scipy.io.loadmat('rescue_locations_haiti31.mat')
    alocs = locdata['agent_loc']
    matfile = scipy.io.loadmat('pre_haiti31.mat')
    
    density = matfile['density']
    search_tasks = matfile['search_tasks']
    offset_to_density = matfile['offset'][0]
    scaler = matfile['scaler'][0,0]
    
    nTasks = 200.0
    overallpreint = np.sum(density)
    preint = density*nTasks / overallpreint
    
    topval = np.max(preint)
    plt.hist(preint.flatten(), bins=26, range=(topval/256, topval))
    plt.show()
    
    gtimage = scipy.ndimage.imread('gt_haiti31.png')
    gtdensity = 255.0-gtimage
    overallgtint = np.sum(gtdensity)
    gtint = gtdensity*nTasks / overallgtint
    
    gtlaregvals = gtint>topval
    gtnonzerovals = gtint>0.0
    gtlargearea = np.sum(gtlaregvals) /10000.0
    gtnonzeroarea = np.sum(gtlaregvals) /10000.0
    print(gtlargearea)
    print(gtnonzeroarea)
    print(gtlargearea/gtnonzeroarea)
    
    #topval = np.max(gtint)
    plt.hist(gtint.flatten(), bins=26, range=(topval/256, topval))
    plt.show()
    
        
#    university_rw_locations = np.array([[776000,2050500], [776000-10,2050540], [776000-50, 2050560], [776000-80, 2050580]], np.float)
#    college_rw_locations = np.array([[772450, 2051950], [772490, 2051950], [772450, 2052000], [772490, 2052000]], np.float)
#    stadium_rw_locations = np.array([[773750, 2051400], [773800, 2051400], [773760, 2051450], [773810, 2051450]], np.float)
#    fw_locations = np.array([[768000,2052400,-0.2], [777500,2051175,np.pi+0.3]])
#    rw_locations = np.concatenate((university_rw_locations, college_rw_locations, stadium_rw_locations))
#    
#    
#    satellite = plt.imread('haiti_dataset_overview.jpg')
#    pic_coord1 = np.array([43, 41], np.float)
#    pic_diff = np.array([1833, 977], np.float)
#    UMT_coord1 = np.array([767700, 2048500], np.float)
#    UMT_coord2 = np.array([777500, 2053700], np.float)
#    pic2UMT_scale = (UMT_coord2-UMT_coord1)/pic_diff
#    UMTbotleft = UMT_coord1 - pic_coord1*pic2UMT_scale
#    UMTtopright = UMTbotleft + satellite.shape[-2::-1]*pic2UMT_scale
#    plot_botleft = (UMTbotleft-offset_to_density)/scaler
#    plot_topright = (UMTtopright-offset_to_density)/scaler
#    plt.imshow(satellite, extent=(plot_botleft[0], plot_topright[0], plot_botleft[1], plot_topright[1]))
#    
#    boundary_idx = get_nonzero_boundary_indices(density)
#    plt.plot(boundary_idx[0], boundary_idx[1], 'bo', markersize=2, markeredgewidth=0)
#    
#    plt.imshow(density.T, origin='lower', alpha=0.5)
#    plt.set_cmap(plt.get_cmap('Greys'))
#    fwpos = (fw_locations[:, :2]-offset_to_density)/scaler
#    rwpos = (rw_locations-offset_to_density)/scaler
#    plt.plot(fwpos[:,0], fwpos[:,1], 'rx', ms=15, mew=3)
#    plt.plot(rwpos[:,0], rwpos[:,1], 'g+', ms=15, mew=2)
#    taskplot = (search_tasks-offset_to_density)/scaler
#    plt.plot(taskplot[:,0], taskplot[:,1], 'ro', fillstyle='none', ms=8, mew=2)
#    plt.axis((plot_botleft[0], plot_topright[0], min(boundary_idx[1])-10, max(boundary_idx[1])+10))
#    #plt.show()
#    plt.axis('off')
#    fig = plt.gcf()
#    fig.set_size_inches(25, 15)
#    plt.savefig('haiti_setting.jpg', bbox_inches='tight')