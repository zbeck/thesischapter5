%% load haiti data file

load('haitidamage.mat')

%% Initial parameters of the area

x_range = [767650, 777500];
y_range = [2048150, 2053600];
rect_size = 591.6;

exclude_topleft =   [771500, 2052500;...
                     772100, 2053200];
exclude_topright =  [773800, 2053200;...
                     774600, 2052900;...
                     775800, 2051900;...
                     776500, 2051400];
exclude_botleft =   [770400, 2051300;...
                     772000, 2050700;...
                     773000, 2049300];
exclude_botright =  [777000, 2049100];

%% Adjust parameters for grid size (rect_size)

y_diff = y_range(2)-y_range(1);
y_diff = ceil(y_diff/rect_size) * rect_size;
y_range(2) = y_range(1) + y_diff;
x_diff = x_range(2)-x_range(1);
x_diff = ceil(x_diff/rect_size) * rect_size;
x_range(2) = x_range(1) + x_diff;

%% Filter points in DT

x_ok = (DT.X > x_range(1)) + (DT.X < x_range(2));
y_ok = (DT.Y > y_range(1)) + (DT.Y < y_range(2));
pos_pass = (x_ok+y_ok) == 4;
for corner = 1:size(exclude_topleft,1)
    pos_pass = pos_pass & (DT.X > exclude_topleft(corner,1) | DT.Y < exclude_topleft(corner,2));
end
for corner = 1:size(exclude_topright,1)
    pos_pass = pos_pass & (DT.X < exclude_topright(corner,1) | DT.Y < exclude_topright(corner,2));
end
for corner = 1:size(exclude_botleft,1)
    pos_pass = pos_pass & (DT.X > exclude_botleft(corner,1) | DT.Y > exclude_botleft(corner,2));
end
for corner = 1:size(exclude_botright,1)
    pos_pass = pos_pass & (DT.X < exclude_botright(corner,1) | DT.Y > exclude_botright(corner,2));
end

%reference rectangle
rect_c = [mean(x_range) mean(y_range)];
%rect grid
[mesh_x, mesh_y] = meshgrid((x_range(1)+rect_size/2):rect_size:(x_range(2)-rect_size/2), (y_range(1)+rect_size/2):rect_size:(y_range(2)-rect_size/2));
mesh_x = reshape(mesh_x, 1, []);
mesh_y = reshape(mesh_y, 1, []);

pre_fit = true(size(mesh_x));
for corner = 1:size(exclude_topleft,1)
    pre_fit = pre_fit & ((mesh_x+rect_size/2) > exclude_topleft(corner,1) | (mesh_y-rect_size/2) < exclude_topleft(corner,2));
end
for corner = 1:size(exclude_topright,1)
    pre_fit = pre_fit & ((mesh_x-rect_size/2) < exclude_topright(corner,1) | (mesh_y-rect_size/2) < exclude_topright(corner,2));
end
for corner = 1:size(exclude_botleft,1)
    pre_fit = pre_fit & ((mesh_x+rect_size/2) > exclude_botleft(corner,1) | (mesh_y+rect_size/2) > exclude_botleft(corner,2));
end
for corner = 1:size(exclude_botright,1)
    pre_fit = pre_fit & ((mesh_x-rect_size/2) < exclude_botright(corner,1) | (mesh_y+rect_size/2) > exclude_botright(corner,2));
end

%% Visualisation

hold off
scatter(DT.X(pos_pass), DT.Y(pos_pass), 5, DT.DamageLevel(pos_pass))
colormap(jet(6))
colorbar
axis equal
hold on
plot(mesh_x(pre_fit), mesh_y(pre_fit), 'g+')
allexclude = [exclude_botleft; exclude_botright; exclude_topleft; exclude_topright];
plot(allexclude(:,1), allexclude(:,2), 'rx')

rect_x = repmat(mesh_x(pre_fit), [5,1]) + repmat([1;1;-1;-1;1]*rect_size/2, [1, sum(pre_fit)]);
rect_y = repmat(mesh_y(pre_fit), [5,1]) + repmat([1;-1;-1;1;1]*rect_size/2, [1, sum(pre_fit)]);
plot(rect_x, rect_y, 'g', 'LineWidth', 3)
dx = mod(mesh_x(1), rect_size);
dy = mod(mesh_y(1), rect_size);
xgrid = round((DT.X(pos_pass)-dx)/rect_size)*rect_size+dx;
ygrid = round((DT.Y(pos_pass)-dy)/rect_size)*rect_size+dy;
validpos = unique([xgrid, ygrid], 'rows');
scatter(validpos(:,1), validpos(:,2), 30, zeros([size(validpos,1),1]), 'filled')

%% Export data

buildings = [DT.X(pos_pass), DT.Y(pos_pass), DT.DamageLevel(pos_pass)];
%fix the one 0 damage grade to 1 (judged according to imagery)
buildings(buildings(:,3)==0, 3) = 1;
taskpoints_gt = validpos;
taskpoints_pre = [mesh_x(pre_fit)', mesh_y(pre_fit)'];
area_bounds = struct('x_range', x_range, 'y_range', y_range,...
    'exclude_topleft', exclude_topleft, 'exclude_topright', exclude_topright,...
    'exclude_botleft', exclude_botleft, 'exclude_botright', exclude_botright);
save('filtered_haiti_data31.mat', 'buildings', 'taskpoints_gt', 'taskpoints_pre', 'area_bounds', 'rect_size')
