# -*- coding: utf-8 -*-
"""
Created on Mon Oct 05 13:13:29 2015

@author: Zoli
"""
from agentgroup import AgentGroup
from agent import Attitude, CommFactory, dist
from constants import RW_BATTERY_TIME, RW_MAX_SPEED, SIM_STEP, EPSILON, EXIT_EXP_VAL
from ResQr import resqr_process
from quadsimulator import Quad
import threading
from multiprocessing import Process
import numpy as np
import scipy.io
import sys
import time

class RescueGroup(AgentGroup):
    def __init__(self, agent_coords, comm, gradient=None, debug=False):
        attitudes = []
        for c in agent_coords:
            attitudes.append(Attitude(c, [0,0], RW_BATTERY_TIME))
        AgentGroup.__init__(self, attitudes, comm, topics=['update_resq_attitudes'])
        self.log['taskStart'] = []
        self.SIM_STEP = SIM_STEP
        self.CALC_TIME = 2.0
        self.COMM_TIME = 1.0
        self.DECISION_INTERVAL = 3.0
        self.debug = debug
        next_level_debug = max(0, self.debug-1)
        next_level_debug2 = max(0, self.debug-2)
        for i in range(len(self.agent_states)):
            astates_txt = ''.join([s.tostring() for s in self.agent_states])
            gradient_txt = ''
            if gradient is not None:
                gradient_txt = gradient.tostring()
#            proc = threading.Thread(target=resqr_process, args=(i, astates_txt, comm.subscribe_addr, comm.response_addr, comm.context, next_level_debug))
            proc = Process(target=resqr_process, args=(i, astates_txt, comm.subscribe_addr, comm.response_addr, None, gradient_txt, next_level_debug))
            self.agents.append(proc)
            proc.start()
            self.platforms.append(Quad(self.agent_states[i].attitude, debug=next_level_debug2))
        self.sender_lock = threading.Lock()
        time.sleep(3)
        self.comm_start()
        
    def init_search_comm(self, search_states):
        self.comm.bcast('init_search_comm', ''.join([s.tostring() for s in search_states]))
            
    def set_rescue_timer(self, rescue_timer):
        for p in self.platforms:
            p.set_rescue_timer(rescue_timer)
            
    def restart_rescues(self, tasks):
        '''Initiate the possible restart of a rescue task if the location estimate
        has improved significantly.
        
        Parameters
        ==========
        tasks:
            np.ndarray, [[pos_x, pos_y, rescue_time_estimate, target_id],...]'''
        tasks = np.asarray(tasks)
        current_tasks = []
        for i in range(len(self.platforms)):
            if self.platforms[i].att.currentTask is not None and self.platforms[i].att.currentTask[0] >= 0:
                current_tasks.append([i, self.platforms[i].att.currentTask[0]])
        if not current_tasks:
            return
        current_tasks = np.asarray(current_tasks, np.int)
        filtered_tasks = tasks[np.in1d(tasks[:,-1], current_tasks[:,1])]
        for t in filtered_tasks:
            platform = self.platforms[current_tasks[current_tasks[:,1]==int(np.round(t[-1]))][0,0]]
            distance = dist(t[:2], platform.att.loc)
            time_to_complete = distance/RW_MAX_SPEED + t[2]
            if time_to_complete < platform.att.estimatedWaitTime():
                platform.set_target(t[:3])
        
    def run_negotiation(self, nsample, overall_sample):
        responses = self.comm.bcast('decide_resq', np.array([nsample, overall_sample], np.int))
        for r in responses:
            agent_id = np.fromstring(r[CommFactory.topic_len:], np.int, 1)
            data = r[CommFactory.topic_len+len(agent_id.tostring()):]
            agent_id = agent_id[0]
            instruction = np.fromstring(data, np.float)
            debugtime = instruction[-1]
            instruction = instruction[:-1]
            if self.debug:
                print('decision %s%d: %s %s' % (r[:CommFactory.topic_len],
                    agent_id, instruction, time.asctime(time.localtime(debugtime))))
            self.agent_instructions[agent_id] = instruction
        return self.agent_instructions
        
    def run_simulation(self, next_decision_time):
        reached_tasks = []
        completed_tasks = []
        for i in range(len(self.platforms)):
            self.platforms[i].set_target(self.agent_instructions[i])
        while self.time < next_decision_time:
            for p in self.platforms:
                reached_task, completed_task = p.step(self.SIM_STEP)
                if reached_task is not False:
                    reached_tasks.append(reached_task)
                    if self.remove_task(reached_task[:2]) and self.logging:
                        self.log['taskStart'].append(np.concatenate((reached_task,
                            [reached_task[2]+self.SIM_STEP+self.time])))
                if completed_task is not False:
                    completed_tasks.append(completed_task)
                    if self.logging:
                        self.log['taskDone'].append(np.concatenate((completed_task,
                            [self.time+self.SIM_STEP])))
            if self.logging:
                newline = np.array([p.att.loc for p in self.platforms], np.float)
                if self.debug>1:                    
                    for c in np.reshape(newline, -1):
                        print("{:4.0f}".format(c)),
                    print()
                self.log['pos'].append(newline)
            self.time += self.SIM_STEP
        return reached_tasks, completed_tasks
        
    def remove_task(self, task):
        self.comm.bcast('delete_rescue_task', task)
        success = False
        for t in self.tasks:
            if dist(t[:2], task[:2]) < EPSILON:
                success = True
                break
        return success
            
    def update_problem(self, tasks, density, pass_on_density):
        self.tasks = tasks
        #checking if rescue has finished
        if len(tasks) == 0 and density.exp_value() < EXIT_EXP_VAL:
            self.comm_stop()
            return
            
        self.comm.bcast('update_tasks', tasks)
        if pass_on_density:
            self.comm.bcast('update_density', density)
        else:
            self.comm.bcast('update_density', '')
            
    def check_finished(self):
        if AgentGroup.check_finished(self):
            self.finish_current_tasks()
            self.logging = False
            return True
#            if all([p.att.waitTime <= 0. for p in self.platforms]):
#                return True
#            if all([p.att.batteryTime <= 0. for p in self.platforms]):
#                return True
        return False
        
    def finish_current_tasks(self):
        if self.logging:
            for p in self.platforms:
                if p.att.currentTask is not None and p.att.currentTask[0] >= 0:
                    self.log['taskDone'].append(np.array([p.att.currentTask[0], self.time+p.att.waitTime]))
#    def __call__(self, next_decision_time=None):
#        while not self.check_finished():
#            print(self.time),
#            self.run_negotiation()
#            print('.')
#            if next_decision_time == None:
#                next_decision_time = self.time + self.DECISION_INTERVAL
#            self.run_simulation(next_decision_time)
#            discovered = self.explorer.step_to_time(self.time)
#            if discovered:
#                self.update_densities(self.explorer.get_density())
#            for t in discovered:
#                self.add_task(t)
#            if self.agents[0].pf.isCompleted():
#                break
#            self.refresh_agent_positions()
        
        
if __name__ == "__main__":
    from ProblemFactory import Distribution2D
    import os
    print('Current dir:')
    print(os.getcwd())
    if os.getcwd()[0:3].lower() != 'd:\\':
        os.chdir('/home/zb1f12/dec_dualsar/')
    
    runID = int(sys.argv[1])
    print('Loading simoutput%d.mat' % (runID,))
    
    density = np.fromfile('gt_haiti10_2-4-10-40-125_E-61.062_1pxPm.npa', dtype=np.float32).reshape((1388L, 1612L))
    dstr = Distribution2D(density)
    dstr.setExpectedVal(61.062)
    density = dstr.getPDF()
    #density = np.zeros([1388, 1612])
    matfilecontent = scipy.io.loadmat('simout/simoutput%d.mat' % (runID,))
    tasktimes = matfilecontent['resqTasks']
    exploretimes = matfilecontent['searchTasks']
    exloc = exploretimes[:,[0,1]]
    extime = exploretimes[:,2]
    task_replay = tasktimes[:,[0,1,3]]
    cmd = RescueGroup([[1080,810], [1083,796], [1069, 794], [1065, 809], [443, 396], [457, 382], [469, 393], [458, 405]], density,
                    port_start=8880+10*runID, debug = False)
    cmd.explorer.task_replay = task_replay
    cmd.explorer.discovery_locations = exloc
    cmd.explorer.discovery_times = extime
    cmd.explorer.discover_radius = 62.5
    cmd()
    cmd.comm_stop()
    print('Finished. Writing into poslog%d.mat' % (runID,))
    scipy.io.savemat('simout/poslog%d.mat' % (runID,), cmd.log)
    #command took 0:40:29.03 (2429.03s total)