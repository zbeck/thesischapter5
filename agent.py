# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 22:05:00 2015

@author: Zoli
"""
import numpy as np
import zmq
import sys
import thread
import time

MACHINE_EPSILON = 5* (7./3 - 4./3 - 1)

class CommFactory:
    """A singleton class for handling roadcast requests and creating communicator instances.
    """
    bcast_topics = {
        'stop': 's!',
        'init_comm_search': 'is',
        'init_comm_manvre': 'im',
        'delete_search_task': '-s',
        'delete_rescue_task': '-r',
        'add_task': '+t',
        'update_tasks': 'ut',
        'update_density': 'ud',
        'update_search_attitudes': 'us',
        'update_resq_attitudes': 'ur',
        'decide_search': 'ds',
        'decide_resq': 'dr'}
    msg_id = {
        'resq2resq': 'rr',
        'search2search': 'ss',
        'resq2search': 'rs',
        'search2resq': 'sr',
        'manvre_tardiness': 'sm',
        'manvre_critical': 'rm',
        'busy': 'bu'}
    topic_len = 2
    noblock_sleep = 0.1
    bcast_busy_wait_time = 3.0
    def __init__(self, connaddr, port_start, debug=False):
        #checking id message ids are unique
        all_ids = set(self.bcast_topics.values() + self.msg_id.values())
        assert(len(all_ids) == len(self.bcast_topics)+len(self.msg_id))
        
        self.next_port = port_start
        self.connaddr = connaddr
        self.context = zmq.Context()
        self.sender = self.context.socket(zmq.PUB)
        self.receiver = self.context.socket(zmq.PULL)
        self.subscribe_addr = self._get_connparam()
        self.response_addr = self._get_connparam()
        
        self.sender.bind(self.subscribe_addr)
        self.receiver.bind(self.response_addr)
        self.bcast_topics_with_response = ['decide_search', 'decide_resq']
        self.proxies = []
            
    def _get_connparam(self):
        socket = self.context.socket(zmq.PUSH)
        if self.connaddr[:6] == 'tcp://':
            port = socket.bind_to_random_port(self.connaddr[:-1])
            connparam = self.connaddr+str(port)
            socket.unbind(connparam)
        elif self.connaddr[:6] == 'ipc://':
            connparam = self.connaddr+str(self.next_port)
            self.next_port += 1
        return connparam
        
    def _is_busy_response(self, msg):
        return msg[:self.topic_len] == self.msg_id['busy']
    
    def bcast(self, topic, data=''):
        if not isinstance(data, basestring):
            try:
                data = data.tostring()
            except AttributeError:
                try:
                    data = ''.join([a.tostring() for a in data])
                except AttributeError:
                    try:
                        data = np.array(data, np.float).tostring()
                    except:
                        raise TypeError('Data argument must be string, list, or numpy.ndarray!')

        if topic in self.bcast_topics_with_response:
            '''
            in case of a brodcast with response, all nodes will asnwer a
            busy message as they recieve the broadcast message
            so we initially listen for responses,
            then count if twice as many responses came as buy responses
            '''
            self.sender.send(self.bcast_topics[topic]+data)
            responses = []
            busy_nodes = -1
            while len(responses) != busy_nodes*2:
                #initially wait for response for a couple seconds
                if busy_nodes < 0:
                    till = time.time()+self.bcast_busy_wait_time
                    while time.time()<till:
                        try:
                            msg = self.receiver.recv(zmq.NOBLOCK)
                            responses.append(msg)
                            break
                        except zmq.ZMQError as e:
                            if e.errno != zmq.EAGAIN:
                                self.commstop()
                                raise e
                            else:
                                time.sleep(self.bcast_busy_wait_time/10.)
                else:
                    responses.append(self.receiver.recv())
                busy_nodes = sum([self._is_busy_response(r) for r in responses])
            responses = [r for r in responses if not self._is_busy_response(r)]
            return sorted(responses)
        elif topic in self.bcast_topics:
            self.sender.send(self.bcast_topics[topic]+data)
            
    def make_state(self, attitude):
        return AgentState(self.context, attitude, self._get_connparam())
        
    def create_publish_proxy(self):
        subscriber = self.context.socket(zmq.XSUB)
        publisher = self.context.socket(zmq.XPUB)
        subaddr = self._get_connparam()
        pubaddr = self._get_connparam()
        subscriber.bind(subaddr)
        publisher.bind(pubaddr)
        self.proxies.append(PubSubProxy(subscriber, publisher))
        return subaddr, pubaddr
        
    def commstop(self):
        for p in self.proxies:
            p.commstop()
        
class PubSubProxy:
    def __init__(self, subsocket, pubsocket):
        self.subsocket = subsocket
        self.pubsocket = pubsocket
        self.run = True
        thread.start_new_thread(self.thread_fcn, ())
        
    def thread_fcn(self):
        while self.run:
            try:
                msg = self.subsocket.recv(zmq.NOBLOCK)
                self.pubsocket.send(msg)
            except zmq.ZMQError as e:
                if e.errno != zmq.EAGAIN:
                    self.commstop()
                    raise e
                else:
                    time.sleep(CommFactory.noblock_sleep)
        
    def commstop(self):
        self.run = False
            

class CommAgent:
    """The base communicator class for a search or rescue agent.
    """
    def __init__(self, state, bcast_addr, reply_addr, topics=['stop'], context=None, debug=False):
        if context is None:
            self.context = zmq.Context()
        else:
            self.context = context
        self.bcast_recv = self.context.socket(zmq.SUB)
        self.bcast_reply = self.context.socket(zmq.PUSH)
        self.direct_recv = self.context.socket(zmq.PULL)        
        self.debug = debug
        if self.debug:
            print 'Socket created'         
        #Bind socket to local host and port
        try:
            self.bcast_recv.connect(bcast_addr)
            self.bcast_reply.connect(reply_addr)
            self.direct_recv.bind(state.connParam)
        except zmq.ZMQBindError as e:
            print 'Bind failed. Error: ' + str(e)
            sys.exit()
        if self.debug:
            print 'Socket bind complete'
        for topic in topics:
            if not topic in CommFactory.bcast_topics:
                raise RuntimeError('Invalid topic given!')
            self.bcast_recv.setsockopt(zmq.SUBSCRIBE, CommFactory.bcast_topics[topic])
        self.run = True
        thread.start_new_thread(self.bcast_recv_thread, ())
        thread.start_new_thread(self.direct_recv_thread, ())
        
    def bcast_recv_thread(self):
        while self.run:
            try:
                msg = self.bcast_recv.recv(zmq.NOBLOCK)
                self.recvmessage(self.bcast_reply, msg)
            except zmq.ZMQError as e:
                if e.errno != zmq.EAGAIN:
                    self.commstop()
                    raise e
                else:
                    time.sleep(CommFactory.noblock_sleep)
                    
    def direct_recv_thread(self):
        while self.run:
            try:
                msg = self.direct_recv.recv(zmq.NOBLOCK)
                self.recvmessage(None, msg)
            except zmq.ZMQError as e:
                if e.errno != zmq.EAGAIN:
                    self.commstop()
                    raise e
                else:
                    time.sleep(CommFactory.noblock_sleep)
        
    def commstop(self):
        self.run = False
        
    def recvmessage(self, conn, data):
        msg_type = data[:CommFactory.topic_len]
        if msg_type == CommFactory.bcast_topics['stop']:
            self.commstop()
        else:
            self.processmessage(conn, data[CommFactory.topic_len:], msg_type)
            
    def processmessage(self, conn, data, msg_type):
        pass
    
    @staticmethod        
    def sendSocketMessage(conn, data, msg_type):
        #tim = time.time()
        if not isinstance(data, basestring):
            try:
                data = data.tostring()
            except AttributeError:
                try:
                    data = ''.join([a.tostring() for a in data])
                except AttributeError:
                    try:
                        data = np.array(data, np.float).tostring()
                    except:
                        raise TypeError('Data argument must be string, list, or numpy.ndarray!')
        #print('prep:%.2f' % (time.time()-tim))
        try:
            conn.send(msg_type+data)
            #print('sent:%.2f' % (time.time()-tim,))
        except zmq.error:
            print('socket closed')
            return False
        return True
        
class AgentState:
    """A class responsible for communication to an agent."""
    def __init__(self, context, attitude, connparam):
        self.context = context
        self.attitude = attitude
        self.socket = self.context.socket(zmq.PUSH)
        self.connParam = connparam
        self.is_connected = False
    
    @staticmethod
    def copy(otherState):
        assert(isinstance(otherState, AgentState))
        return AgentState(otherState.context, otherState.attitude, otherState.connParam)
    
    def connectSocket(self):
        if not self.is_connected:
            self.socket.connect(self.connParam)
            self.is_connected = True
        
    def send(self, data, msg_type):
        return CommAgent.sendSocketMessage(self.socket, data, msg_type)
        
    def tostring(self):
        retval = self.attitude.tostring()
        integers = np.array([len(self.connParam)], np.int)
        retval += integers.tostring()
        retval += self.connParam
        return retval
        
    @staticmethod
    def fromstring(data, context):
        if context is None:
            context = zmq.Context()
        retlist = []
        while len(data) > 0:
            att_array = np.fromstring(data, np.float, Attitude.array_size)
            att = Attitude.fromarray(att_array)
            data = data[len(att_array.tostring()):]
            paramlen = np.fromstring(data, np.int, 1)
            data = data[len(paramlen.tostring()):]
            connparam = data[:paramlen]
            data = data[paramlen:]
            retlist.append(AgentState(context, att, connparam))
        return retlist
        
class Attitude:
    def __init__(self, location, speed, batteryTime):
        self.loc = np.array(location, np.float)
        self.speed = np.array(speed, np.float)
        self.batteryTime = batteryTime
        self.waitTime = 0.0
        self.currentTask = None
        self.waitEstimateError = 0.0
    
    def getHeading(self):
        return np.arctan2(self.speed[1], self.speed[0])
        
    def getPosHeading(self):
        return list([self.loc[0], self.loc[1], np.arctan2(self.speed[1], self.speed[0])])
        
    def getDefaultInstruction(self):
        return self.loc+self.speed*(self.batteryTime/10.0)
        
    def estimatedWaitTime(self):
        return max(0., self.waitTime + self.waitEstimateError)
        
    def tostring(self):
        if self.currentTask is None:
            ct = [np.inf, np.inf]
        else:
            ct = self.currentTask
        data = np.concatenate((self.loc, self.speed, [self.batteryTime,
                                self.waitTime, self.waitEstimateError], ct))
        return data.tostring()
        
    @staticmethod
    def fromstring(data):
        att_array = np.fromstring(data, np.float)
        att_array.shape = (-1, Attitude.array_size)
        return [Attitude.fromarray(a) for a in att_array]
        
    @staticmethod
    def fromarray(data):
        """input: [loc_x, loc_y, speed_x, speed_y, battery_time, wait_time, wait_estimate_error, current_task_x, current_task_y]"""
        instance = Attitude(data[:2], data[2:4], data[4])
        instance.waitTime = data[5]
        instance.waitEstimateError = data[6]
        if not np.all(np.isinf(data[7:])):
            instance.currentTask = data[7:]
        return instance
        
    array_size = 9
        
        
def dist(a,b):
    return np.linalg.norm(a-b)
    
def direction(a,b):
    if np.all(a==b):
        return b-a
    return (b-a)/dist(a,b)
    
def closest_point(point, points):
    diff = np.asarray(points) - np.asarray(point)
    dist_2 = np.einsum('ij,ij->i', diff, diff)
    return np.argmin(dist_2)
    
def closest_nonzero_dist(point, points):
    diff = np.asarray(points) - np.asarray(point)
    dist_2 = np.einsum('ij,ij->i', diff, diff)
    dist_2 = np.ma.masked_less_equal(dist_2, MACHINE_EPSILON)
    return np.sqrt(np.min(dist_2))
    
def angle_rng_pi(anglerad):
    return -(( np.pi - anglerad) % (2 * np.pi )) + np.pi
    
def angle_rng_2pi(anglerad):
    return anglerad % (2 * np.pi )


    
if __name__ == "__main__":
    pts = [[0,0],[0,2],[2,2],[0,4]]
    print(min(1., closest_nonzero_dist(pts[2], [pts[2]])/2.3))
    print('end')
#    ca = CommAgent(('localhost', 8888), True)
#    while ca.run:
#        pass
