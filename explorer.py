# -*- coding: utf-8 -*-
"""
Created on Thu May 28 11:19:31 2015

@author: Zoli
"""

from ProblemFactory import Distribution2D
from constants import DICOVER_RADIUS
import numpy as np


class Explorer:
    class DiscoverArea:
        rectangle, circle = range(2)
    def __init__(self):
        #we discover either by location and radius
        self.gt_tasks = []
        self.discovered_tasks = []
        #or by an already done experiment
        self.task_replay = []
        
        self.time = 0
        self.discovery_locations = []
        self.discovery_times = []
        self.distribution = None
        self.discover_shape = self.DiscoverArea.rectangle
        self.discover_radius = DICOVER_RADIUS
        
    def init_distribution(self, density):
        self.distribution = Distribution2D(density)
        for t in self.discovered_tasks:
            self.gt_tasks.append(t)
        self.discovered_tasks = []
        
    def get_density(self):
        return self.distribution.getPDF()
    
    def is_discovered_by(self, task, location):
        if self.discover_shape == self.DiscoverArea.rectangle:
            diff = np.array(task) - np.array(location)
            return (abs(diff[0]) < self.discover_radius) and (abs(diff[1]) < self.discover_radius)
        return False
            
    def discover(self, location):
        if self.discover_shape == self.DiscoverArea.rectangle:
            radius = np.array([self.discover_radius, self.discover_radius], np.float)
            loc = np.array(location, np.float)
            rect = [loc+radius, loc-radius]
            if isinstance(self.distribution, Distribution2D):
                self.distribution.clearRectangle(rect)
            current_discovery = []
            for t in self.gt_tasks:
                if self.is_discovered_by(t, location):
                    current_discovery.append(t)
            for t in current_discovery:
                self.gt_tasks.remove(t)
            self.discovered_tasks += current_discovery
            return current_discovery
        
    def step_to_time(self, timePoint):
        discovered = []
        for i in range(len(self.discovery_times)):
            if (self.discovery_times[i] <= timePoint) and (self.discovery_times[i] > self.time):
                discovered += self.discover(self.discovery_locations[i])
        for item in self.task_replay:
            t_i = item[2]
            if (t_i <= timePoint) and (t_i > self.time):
                discovered.append(np.array([item[0],item[1]], np.float))
        self.time = timePoint
        return discovered
        
if __name__ == "__main__":
    er = Explorer()
    er.discovery_locations.append([2,3])
    er.discovery_times.append(1.1)
    er.discovery_locations.append([3,4])
    er.discovery_times.append(2.1)
    er.init_distribution(Distribution2D(np.random.rand(5,7)))
    er.discover_radius = 1.5
    print(er.distribution.getPDF())
    er.step_to_time(2.0)
    print(er.distribution.getPDF())
    er.step_to_time(3.0)
    print(er.distribution.getPDF())