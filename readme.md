# Code related to the thesis Collaborative Search and Rescue by Autonomous Robots
This repository contains the code used for the evaluation of the Chapter 5 of the thesis *Collaborative Search and Rescue by Autonomous Robots* by Zoltan Beck.
[The full thesis can be found here.](ZoltanBeckThesisWebFinal.pdf)

# Details about the code structure

Most important classes:


 +  __Commander__ (*commander.py*): Responsible for creating and running the complete simulation.

 +  __CommFactory__ (*agent.py*): A singleton class for handling broadcast requests and creating communicator instances.

 +  __AgentGroup__ (*agentgroup.py*): Abstract class for a group of homogeneous agents.

 +  __SearchGroup__ (*searchgroup.py*): Group of search agents. The method `run_agent_scherri_search()` and `run_agent_manvre()` are responsible for the two path-planning approaches (Section 5.3.2.1 and 5.3.2.2 respectively).

 +  __RescueGroup__ (*rescuegroup.py*): Group of rescue agents.

 +  __NoSweepPlanner__ (*sweep.py*): Route planner for fixed-wing UAVs passing through specific locations.

 +  __SensorSim__ (*sensor_sim.py*): Simulates and models GSM signal detection (Section 5.2).

 +  __FixedWing__ (*fixedsimulator.py*): Simulates a fixed-wing UAV's motion following waypoints in 2D.

 +  __Quad__ (*quadsimulator.py*): Simulates a small rotary-wing UAV's motion following waypoints in 2D.

 +  __CommAgent__ (*agent.py*): Abstract class for communicating agents able to receive messages of specific topics.
 
 +  __Searchr__ (*Searchr.py*): A search planning agent.

 +  __ResQr__ (*ResQr.py*):	A rescue planning agent.

 +  __ManoeuvrePlanner__ (*manvre.py*): An agent responsible for collecting, processing and providing information for short-term path planning for search (Section~\ref{sec:applManvre*).
 
 +  __DeviateRouteSate__ (*manvre.py*): A class used by a third-party MTCS planner for path planning purposes.
 
 +  __MultiTargetBeliefs__ (*sensor_sim.py*): Maintains updates about mobile phones according to Section 5.2.
 
 +  __AgentState__ (*agent.py*): A class for exchanging and preserving details for direct communication to agents.
 
 +  __SearchProblem__ (*problem.py*): Contains a search MRTA problem and is able to solve it with the methods discussed in Section 3.2.
 
 +  __ProblemFactory__ (*ProblemFactory.py*): Contains an UMRTA rescue problem and is able to provide HOP sample MRTA problems.
 
 +  __RescueProblem__ (*problem_old.py*): Contains a hindsight rescue MRTA problem and is able to solve it with the methods discussed in Section 3.2.


![classdiagram](UMLPython.png)