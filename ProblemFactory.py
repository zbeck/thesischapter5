# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 20:29:27 2015

@author: Zoli
"""
from bisect import bisect_left
import numpy as np
from problem_old import RescueProblem
from agent import closest_point
from constants import RW_TARGETDISTANCE

class ProblemFactory:
    def __init__(self, agents, density, tstay, speed, fix=[], gradient=None):
        self.TARGETDISTANCE = RW_TARGETDISTANCE
        self.agents = agents
        self.DEFAULT_STAY_TIME = float(tstay)
        self.fixTasks = []
        for t in fix:
            self.add_task(t)
        if density is not None:
            self.distribution = Distribution2D(density)
        else:
            self.distribution = None
        self.gradient = gradient
        self.SPEED = speed
        self.discovery = []
        
    def getExpectedVal(self):
        if self.distribution is None:
            return 0.
        return self.distribution.getExpectedVal()
    
    def updateDensity(self, density):
        self.distribution = Distribution2D(density)
        
    def setDiscoveryActivation(self, discovery):
        self.discovery = np.array(discovery, np.float)
        
    def sampleTask(self):    
        [rndx, rndy] = np.random.uniform(size=2)
        task = self.distribution.sample(rndx, rndy)
        return np.concatenate((task,[self.DEFAULT_STAY_TIME]))
        
    def sampleTasks(self):
        if self.distribution is not None:
            pointNum = np.random.poisson(self.distribution.getExpectedVal())
            if len(self.discovery) > 0:
                points_with_activations = [np.concatenate((t, [0])) for t in self.fixTasks] + [self.add_activaition(self.sampleTask()) for i in range(pointNum)]
                return points_with_activations
            else:
                points = self.fixTasks[:] + [self.sampleTask() for i in range(pointNum)]
                return points
        else:
            if len(self.discovery) > 0:
                points_with_activations = [np.concatenate((t, [0])) for t in self.fixTasks]
                return points_with_activations
            else:
                return self.fixTasks[:]
            
        
    def add_activaition(self, sampledpoint):
        discovery_points = self.discovery[:,0:2]
        closestid = closest_point(sampledpoint[:2], discovery_points)
        return np.concatenate((sampledpoint, [self.discovery[closestid, -1]]))
        
    def getProblem(self, pid):
        tasks = self.sampleTasks()
        rp = RescueProblem(self.agents, tasks, len(self.fixTasks), self.SPEED)
        return rp
        
    def makeTargetFromVector(self, vec, agentID):
        if np.all(vec == 0.):
            if self.gradient is None:
                unit_vec = vec
            else:
                vec_dir = self.gradient[self.agents[agentID].loc]
                unit_vec = np.array([np.cos(vec_dir), np.sin(vec_dir)])
        else:
            unit_vec = vec / np.linalg.norm(vec)
        return self.agents[agentID].loc + unit_vec*self.TARGETDISTANCE
        
    def makeTargetFromTask(self, taskID):
        return np.array(self.fixTasks[taskID], np.float)
        
    def remove_task(self, task):
        fixlen = len(self.fixTasks)
        self.fixTasks = [t for t in self.fixTasks if not (t[0]==task[0] and t[1]==task[1])]
        if fixlen != len(self.fixTasks)+1:
            return False
        return True
        
    def add_task(self, task):
        if len(task) == 2:
            self.fixTasks.append(np.concatenate((task,[self.DEFAULT_STAY_TIME])))
        else:
            self.fixTasks.append(np.asarray(task, np.float))
        
    def isCompleted(self):
        if len(self.fixTasks) > 0:
            return False
        if self.distribution.getExpectedVal() > 0:
            return False
        return True
       
class Distribution2D:
    def __init__(self, density):
        if not isinstance(density, Density):
            self.pos_scaler = 1.0
            self.pos_offset = np.array([0., 0.])
        else:
            self.pos_scaler = density.scaler
            self.pos_offset = density.offset
            density = density.d
        self.cols = []
        for col in density:
            self.cols.append(Distribution1D(col))
        self.buildRowDist();
    
    def buildRowDist(self):
        rowDensity = []
        for col in self.cols:
            rowDensity.append(col.getExpectedVal())
        self.rowDist = Distribution1D(rowDensity)
        
    def getExpectedVal(self):
        return self.rowDist.getExpectedVal()
        
    def setExpectedVal(self, expVal):
        scaleFactor = expVal / self.getExpectedVal()
        self.scale(scaleFactor)
        
    def sample(self, rndx, rndy):
        col = self.rowDist.sample(rndx)
        row = self.cols[col].sample(rndy)
        cell = np.array([col, row], np.float)
        return cell*self.pos_scaler + self.pos_offset
        
    def scale(self, factor):
        for col in self.cols:
            col.scale(factor)
        self.buildRowDist()
        
    def clearRectangle(self, rect):
        rect = (np.asarray(rect) - self.pos_offset) / self.pos_scaler
        p0 = np.array(rect[0], np.int)
        p1 = np.array(rect[1], np.int)
        rngx = range(max(min(p0[0], p1[0]), 0), min(max(p0[0], p1[0]), len(self.cols)))
        rngy = range(max(min(p0[1], p1[1]), 0), min(max(p0[1], p1[1]), len(self.cols[0].cdf)))
        for row in rngx:
            self.cols[row].clearRange(rngy)
        self.buildRowDist()
        
    def getPDF(self):
        pdf = np.array([c.getPDF() for c in self.cols], np.float64)
        return pdf
        
    def getDensity(self):
        return Density(self.getPDF(), self.pos_scaler, self.pos_offset)
        
        
class Distribution1D:
    def __init__(self, density):
        cdf = []
        sumVal = 0
        for d in density:
            sumVal = sumVal + d
            cdf.append(sumVal)        
        self.cdf = np.array(cdf, np.float64)
            
    def getExpectedVal(self):
        return self.cdf[-1]
        
    def sample(self, rnd):
        return bisect_left(self.cdf, rnd*self.cdf[-1])
        
    def scale(self, factor):
        self.cdf = self.cdf * factor
        
    def clearRange(self, rng):
        if not rng:
            return
        if rng[0] == 0:
            val = 0
        else:
            val = self.cdf[rng[0]-1]
        decrease = self.cdf[rng[-1]] - val
        self.cdf[[rng]] = val
        self.cdf[(rng[-1]+1):] = np.clip(self.cdf[(rng[-1]+1):]-decrease, 0, np.inf)

    def getPDF(self):
        return np.concatenate(([self.cdf[0]], self.cdf[1:]-self.cdf[:-1]))
        
class Density:
    def __init__(self, density, scaler=1., offset=np.array([0.,0.])):
        self.d = np.asanyarray(density)
        self.scaler = float(scaler)
        self.offset = np.asarray(offset, np.float)
        
    def __getitem__(self, index):
        if len(index) != len(self.d.shape):
            raise IndexError('Index dimension mismatch with density array.')
        index = np.asarray(index)
        offset = self.offset
        clip = np.asarray([self.d.shape[0]-1, self.d.shape[1]-1])
        while len(index.shape) > len(offset.shape):
            offset = offset[:,np.newaxis]
            clip = clip[:,np.newaxis]
        dsc_idx = np.array((index - offset) / self.scaler, np.int)
        dsc_idx = np.clip(dsc_idx, 0, clip)
        return self.d[tuple(dsc_idx)]
        
    def repr_value(self):
        return np.mean(self.d[self.d>0.])
        
    def exp_value(self):
        return np.sum(self.d)
        
    def tostring(self):
        retval = np.concatenate(([self.scaler], self.offset)).tostring()
        retval += np.array(self.d.shape, np.int).tostring()
        retval += self.d.tostring()
        return retval
        
    @staticmethod
    def fromstring(data):
        if len(data) == 0:
            return None
        params = np.fromstring(data, np.float, 3)
        data = data[len(params.tostring()):]
        shape = np.fromstring(data, np.int, 2)
        data = data[len(shape.tostring()):]
        density = np.fromstring(data, np.float).reshape(shape)
        return Density(density, params[0], params[1:])
        
    def plot(self, ax):
        size = np.asarray(self.d.shape, np.float) * self.scaler
        extent = np.concatenate((self.offset[0]+[0, size[0]], self.offset[1]+[0, size[1]]))
        ax.imshow(self.d, extent=extent)
        
        
def test_density():
    d=np.arange(9).reshape(3,3)
    print(d)
    dd=Density(d)
    print(dd[np.array([0,1]),np.array([0,2])])
    
def test_problem_factory():
    print('Testing ProblemFactory...')
    print('test density:')
    density = [[1,2,2], [3,1,0], [1,3,2]]
    print(density)
    pf = ProblemFactory(density)
    print('expected value:')
    Eval = 2
    print(Eval)
    pf.setExpectedVal(Eval)
    pts = []
    print('taking 1000 samples...')
    for i in range(1000):
        pts = pts+pf.sampleTasks()
    print('number of points per position:')
    for x in range(3):
        for y in range(3):
            print(str(pts.count([x,y]))+'\t'),
        print('')
    print('overall number of points')
    print(len(pts))
        
if __name__ == "__main__":
    test_density()