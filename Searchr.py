# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 20:26:26 2015

@author: Zoli
"""
import numpy as np
from problem import SearchProblem
from agent import CommFactory, CommAgent, AgentState, Attitude, closest_point
from constants import FW_STAY_TIME, FW_CRUISE_SPEED, EPSILON
from operator import itemgetter
import time
import threading
import sys

def searchr_process(ID, state, rescueAgentStates, searchAttitudes, tasks, config_set, bcast_addr, reply_addr, context=None, task_time=FW_STAY_TIME, externalDistanceFunction=None, task_pos_convert_fcn=None, debug=False):
    st = AgentState.fromstring(state, context)[0]
    context = st.context
    rstates = AgentState.fromstring(rescueAgentStates, context)
    satt = Attitude.fromstring(searchAttitudes)
    searchr = Searchr(ID, st, rstates, satt, tasks, config_set, bcast_addr, reply_addr, context, task_time, externalDistanceFunction, task_pos_convert_fcn, debug)
    searchr.initSockets()
#    cntr = 0
    while searchr.run:
        time.sleep(0.1)
#        if cntr % 100 == 50+ID*3:
#            print('src%d'%(ID,))
#        cntr += 1

class Searchr(CommAgent):
    def __init__(self, ID, state, rescueAgentStates, searchAttitudes, tasks, config_set, bcast_addr, reply_addr, context=None, task_time=FW_STAY_TIME, externalDistanceFunction=None, task_pos_convert_fcn=None, debug=False):
        CommAgent.__init__(self, state, bcast_addr, reply_addr,
            ['stop', 'init_comm_manvre', 'delete_search_task', 'update_search_attitudes', 'decide_search'], context, debug)
        self.tasks = np.array(tasks, np.float)
        self.task_time = task_time
        self.attitudes = searchAttitudes
        self.nagents = len(searchAttitudes)
        self.id = ID
        self.rescueAgentStates = []
        for s in rescueAgentStates:
            self.rescueAgentStates.append(AgentState.copy(s))
        self.useDebugExternalProblem = False
        self.dist_fcn = externalDistanceFunction
        self.task_convert_fcn = task_pos_convert_fcn
        self.feedback = None
        self.useFeedback = False
        self.feedbackLock = threading.RLock()
        self.resetFeedback()
        self.manvre_states = []
        self.schedule = []
        self.configurations = config_set
        self.taskDistanceTable = None
        
    def initSockets(self):
        for st in self.rescueAgentStates:
            if st:
                st.connectSocket()
            
    def initManvreComm(self, manvre_states):
        self.manvre_states = manvre_states
        for st in self.manvre_states:
            st.connectSocket()
        
    def resetFeedback(self):
        with self.feedbackLock:
            self.useFeedback = False
            self.feedback = [[] for t in self.tasks]
            
    def pruneFeedback(self, pruneCount=20000):
        with self.feedbackLock:
            if sum([len(fb) for fb in self.feedback]) > pruneCount:
                if self.debug:
                    print('pruned %d size feedback' % (sum([len(fb) for fb in self.feedback]),))
                for fb in self.feedback:
                    #remove every second element
                    fb = [fb[i] for i in range(0, len(fb), 2)]

    #OBSOLETE!
#    def debugMakeCustomProblem(self, distanceTable):
#        self.useDebugExternalProblem = True
#        self.makeProblem()
#        self.problem.distanceTable = distanceTable
        
    def makeProblem(self):
        self.problem = SearchProblem(self.attitudes,
            np.concatenate((self.tasks, np.repeat([[self.task_time]], len(self.tasks), 0)),axis=1),
            len(self.tasks), FW_CRUISE_SPEED, self.dist_fcn, self.configurations, self.taskDistanceTable)
        if self.taskDistanceTable is None:
            self.taskDistanceTable = self.problem.distanceTable
        
    def solveProblem(self):
        if not self.useDebugExternalProblem:
            self.makeProblem()
        if self.debug:
            print(self.problem)
        if self.useFeedback:
            with self.feedbackLock:
                for tfb in self.feedback:
                    tfb.sort(key=itemgetter(0))
                if self.debug:
                    print('search feedback size:%d' % (sum([len(fb) for fb in self.feedback]),))
                schedule = self.problem.getFeedbackSchedule(self.feedback)
                self.resetFeedback()
        else:
            _, _, schedule = self.problem.getGreedyInstructions()
        if self.debug:
            print('Problem solved.')
        self.schedule = schedule[self.id]
        return schedule
        
    def mainCycle(self, sendToRescue, sendToManvre):
        if self.debug:
            print('Cycle starts (search)...')
        overall_schedule = self.solveProblem()
        if sendToRescue:
            self.sendScheduleToRescue(overall_schedule)
            if self.debug:
                print('Schedule sent to rescue agents.')
        if sendToManvre:
            self.sendTardinessToManvre()
            if self.debug:
                print('Tardiness data sent to Manoeuvre agent.')
        if len(self.schedule) < 2:
            target = np.concatenate((self.tasks[[self.schedule[0:2]]], [[np.inf,np.inf] for i in range(2-len(self.schedule))]), axis=0)
        else:
            target = np.array(self.tasks[[self.schedule[0:2]]])
        if self.debug:
            print('Next targets:')
            print(target)
        return target
        
    def sendScheduleToRescue(self, schedule):
        if len(self.tasks) == 0:
            return
        execution = self.problem.getExecutionTimes(schedule)
        try:
            real_task_pos = [self.task_convert_fcn(t) for t in self.tasks]
        except:
            real_task_pos = self.tasks
        data = np.concatenate((real_task_pos, execution[:,np.newaxis]), axis=1).tostring()
        if self.debug:
            print('Sent ', len(data))
        for state in self.rescueAgentStates:
            state.send(data, CommFactory.msg_id['search2resq'])
            
    def sendTardinessToManvre(self):
        #sending tardiness data to manvre agents if there is any
        if len(self.manvre_states) > 0:
            with self.feedbackLock:
                tardyCount, nonTardyDiffList = self.problem.getTardinessList(self.schedule, self.feedback)
            data = np.array([tardyCount], np.int).tostring()
            data += np.array(nonTardyDiffList, np.float).tostring()
            self.manvre_states[self.id].send(data, CommFactory.msg_id['manvre_tardiness'])
                
    def processmessage(self, conn, data, msg_type):
        if msg_type == CommFactory.msg_id['resq2search']:
            self.process_feedback_message(data)
        elif msg_type == CommFactory.bcast_topics['decide_search']:
            conn.send(CommFactory.msg_id['busy']+chr(ord('A')+self.id))
            args = np.fromstring(data, np.int, 2)
            target = self.mainCycle(args[0], args[1])
            conn.send(CommFactory.bcast_topics['decide_search'] +
#                np.array([self.id],np.int).tostring() + target.tostring())
                np.array([self.id],np.int).tostring() + np.concatenate((target.flatten(),[time.time()])).tostring())
        elif msg_type == CommFactory.bcast_topics['delete_search_task']:
            self.remove_task(np.fromstring(data, np.float))
        elif msg_type == CommFactory.bcast_topics['update_search_attitudes']:
            self.attitudes = Attitude.fromstring(data)
            if self.attitudes[self.id].batteryTime <= 0:
                self.commstop()
        elif msg_type == CommFactory.bcast_topics['init_comm_manvre']:
            manvre_states = AgentState.fromstring(data, self.context)
            self.initManvreComm(manvre_states)
            
    def process_feedback_message(self, data):
        searchExecutions = np.fromstring(data, np.float)
        searchExecutions = searchExecutions.reshape(-1,4)
        if self.debug:
            print('Received %d feedback tasks' % (len(searchExecutions),))
        with self.feedbackLock:
            self.useFeedback = True
            if len(self.tasks) > 0:
                try:
                    real_task_pos = [self.task_convert_fcn(t) for t in self.tasks]
                except:
                    real_task_pos = self.tasks
                for e in searchExecutions:
                    taskid = closest_point(e[0:2], real_task_pos)
                    self.feedback[taskid].append( (e[2],e[3]) )
            
    def remove_task(self, task):
        index = np.argmin(np.sum(np.abs(self.tasks - task), 1))
        if np.max(np.abs(self.tasks[index] - task)) > EPSILON:
            return False
        self.tasks = np.delete(self.tasks, index, 0)
        if self.taskDistanceTable is not None:
            self.taskDistanceTable = np.delete(np.delete(self.taskDistanceTable, index, 2), index, 1)
#        if len(self.tasks) == 0:
#            self.commstop()
        return True
            

if __name__ == "__main__":
    import feedbackTestData
    from ResQr import ResQr
    sagents = [Attitude([1147,940], [FW_CRUISE_SPEED,0], 216000), Attitude([409,404], [0,FW_CRUISE_SPEED], 216000)]
    ragents = [Attitude([0,0], [0,0], 216000)]
    tasks = feedbackTestData.tasks
    fb = feedbackTestData.feedback
    distanceTable = feedbackTestData.distanceTable
    
    testID = int(sys.argv[1])
    #rq = ResQr(0, [AgentState(Attitude([0,0], 0, 1), 'localhost', 8888)], [[1, 1],[0, 1]])
    sstates = [AgentState(sagents[0], 'localhost', 8888),
              AgentState(sagents[1], 'localhost', 8889)]
    rstates = [AgentState(ragents[0], 'localhost', 8887)]
    if testID >= 2:
        density = np.zeros((10, 10))
        rq = ResQr(0, rstates, debug=1)
        rq.update_problem([[1,2],[3,4]], density)
        time.sleep(12)
    else:
        se = Searchr(testID, sstates[testID], rstates, sagents, tasks, debug=1)
        se.debugMakeCustomProblem(distanceTable)
        se.initSockets()
        se.mainCycle(True)
        se.commstop()
        time.sleep(2)
        