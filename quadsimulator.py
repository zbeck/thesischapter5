# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:18:35 2015

@author: Zoli
"""
from agent import Attitude, dist, direction
from constants import RW_STAY_TIME, RW_MAX_ACC, RW_MAX_SPEED
import time
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import matplotlib.animation as animation
plt.rcParams['animation.ffmpeg_path'] = 'c:/Anaconda/Lib/ffmpeg/bin/ffmpeg.exe'

class Quad:
    def __init__(self, attitude, debug=False):
        self.debug = debug
        self.MAX_ACC = RW_MAX_ACC
        self.MAX_SPEED = RW_MAX_SPEED
        self.WIND_SPEED_RMS = 1.0
        self.WIND_SPEED_CONSTANT = np.array([0.5, 0], np.float)
        if not isinstance(attitude, Attitude):
            raise TypeError('attitude needs to have agent.Attitude type')
        self.att = attitude
        self.steptime = 0.1
        self.target = attitude.loc
        self.target_wait = 0.
        self.target_area = 0.
        self.rescue_timer = None
        self.wind_random_component = np.array([0.1, -0.2], np.float)
        
    def set_rescue_timer(self, rescue_timer):
        self.rescue_timer = rescue_timer
        
    def get_wind(self):
        #!TODO! recalculate random wind
        return self.WIND_SPEED_CONSTANT + self.wind_random_component
        
    def set_target(self, target):
        """Sets a target.
        
        Parameters
        ---------
        target: np.ndarray [x, y, <wait_time>]
            Target position is at x, y, and the wait time can be given as the third element of the array, default is 0.
            Is there is a rescue timer set, wait_time is ignored, and the time is acquired from the rescue timer."""
        self.target_id = -1
        self.target = np.array(target[:2], np.float)
        if len(target) > 2:
            self.target_area = target[2]
        else:
            self.target_area = 0.
        if self.rescue_timer is None:
            if len(target) > 2:
                self.target_wait = target[2]
            else:
                self.target_wait = 0.
        else:
            self.target_wait, self.target_id = self.rescue_timer.get_rescue_time(self.target)
        #if the task stays the same (restarting task)
        if self.att.currentTask is not None and self.target_id == self.att.currentTask[0]:
            self.att.waitTime = 0.
            self.att.waitEstimateError = 0.
            #currently heading to new position, not finished task
            self.att.currentTask = None
        
    def step(self, dt):
        if self.att.batteryTime <= 0:
            return False, False
        reached_target = False
        finished_target = False
        self.att.batteryTime -= dt
        self.att.waitTime -= dt
        #if currently not waiting
        if self.att.waitTime < 0:
            if self.att.currentTask is not None:
                if self.att.currentTask[0] >= 0:
                    finished_target = [self.att.currentTask[0]]
                self.att.currentTask = None
            dt = -self.att.waitTime
            dt = self.move_to_target(dt)
            #if reached location
            if dt > 0:
                self.att.waitTime = self.target_wait - dt
                self.att.waitEstimateError = self.target_area - self.target_wait
                self.att.currentTask = np.array([self.target_id, 0.], np.float)
                reached_target = np.concatenate((self.target, [self.target_wait, self.target_area, self.target_id]))
                self.target_id = -1
                if self.debug:
                    print('target reached:')
                    print(self.target)
                    print('location:')
                    print(self.att.loc)
                    print('stay time:')
                    print(self.att.waitTime)
            if self.att.waitTime < 0:
                self.att.waitTime = 0.0
        return reached_target, finished_target
        
    def reach_speed(self, dt, v_targ=np.array([0,0], np.float)):
        if dt <= 0.0:
            return 0.0
        t_acc = dist(v_targ, self.att.speed)/self.MAX_ACC
        if self.debug:
            print('tacc:'+str(t_acc))
        if t_acc > dt:
            #if acceleration to the target speed takes longer than dt,
            # do max acceleration towards the target speed vector
            ratio = dt/t_acc
            v_targ = self.att.speed*(1-ratio) + v_targ*ratio
            t_acc = dt
            if self.debug:
                print('vtarg:'+str(np.linalg.norm(v_targ))+'('+str(v_targ)+')')
        #motion: max acceleration (deceleration) to target speed and
        oldloc = np.array(self.att.loc)
        self.att.loc += (self.att.speed+v_targ)*.5*t_acc
        if dist(oldloc, self.att.loc) > 2*RW_MAX_SPEED*0.1:
            raise RuntimeError('Error! Position jump happened')
        self.att.speed = v_targ
        dt -= t_acc
        return dt
        
    def hold_speed(self, dt):
        if dt <= 0.0:
            return 0.0
        d_target = dist(self.att.loc, self.target)
        d_break = np.linalg.norm(self.att.speed)**2 / self.MAX_ACC * 0.5
        vec_movement = self.att.speed*dt
        if self.debug:
            print('d'+str(d_target)+' b'+str(d_break)+' v'+str(np.linalg.norm(vec_movement)))
        if d_target < d_break: #something's wrong and we're going to run over, there should be no speed hold
            vec_movement = np.array([0,0], np.float)
            #dt doesn't change
        elif d_target - d_break < np.linalg.norm(vec_movement): #hold a bit and then brake
            ratio = (d_target - d_break) / np.linalg.norm(vec_movement)
            vec_movement *= ratio
            dt = dt*(1-ratio)
        else:   #only hold
            dt = 0.0
        self.att.loc += vec_movement
        return dt
            
    def move_to_target(self, dt):
        class Envelope:
            UpHoldDown, UpDown, Down, Halt = range(4)
        d_target = dist(self.att.loc, self.target)
        v_current = np.dot(direction(self.att.loc, self.target), self.att.speed)
        v_max = np.sqrt(self.MAX_ACC * d_target + v_current**2 * .5)
        if v_max > self.MAX_SPEED:
            #accelerate to max speed, hold and then decelerate
            envelope_type = Envelope.UpHoldDown
            v_targ = self.MAX_SPEED*direction(self.att.loc, self.target)
#        elif v_max < EPSILON:
#            #stay still
#            envelope_type = Envelope.Halt
#            v_targ = np.array([0,0], np.float)
        elif v_max < v_current*1.01:
            #need to slow down
            envelope_type = Envelope.Down
            v_targ = np.array([0,0], np.float)
        else:
            #speed up till v_max than slow down
            envelope_type = Envelope.UpDown
            v_targ = v_max*direction(self.att.loc, self.target)
        if self.debug:
            print(envelope_type),
            print(': M'+str(v_max)+' T'+str(np.linalg.norm(v_targ))+' D'+str(d_target)+' V'+str(v_current))
            print(dt)
        oldloc = np.array(self.att.loc)
        if (envelope_type == Envelope.UpDown) or (envelope_type == Envelope.UpHoldDown):
            #reach maximum speed
            dt = self.reach_speed(dt, v_targ)
            if self.debug:
                print(dt)
        if dist(oldloc, self.att.loc) > 2*RW_MAX_SPEED*0.1:
            raise RuntimeError('Error! Position jump happened @ accelerate')
        oldloc = np.array(self.att.loc)
        if (envelope_type == Envelope.UpHoldDown) or (envelope_type == Envelope.Down):
            #holding target speed
            dt = self.hold_speed(dt)
            if self.debug:
                print(dt)
        if dist(oldloc, self.att.loc) > 2*RW_MAX_SPEED*0.1:
            #print('pos jump @ hold! dt:%f, speed%s' % (dt,str(self.att.speed)))
            raise RuntimeError('Error! Position jump happened @ hold')
        oldloc = np.array(self.att.loc)
        #slow down (if any time left)
        dt = self.reach_speed(dt)
        if dist(oldloc, self.att.loc) > 2*RW_MAX_SPEED*0.1:
            raise RuntimeError('Error! Position jump happened @ decelerate')
        if self.debug:
            print(dt)
        if envelope_type == Envelope.Halt:
            return 0.0
        return dt
        
    def test_anim_step(self, i):
        dt = 0.1
        if self.debugTargetCounter < len(self.debugTargets):
            self.step(dt)
            self.xlineData.append(self.att.loc[0])
            self.ylineData.append(self.att.loc[1])
            #print('ho '),
            self.line.set_data(self.xlineData, self.ylineData)
        return self.line
            
    def test_anim_init(self):
        return self.line
        
    def test(self):
        self.fig = plt.figure()
        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(0, 60), ylim=(0, 25))
        ax.grid()
        line1, = ax.plot([e[0] for e in self.debugTargets],
                     [e[1] for e in self.debugTargets], 'gx')
        self.xlineData = [self.att.loc[0]]
        self.ylineData = [self.att.loc[1]]
        self.line, = ax.plot(self.xlineData, self.ylineData, 'ro-')
        #self.test_anim_step()
        return 
    
    def test2(self):
        self.xlineData = [self.att.loc[0]]
        self.ylineData = [self.att.loc[1]]
        while self.debugTargetCounter < len(self.debugTargets):
            self.step(0.1)
            self.xlineData.append(self.att.loc[0])
            self.ylineData.append(self.att.loc[1])
            
if __name__ == "__main__":
    q = Quad(Attitude([0,0], [5,0], 10))
    q.test2()
    scipy.io.savemat('matlab/quadSimTest.mat', dict(tasks=q.debugTargets, coordsx=q.xlineData, coordsy=q.ylineData))
#    ani = q.test()
#    ani = animation.FuncAnimation(q.fig, q.test_anim_step, frames=200, blit=True, repeat=False, save_count=200)
#    FFwriter = animation.FFMpegWriter()
#    ani.save('test.mp4', writer = FFwriter, fps=10)#, extra_args=['-vcodec', 'libx264'])