# -*- coding: utf-8 -*-
"""
Created on Sun Nov 01 14:25:22 2015

@author: Zoli
"""
import numpy as np
from constants import EPSILON, OBSERVATION_INTERVAL, FW_CRUISE_SPEED, FW_PLAN_TURN_RADIUS
from fixedsimulator import FixedWing
from ProblemFactory import Density
from agent import angle_rng_pi, angle_rng_2pi, direction, closest_nonzero_dist, dist
import mcts.mcts
import mcts.tree_policies
import mcts.default_policies
import mcts.backups
import mcts.graph

class MCTSPlanner:
    
    class MoveAction:
        def __init__(self, posheading):
            self.posheading = np.asarray(posheading)
            
        def __eq__(self, other):
            return all(self.posheading == other.posheading)
            
        def __hash__(self):
            return hash(self.posheading.tostring())
            
        def __str__(self):
            return str(self.posheading)
            
        def isQuit(self):
            return np.any(np.isinf(self.posheading))
            
    class MCTSState:
        def __init__(self, pos_heading=[], observations=[], pathLength=0, branching = 5, obs_dist = OBSERVATION_INTERVAL*FW_CRUISE_SPEED):
            if len(observations) == 0:
                observations = [pos_heading[0:2]]
            self.observations = np.asarray(observations)
            self.branching = branching
            self.obs_dist = obs_dist
            self.pathLength = pathLength
            self.nextLength = pathLength + 1
            self.actions = [MCTSPlanner.MoveAction(ph) for ph in MCTSPlanner.getNextPointRange(np.asarray(pos_heading), self.obs_dist, self.branching)]
            self.posheading = pos_heading
            
        def perform(self, action):
            new_pos_heading = action.posheading
            if self.obs_dist == OBSERVATION_INTERVAL*FW_CRUISE_SPEED:
                observation_loc = [action.posheading[0:2]]
            else:
                observation_loc = MCTSPlanner.getObservations([self.observations[-1], self.posheading[0:2], action.posheading[0:2]], self.posheading[2])
            observations = np.concatenate((self.observations, observation_loc))
            return MCTSPlanner.MCTSState(new_pos_heading, observations, self.nextLength, self.branching, self.obs_dist)
            
        def reward(self, parent, action):
            return 0.
            
        def is_terminal(self):
            return False
            
        def __eq__(self, other):
            if len(self.observations) != len(other.observations):
                return False
            return np.all(self.observations == other.observations)
            
        def __hash__(self):
            return hash(self.observations.tostring())
            
    class ScerriState:
        def __init__(self, mctsstate, util_fcn):
            self.baseState = mctsstate
            self.actions = mctsstate.actions
            self.overall_util = 0.
            self.utility_fcn = util_fcn
            
        def is_terminal(self):
            return False
        
        def perform(self, action):
            new_state = MCTSPlanner.ScerriState(self.baseState.perform(action), self.utility_fcn)
            new_obs = new_state.baseState.observations[len(self.baseState.observations):]
            new_state.overall_util = self.overall_util + self.utility_fcn(new_obs, self.baseState.observations)
            return new_state
            
        def reward(self, parent, action):
            return self.overall_util/len(self.baseState.observations)
            
        def __eq__(self, other):
            return self.baseState == other.baseState
            
        def __hash__(self):
            return hash(self.baseState)
        
            
    @staticmethod
    def getNextPointRange(current, travelDisance, nPoints, turnRange=[-1+EPSILON, 1-EPSILON]):
        if nPoints == 0:
            return []
        cp, cn = FixedWing.getTurnCircleCenters(current)
        heading = current[2]
        headingUnitVector = np.array([np.cos(heading), np.sin(heading)], np.float)
        radiusVector = cp-current[0:2]
        prange = []
        for turnAngle in np.linspace(turnRange[0]*travelDisance/FW_PLAN_TURN_RADIUS, turnRange[1]*travelDisance/FW_PLAN_TURN_RADIUS, nPoints, dtype=np.float):
            new_pos_heading = np.concatenate((
                MCTSPlanner.getPointByAngleAndDistance(current[0:2], headingUnitVector, turnAngle, radiusVector, travelDisance),
                [angle_rng_pi(heading+turnAngle)]))
            prange.append(new_pos_heading)
        return prange
    
    @staticmethod
    def getPointByAngleAndDistance(startPos, headingUnitVector, turnAngle, radiusVector, travelDistance):
        rv = np.sign(turnAngle) * radiusVector
        ta = np.abs(turnAngle)
        if travelDistance < FW_PLAN_TURN_RADIUS*ta:
            return False
        tangential = headingUnitVector * (FW_PLAN_TURN_RADIUS*np.sin(ta) + (travelDistance - FW_PLAN_TURN_RADIUS*ta)*np.cos(ta))
        radial = rv * (1-np.cos(ta) + (travelDistance/FW_PLAN_TURN_RADIUS - ta)*np.sin(ta))
        return startPos+tangential+radial
        
    @staticmethod
    def getObservations(path, heading, obs_dist = OBSERVATION_INTERVAL*FW_CRUISE_SPEED, tmp_dist = None):
        if len(path) == 1:
            return []
        if tmp_dist == None:
            tmp_dist = obs_dist
        pos = path[0]
        target = path[1]
        heading_vec = np.array([np.cos(heading), np.sin(heading)], np.float)
        target_projection = np.dot(target-pos, heading_vec)*heading_vec+pos
        #when direction is fairly good, skip turning
        if dist(target_projection, target) < EPSILON:
            turn_angle = 0.0
        else:
            radius_vec = direction(target_projection, target) * FW_PLAN_TURN_RADIUS
            turn_direction = np.sign(np.dot([-np.sin(heading), np.cos(heading)], radius_vec))            
            origin = pos + radius_vec
            toorigin_vec = origin-target
            heading_diff_angle = turn_direction * (np.arctan2(toorigin_vec[1], toorigin_vec[0]) - np.arctan2(radius_vec[1], radius_vec[0]))
            if dist(origin, target) < FW_PLAN_TURN_RADIUS + EPSILON:
                tangent_cos_angle = 0.0
            else:
                tangent_cos_angle = np.arccos(FW_PLAN_TURN_RADIUS/dist(origin, target))
            turn_angle = angle_rng_2pi(heading_diff_angle-tangent_cos_angle)

        if turn_angle*FW_PLAN_TURN_RADIUS > tmp_dist:    #observation reached while turning
            turn_angle = turn_direction * (tmp_dist/FW_PLAN_TURN_RADIUS)
            turn_matrix = np.matrix([[np.cos(turn_angle), -np.sin(turn_angle)], [np.sin(turn_angle), np.cos(turn_angle)]], np.float)
            pos = origin + np.asarray(-turn_matrix*radius_vec[:,np.newaxis]).reshape(2,)
            path[0] = pos
            heading = angle_rng_pi(heading+turn_angle)
            return [np.array(pos, copy=True)] + MCTSPlanner.getObservations(path, heading, obs_dist)
        else:   #finish turn
            if turn_angle != 0.0:
                tmp_dist = tmp_dist - turn_angle * FW_PLAN_TURN_RADIUS
                turn_angle *= turn_direction
                turn_matrix = np.matrix([[np.cos(turn_angle), -np.sin(turn_angle)], [np.sin(turn_angle), np.cos(turn_angle)]], np.float)
                pos = origin + np.asarray(-turn_matrix*radius_vec[:,np.newaxis]).reshape(2,)
                heading = angle_rng_pi(heading+turn_angle)
            target_distance = dist(pos, target)
            if target_distance >= tmp_dist:    #observation reached while going straight
                pos += (target-pos)*(tmp_dist/target_distance)
                path[0] = pos
                return [np.array(pos, copy=True)] + MCTSPlanner.getObservations(path, heading, obs_dist)
            else: #target is reached before observation
                tmp_dist -= target_distance
                return MCTSPlanner.getObservations(path[1:], heading, obs_dist, tmp_dist)
        
        
    
class densityAtLoc:
    def __init__(self, density):
        if not isinstance(density, Density):
            self.density = Density(density)
        else:
            self.density = density
    def __call__(self, pos, observations):
        pos = pos[-1]
        return self.density[pos[0], pos[1]]
        
class densityMultipleObs:
    def __init__(self, density):
        if not isinstance(density, Density):
            self.density = Density(density)
        else:
            self.density = density
        self.def_dist = OBSERVATION_INTERVAL*FW_CRUISE_SPEED
    def __call__(self, pos, observations):
        posarray = np.asarray(pos, np.int)
        densval = self.density[posarray[:,0], posarray[:,1]]
        dist_factor = [min(1., closest_nonzero_dist(p, np.concatenate((observations, pos)))/self.def_dist) for p in pos]
        return np.sum(densval * dist_factor)
    
def test_mcts():
    import time
    import scipy.io
    
    ts = mcts.mcts.MCTS(tree_policy=mcts.tree_policies.UCB1(c=1.41),
                        default_policy=mcts.default_policies.immediate_reward,
                        backup=mcts.backups.monte_carlo)
    posheading = [0,0,0]
    _, path_to_target = FixedWing.navigateTo(posheading, MCTSPlanner.MCTSState.target)
    MCTSPlanner.MCTSState.origPath = path_to_target
    actions = [posheading]
    t = time.time()
    currentState = MCTSPlanner.MCTSState(posheading, branching=5)
    root = mcts.graph.StateNode(None, currentState)
    best_action = ts(root, 50000)
    while not best_action.isQuit():
        actions.append(best_action.posheading)
        currentState = currentState.perform(best_action)
        posheading = best_action.posheading
        root = mcts.graph.StateNode(None, currentState)
        best_action = ts(root, 50000)
    elapsed = time.time() - t
    print('Tree search took %d seconds (searched %d times)' % (elapsed, len(currentState.observations)))
    finishPoints, length = FixedWing.navigateTo(posheading, MCTSPlanner.MCTSState.target)
    finishPoints.append(MCTSPlanner.MCTSState.target)
    scipy.io.savemat('matlab/mctsApproxTest.mat', dict(mctsPath=currentState.observations, obs=currentState.allObservations(), endPath=finishPoints, actions=actions))
    
    
if __name__ == "__main__":
    from scipy import misc
    import time
    import scipy.io
    
    from ProblemFactory import Distribution2D    
    
    density = misc.imread('pre_haiti10_v-20_c.1.png', flatten=True)
#    density = misc.imread('gt_haiti10_2-4-10-40-125_E-61.062_1pxPm.png', flatten=True)
    density = density.transpose()/255.0
    maskedDensity = np.ma.MaskedArray(density, ~np.array(density,np.bool))
    mean_density = np.ma.mean(maskedDensity)
    
    distribution = Distribution2D(density)
    samples = []
    for i in range(500):
        [rndx, rndy] = np.random.uniform(size=2)
        samples.append(distribution.sample(rndx, rndy))
            
            
    posheading = [400,400,0]
    ts = mcts.mcts.MCTS(tree_policy=mcts.tree_policies.UCB1(c=1.41*mean_density),
                        default_policy=mcts.default_policies.immediate_reward,
                        backup=mcts.backups.monte_carlo)
    t = time.time()
    n=10
    state = MCTSPlanner.ScerriState(MCTSPlanner.MCTSState(posheading, branching=20, obs_dist=n*OBSERVATION_INTERVAL*FW_CRUISE_SPEED+EPSILON), densityMultipleObs(density))
    root = mcts.graph.StateNode(None, state)
    best_action = ts(root, 20000, expansion_size=5, debug=False)
    best_seq = ts.best_sequence(root)
    elapsed = time.time() - t
    endstate = state
    for a in best_seq:
        endstate = endstate.perform(a)
    util = endstate.reward(None, None)
    print('Tree search took %d seconds (%d long best route)' % (elapsed, len(best_seq)))
    print('Overall utility gain', util)
    scipy.io.savemat('matlab/mctsScerriTest.mat', dict(mctsPath=[posheading]+[a.posheading for a in best_seq], best_action=best_action.posheading, obs=endstate.baseState.observations, samples=samples))
    #scipy.io.savemat('matlab/samples.mat', {'samples': samples})
    
    
    
    