# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:18:35 2015

@author: Zoli
"""
from agent import Attitude, dist, angle_rng_pi, angle_rng_2pi
from constants import FW_TURN_RADIUS, FW_PLAN_TURN_RADIUS, FW_DROP_RADIUS, FW_CRUISE_SPEED, FW_TARGETDISTANCE, FW_STAY_TIME, EPSILON
import numpy as np
#radius used for planning
R = FW_TURN_RADIUS
MIN_POINT_DIST = (0.1+EPSILON)*FW_CRUISE_SPEED

class FixedWing:
    def __init__(self, attitude, target=[], debug=False):
        self.debug = debug
        self.TURN_RADIUS = FW_TURN_RADIUS
        self.SPEED = FW_CRUISE_SPEED
        self.STAY_TIME = FW_STAY_TIME
        self.WIND_SPEED_RMS = 0.0
        self.WIND_SPEED_CONSTANT = np.array([0.5, 0], np.float)
        self.DROP_RADIUS = FW_DROP_RADIUS
        if not isinstance(attitude, Attitude):
            raise TypeError('attitude needs to have agent.Attitude type')
        self.att = attitude
        self.steptime = 0.1
        self.TARGET_JUMP = 2
        self.set_target_list(target)
        self.wind_random_component = np.array([0.1, -0.2], np.float)
        
    @property
    def target(self):
        return (self.manvre_list + self.target_list)[0]
        
    def endTargetIndex(self):
        for i, e in enumerate(self.target_list):
            if e is None:
                return i
        
    def get_path_length(self):
        endIndex = self.endTargetIndex()
        return sum(self.pathLength([self.att.getPosHeading()] + self.manvre_list + self.target_list[:endIndex], False))
        
    def get_last_target(self):
        if not self.target_list[0] is None:
            endIndex = self.endTargetIndex()
            return self.target_list[endIndex-1]
        elif len(self.manvre_list) > 0:
            return self.manvre_list[-1]
        else:
            return self.att.getPosHeading()
        
    def set_target_list(self, targets):
        """Sets a list of fixed targets to follow with headings
        Parameters
        ----------
        targets: [[x,y,heading],[x,y,heading],...]
            list of targets in any format and shape"""
        self.target_list = list(np.array(targets, np.float).reshape(-1,3))+[None, None]
        self.set_manvre([], False)
        
    def append_targets(self, targets):
        """Sets a list of fixed targets (with headings) after the current set of targets
        Parameters
        ----------
        targets: [[x,y,heading],[x,y,heading],...]
            list of targets in any format and shape"""
        newTargets = list(np.array(targets, np.float).reshape(-1,3))
        endIndex = self.endTargetIndex()
        endList = self.target_list[endIndex:]
        transition = self.navigateTo(self.get_last_target(), newTargets[0], False)[0]
        self.target_list[endIndex:] = transition + newTargets + endList
        
    def set_manvre(self, targets, drop_near_targets = True):
        """Adds a temporary manoeuvre to the beginning of the target list.
        Connects the part with the existing target_list, removes previous manoeuvre.
        If the next target is within self.DROP_RADIUS, it removes that target.
        
        Parameters
        ----------
        targets: [[x,y,heading],[x,y,heading],...]
            list of manoeuvre waypoints"""
        if len(targets) == 0:
            manvre = []
            manvrend = self.att.getPosHeading()
        else:
            manvre = list(np.array(targets, np.float).reshape(-1,3))
            manvrend = manvre[-1]
        if drop_near_targets:
            self.drop_near_targets()
        if not self.target_list[0] is None:
            self.manvre_list = manvre + list(FixedWing.navigateTo(manvrend, self.target_list[0], False)[0])
        else:
            self.manvre_list = manvre
        
    def drop_near_targets(self):
        while (not self.target_list[1] is None) and (dist(self.target_list[0][:2], self.att.loc) < self.DROP_RADIUS):
            del self.target_list[0]
        
    def get_next_target(self):
        self.drop_near_targets()
        return self.target_list[0]
        
    def is_targets_finished(self):
        return self.target_list[0] is None
        
    def set_next_target(self, missed=False):
        if len(self.manvre_list) == 0:
            missed_target = self.target
            del self.target_list[0]
            if missed:
                self.target_list.insert(self.TARGET_JUMP, missed_target)
        else:
            del self.manvre_list[0]
            
    def step(self, dt):
        """Steps dt time ahead in the simulation
        
        Returns
        ------
        list of reached targets during the step"""
        if self.att.batteryTime <= 0:
            return []
        reached_target = []
        pathLen = self.get_path_length()
        #decrease battery and wait time, but always keep moving (do not change dt)
        self.att.batteryTime -= dt
        self.att.waitTime -= dt
        while dt > 0:
            dt = self.go_towards_target(dt)
            if dt > 0:
                missed = True
                if dist(self.target[:2], self.att.loc) < FW_TARGETDISTANCE:
                    missed = False
                    reached_target.append(np.concatenate((self.target[:2], self.att.loc)))
                    if self.debug:
                        print('target reached and position:')
                        print(reached_target)
                        print('distance:', dist(self.target[:2], self.att.loc))
                self.set_next_target(missed)
        if self.att.waitTime < 0:
            self.att.waitTime = 0.0
        pl = self.get_path_length()
        if pl > pathLen:
            self.correct_heading(pl)
        return reached_target
        
    def correct_heading(self, plen):
        theta = EPSILON
        Rp = np.array([[np.cos(theta), -np.sin(theta)],[np.sin(theta), np.cos(theta)]], np.float)
        Rn = np.array([[np.cos(theta), np.sin(theta)],[-np.sin(theta), np.cos(theta)]], np.float)
        s = self.att.speed
        speedvecs = [s, np.dot(Rp, s), np.dot(Rn, s)]
        plens = [plen]
        for sv in speedvecs[1:]:
            self.att.speed = sv
            plens.append(self.get_path_length())
        bestSpeed = np.argmin(plens)
        self.att.speed = speedvecs[bestSpeed]
        
    def go_towards_target(self, dt):
        if dt <= 0.0:
            return 0.
        if self.target is None:
            self.go_straight(dt)
            return 0.
        targetLoc = self.target[:2]
        current_heading = np.arctan2(self.att.speed[1], self.att.speed[0])
        target_heading = heading(self.att.loc, targetLoc)
        heading_diff = angle_rng_pi(target_heading - current_heading)
        turn_dir = np.sign(heading_diff)
        if turn_dir == 0:
            straight_distance = dist(self.att.loc, targetLoc)
        else:
            posheading = [self.att.loc[0], self.att.loc[1], current_heading]
            centers = self.getTurnCircleCenters(posheading, False)
            center = centers[max(0, int(-turn_dir))]
            #get polar coordinates with (center, current radius angle) origin with positive angle towards the turn_dir direction
            phi_0 = current_heading - turn_dir*np.pi/2
            r = dist(center, targetLoc)
            phi = angle_rng_2pi(turn_dir * (heading(center, targetLoc)-phi_0))
            if r < R:
                turn_angle = phi
                straight_distance = 0.
            else:
                turn_angle = phi - np.arccos(R/r)
                straight_distance = np.sqrt(r*r - R*R)
            if ((turn_angle < 0.0 and turn_angle > EPSILON) or (turn_angle > 2*np.pi-EPSILON)) and np.abs(r-R)<EPSILON:
                turn_angle = 0.0
                straight_distance = 0.0
            turn_time = turn_angle * R / self.SPEED
            if turn_time > dt:
                turn_angle = dt * self.SPEED / R
                self.turn_angle(turn_angle, current_heading, turn_dir, center)
                return 0.
            else:
                self.turn_angle(turn_angle, current_heading, turn_dir, center)
                dt -= turn_time
        straight_time = straight_distance / self.SPEED
        if straight_time > dt:
            self.go_straight(dt)
            return 0.
        else:
            self.go_straight(straight_time)
            return dt - straight_time
        
    def turn_angle(self, angle, heading, direction, center):
        if angle == 0.0:
            return
        new_heading = heading+direction*angle
        new_pos = self.getCirclePointByHeading(center, new_heading, direction, False)
        self.att.loc = new_pos
        self.att.speed = np.array([np.cos(new_heading)*self.SPEED, np.sin(new_heading)*self.SPEED], np.float)
        
    def go_straight(self, time):
        self.att.loc += self.att.speed * time
        
    def within_turn_circle(self, point):
        radius_vector = (self.att.speed/np.linalg.norm(self.att.speed))*self.TURN_RADIUS
        radius_vector = np.array([-radius_vector[1], radius_vector[0]], np.float)
        centerpoint1 = self.att.loc + radius_vector
        centerpoint2 = self.att.loc - radius_vector
        dist1 = dist(point, centerpoint1)
        if(dist1 < self.TURN_RADIUS):
            return centerpoint1, dist1, 1
        dist2 = dist(point, centerpoint2)
        if(dist2 < self.TURN_RADIUS):
            return centerpoint2, dist2, -1
        return False
        
    def angle_to_closest_point(self, point, origin):
        v1 = self.att.loc - origin
        v2 = point - origin
        return np.arctan2(v2[1], v2[0]) - np.arctan2(v1[1], v1[0])
        
    def test_anim_step(self, i):
        dt = 0.1
        if self.debugTargetCounter < len(self.debugTargets):
            self.step(dt)
            self.xlineData.append(self.att.loc[0])
            self.ylineData.append(self.att.loc[1])
            #print('ho '),
            self.line.set_data(self.xlineData, self.ylineData)
        return self.line
            
    def test_anim_init(self):
        return self.line
    
    def test2(self, debugTargets):
        debugTargetCounter = 0
        self.set_target(debugTargets[debugTargetCounter])
        self.xlineData = [self.att.loc[0]]
        self.ylineData = [self.att.loc[1]]
        while (debugTargetCounter < len(debugTargets)) and (self.att.batteryTime > 0):
            target = self.step(0.1)
            if isinstance(target, np.ndarray):
                print str(target) + ' reached'
                debugTargetCounter += 1
                if(debugTargetCounter < len(debugTargets)):
                    self.set_target(debugTargets[debugTargetCounter])
            self.xlineData.append(self.att.loc[0])
            self.ylineData.append(self.att.loc[1])
            
    @staticmethod
    def pathLength(points, isPlanning=True):
		if len(points) < 2:
			return []
		return [FixedWing.navigateTo(points[i-1], points[i], isPlanning)[1] for i in np.arange(1, len(points))]
                
    @staticmethod
    def navigateTo(current, destination, isPlanning=True, debug = False):
        """ Computes the ideal path length and additional points to take the ideal path from
        current to destiantion.
        
        Parameters
        ----------
        current: np.ndarray of float [x,y,heading]
            start location
        destination: np.ndarray of float [x,y,heading]
        
        Returns
        -------
        points, path_length:
            points: [[x,y,heading], [x,y,heading]]
                list of two waypoints with headings between current and destination _excluding_ current and destination
            
            path_length: the length of the plaaned route between current and destination"""
        if isPlanning:
            R = FW_PLAN_TURN_RADIUS
        else:
            R = FW_TURN_RADIUS
        cp, cn = FixedWing.getTurnCircleCenters(current, isPlanning)
        dp, dn = FixedWing.getTurnCircleCenters(destination, isPlanning)
        tangents = [(FixedWing.getTangent(cp,dp,0,isPlanning), (1,1)), (FixedWing.getTangent(cp,dn,1,isPlanning), (1,-1)),
                     (FixedWing.getTangent(cn,dn,0,isPlanning), (-1,-1)), (FixedWing.getTangent(cn,dp,-1,isPlanning), (-1,1))]
        circles = [(FixedWing.getTouchingCircle(cp,dp,1,1,isPlanning), 1), (FixedWing.getTouchingCircle(cp,dp,1,-1,isPlanning), 1),
                    (FixedWing.getTouchingCircle(cn,dn,-1,1,isPlanning), -1), (FixedWing.getTouchingCircle(cn,dn,-1,-1,isPlanning), -1)]
        pathLen = []
        for t in tangents:
            if t[0] is False:
                pathLen.append(np.inf)
            else:
                signs = t[1]
                tangent_len = t[0][0]
                tangent_heading = t[0][1]
                turnangle_start = signs[0] * (tangent_heading - current[2])
                turnangle_end = signs[1] * (destination[2] - tangent_heading)
                if turnangle_start < 0.0 and turnangle_start > -EPSILON*0.1:
                    turnangle_start = 0.0
                if turnangle_end < 0.0 and turnangle_end > -EPSILON*0.1:
                    turnangle_end = 0.0
                if debug:
                    print(turnangle_start, turnangle_end)
                turnangle_start = angle_rng_2pi(turnangle_start)
                turnangle_end = angle_rng_2pi(turnangle_end)
                if debug:
                    print(turnangle_start, turnangle_end)
                pathLen.append((turnangle_start+turnangle_end)*R + tangent_len)
        for c in circles:
            if c[0] is False:
                pathLen.append(np.inf)
            else:
                startdir = c[1]
                arc_len = c[0][0]
                start_heading = c[0][1]
                end_heading = c[0][2]
                turnangle_start = angle_rng_2pi(startdir*(start_heading - current[2]))
                turnangle_end = angle_rng_2pi(startdir*(destination[2] - end_heading))
                pathLen.append((turnangle_start+turnangle_end)*R + arc_len)
        bestPathid = np.argmin(pathLen)
        #print(bestPathid),
        if debug:
            print(pathLen)
            print(tangents)
            print(circles)
        if np.isinf(pathLen[bestPathid]):
            return False
        if bestPathid < 4:
            if tangents[bestPathid][1][0] > 0:
                cc = cp
            else:
                cc = cn
            if tangents[bestPathid][1][1] > 0:
                dc = dp
            else:
                dc = dn
            tangent_startpoint = FixedWing.getCirclePointByHeading(cc, tangents[bestPathid][0][1], tangents[bestPathid][1][0], isPlanning)
            tangent_endpoint = FixedWing.getCirclePointByHeading(dc, tangents[bestPathid][0][1], tangents[bestPathid][1][1], isPlanning)
            points = [np.concatenate((tangent_startpoint,[tangents[bestPathid][0][1]])),
                      np.concatenate((tangent_endpoint,[tangents[bestPathid][0][1]]))]
        else:
            points = circles[bestPathid-4][0][3]
        return points, pathLen[bestPathid]
        
    @staticmethod
    def getTurnCircleCenters(pos_and_heading, isPlanning=True):
        if isPlanning:
            R = FW_PLAN_TURN_RADIUS
        else:
            R = FW_TURN_RADIUS
        pos = np.array(pos_and_heading[0:2], np.float)
        rvec = np.array([-R*np.sin(pos_and_heading[2]), R*np.cos(pos_and_heading[2])], np.float)
        center_pos = pos + rvec
        center_neg = pos - rvec
        return center_pos, center_neg
    
    
    @staticmethod
    def getTangent(centerFrom, centerTo, internalDir, isPlanning=True, debug=False):
        """Finds the common tangents of two R radius circles, returns its length and direction heading angle
        
        Parameters
        ----------
        centerFrom: numpy.array([x,y], np.float)
            Start cicrle center
        centerTo: numpy.array([x,y], np.float)
            End cicrle center
        internalDir: selector for which common tangent is needed values
            - positive: the left pointing internal common tangent is returned
            - negative: the right pointing internal common tangent is returned
            - zero: the external common tangent is returned (only one kind because of common radius)
            
        Returns
        -------
        tangent length, tangent heading"""
        if isPlanning:
            R = FW_PLAN_TURN_RADIUS
        else:
            R = FW_TURN_RADIUS
        d = dist(centerFrom, centerTo)
        if d < EPSILON:
            angle = 0.0
        else:
            diff = centerTo - centerFrom
            angle = np.arctan2(diff[1], diff[0])
        if debug:
            print('d',d,'angle',angle)
        if internalDir == 0:
            return d, angle
        else:
            # if circles intersect, no internal common tangent
            if d < 2*R:
                return False
            tanlen = np.sqrt(d*d - 4*R*R)
            alpha = np.arcsin(2*R/d)
            if internalDir > 0:
                return tanlen, angle_rng_pi(angle+alpha)
            else:
                return tanlen, angle_rng_pi(angle-alpha)
                
    MIN_STRAIGHT_LEN = 0.1*FW_CRUISE_SPEED
    @staticmethod
    def getTouchingCircle(centerFrom, centerTo, startDir, circlePos, isPlanning=True):
        """Finds a common touching circle for the tart and end circles
        
        Parameters
        ----------
        centerFrom: numpy.array([x,y], np.float)
            Start cicrle center
        centerTo: numpy.array([x,y], np.float)
            End cicrle center
        startDir: selector [-1 or +1]
            Which direction the start and end circles go
        circlePos: selector [-1 or +1]
            Which side is the touching circle on
            
        Returns
        -------
        Touching arc length, arc start heading, arc end heading"""
        if isPlanning:
            R = FW_PLAN_TURN_RADIUS
        else:
            R = FW_TURN_RADIUS
        IncreasedRadius = np.sqrt(4*R*R + FixedWing.MIN_STRAIGHT_LEN*FixedWing.MIN_STRAIGHT_LEN) - R
        IncreaseAngle = np.arctan( FixedWing.MIN_STRAIGHT_LEN / (2*R) )
        Ri = IncreasedRadius
        if circlePos>=0:
            circlePos = 1
        else:
            circlePos = -1
        if startDir>=0:
            startDir = 1
        else:
            startDir = -1
        d = dist(centerFrom, centerTo)
        if d > (2*R+2*Ri):
            return False
        if d < EPSILON:
            #The tangent will return a good solution in this case
            return False
        alpha = np.arccos(d/(2*R+2*Ri))
        diff = centerTo - centerFrom
        angle = np.arctan2(diff[1], diff[0])
        radiusAngleStart = angle + circlePos*alpha
        radiusAngleEnd = angle - circlePos*alpha + startDir*IncreaseAngle + np.pi
        midCenter = centerFrom + np.array([np.cos(radiusAngleStart), np.sin(radiusAngleStart)],np.float)*(R+Ri)
        midRadiusAngleStart = radiusAngleStart + np.pi - startDir*IncreaseAngle
        midRadiusAngleEnd = angle - circlePos*alpha + startDir*IncreaseAngle
#        waypoint0 = centerFrom +  np.array([np.cos(radiusAngleStart-startDir*IncreaseAngle), np.sin(radiusAngleStart-startDir*IncreaseAngle)],np.float)*R
        waypoint1 = midCenter +  np.array([np.cos(midRadiusAngleStart), np.sin(midRadiusAngleStart)],np.float)*R
#        waypoint2 = midCenter + np.array([np.cos(midRadiusAngleEnd), np.sin(midRadiusAngleEnd)],np.float)*R
        waypoint3 = centerTo + np.array([np.cos(radiusAngleEnd), np.sin(radiusAngleEnd)],np.float)*R
        #on the middle circle turn from -touchpoint1 to -touchpoint2 in the opposite direction
        turnAngle = angle_rng_2pi(-startDir*(midRadiusAngleEnd - midRadiusAngleStart))
        turnStartHeading = angle_rng_pi(angle + circlePos*alpha - startDir*IncreaseAngle + startDir*np.pi/2)
        turnEndHeading = angle_rng_pi(radiusAngleEnd + startDir*np.pi/2)
        waypoint1 = np.concatenate((waypoint1, [turnStartHeading]))
        waypoint3 = np.concatenate((waypoint3, [turnEndHeading]))
        return turnAngle*R+2*FixedWing.MIN_STRAIGHT_LEN, turnStartHeading, turnEndHeading, [waypoint1, waypoint3]
                
    @staticmethod
    def getCirclePointByHeading(center, heading, direction, isPlanning=True):
        if isPlanning:
            R = FW_PLAN_TURN_RADIUS
        else:
            R = FW_TURN_RADIUS
        if direction > 0:
            h = angle_rng_pi(heading - np.pi/2)
        else:
            h = angle_rng_pi(heading + np.pi/2)
        return center + np.array([R*np.cos(h), R*np.sin(h)], np.float)
            
def heading(agent, target):
    diff = target-agent
    return np.arctan2(diff[1], diff[0])
    
def follow_points(points, timeStep = 0.1):
    posarray = [points[0][:2]]
    pts = np.asarray(points)
    fw = FixedWing(Attitude(pts[0, :2], [np.cos(pts[0, 2])*FW_CRUISE_SPEED, np.sin(pts[0, 2])*FW_CRUISE_SPEED], np.inf), pts[1:])
    #skipping missed waypoints
    fw.TARGET_JUMP = -1
    while not fw.target is None:
        fw.step(timeStep)
        posarray.append(np.array(fw.att.loc))
    return posarray
    
#obsolete function!
def measure_turn(distance_array, angle_array):
    """***OBSOLETE FUNCTION***"""
    times = []
    for d in distance_array:
        print d
        subtimes = []
        for a in angle_array:
            f = FixedWing(Attitude([0,0], [FW_CRUISE_SPEED,0], 100000), [np.cos(a)*d, np.sin(a)*d]*2)
            t = 0.1
            target = f.step(0.1)
            while not isinstance(target, np.ndarray):
                target = f.step(0.1)
                t += 0.1
            subtimes.append(t)
        times.append(subtimes)
    return times  

                
def test_navigateTo():
    import scipy.io
    
    RUNS = 20
    startpos = np.random.uniform(size=(RUNS,3))
    startpos[:,0:2] *= R/5
    startpos[:,0] += 10*R
    startpos[:,1] += -R/10
    startpos[:,2] *= 0.1#2*np.pi/100
    startpos[:,2] += np.pi
    destiantion = [0,0,-np.pi]
    poslog = []
    targets = np.zeros((RUNS, 2, 3))
    for i in range(RUNS):
        pos = startpos[i]
        target = FixedWing.navigateTo(pos, destiantion)
        targets[i, :, :] = target[0]
        path = np.concatenate(([pos],targets[i, :, :],[destiantion]))
#        print(path)
        poslog.append(follow_points(path))
    loglen = max([len(l) for l in poslog])
    log_array = np.zeros((RUNS, loglen, 2))
    for i in range(RUNS):
        log_array[i, :len(poslog[i]), :] = poslog[i]
    scipy.io.savemat('matlab/navigateToTest.mat', dict(pos=log_array, targets = targets, start = startpos[:,0:2]))
            
if __name__ == "__main__":
    #print FixedWing.MIN_STRAIGHT_LEN
    test_navigateTo()

#    import scipy.io
#    pos = [0, 0, np.pi]
#    target = FixedWing.navigateTo(pos, [0,0,0])
#    print(target)
#    print(2*np.pi*R)
#    path = np.concatenate(([pos[:2]],target[0],[[0,0]]))
#    print('path:',path)
#    poslog = [follow_points(path, pos[2], 0.01)]
#    print(poslog[0][0])
#    scipy.io.savemat('matlab/navigateToTest.mat', dict(pos=poslog, targets = [target[0]], start = pos))
    
#    f = FixedWing(Attitude([0,0], [FW_CRUISE_SPEED,0], 1000), [-20, 0], True)
#    debugTargets = [[-20, 0], [200, 150], [120, 180], [20, 100], [300, 100], [0, 0]]
#    f.test2(debugTargets)
#    scipy.io.savemat('matlab/fixedtest.mat', dict(tasks=debugTargets, coordsx=f.xlineData, coordsy=f.ylineData))

#    distances = FW_TURN_RADIUS * 2 * np.power(10, np.arange(0, 3.2, 0.2))
#    angles = np.arange(0, np.pi, np.pi/16)
#    times = measure_turn(distances, angles)
#    scipy.io.savemat('matlab/fixedDistanceTest.mat', dict(distances=distances, angles=angles, times=times))

#    ani = q.test()
#    ani = animation.FuncAnimation(q.fig, q.test_anim_step, frames=200, blit=True, repeat=False, save_count=200)
#    FFwriter = animation.FFMpegWriter()
#    ani.save('test.mp4', writer = FFwriter, fps=10)#, extra_args=['-vcodec', 'libx264'])