#!/bin/bash
module load python
module load numpy/1.9.1

export PROG_DIR=/home/zb1f12/dec_dualsar

PID=()
for i in {0..15}
do
    if [ "$i" != "0" ]; then
        echo "Sleeping $TSLEEP s"
        sleep $TSLEEP
    fi
    
    FILEID=`expr $i + $PBS_ARRAYID \\* 16`
    echo "run $i started, file ID: $FILEID"
    date

    CMD="python $PROG_DIR/commander.py $FILEID $MODE"
    echo "Running $CMD"
    $CMD &
    PID[$i]=$!
done
for i in {0..15}
do
    #echo ${PID[$i]}
    wait ${PID[$i]}
done
