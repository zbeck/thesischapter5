# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 22:11:10 2015

@author: Zoli
"""
import numpy as np
from agent import dist, direction, Attitude

class RescueProblem:
    def __init__(self, agents, tasks, nFixed, speed, debug = False):
        self.agents = agents
        self.debug = debug
        self.tasks = np.array(tasks, np.float)
        #if tasks do not include activation there is no need to check activation
        if (len(tasks) == 0) or (self.tasks.shape[1] == 3):
            self.use_activaiton = False
        else:
            self.use_activaiton = True
        self.nFixedTasks = nFixed
        self.SPEED = speed
        self.buildTable()
        
    def __str__(self):
        return '%d agents, %d tasks (%d fixed)' % (len(self.agents), len(self.tasks), self.nFixedTasks)
        
    def buildTable(self):
        self.distanceTable = np.array(
            [[dist(a[:2],b[:2])/self.SPEED + a[2] for b in self.tasks]+
            [dist(a[:2],b.loc)/self.SPEED + b.estimatedWaitTime() + a[2] for b in self.agents]
            for a in self.tasks])
    
    def solveGreedily(self):
        schedule = [[] for i in range(len(self.agents))]
        agentloc = np.array(range(len(self.agents)), np.int) + len(self.tasks)
        agenttime = np.array([0]*len(self.agents), np.float)
        availtasks = np.array([True]*len(self.tasks), np.bool)
        while np.any(availtasks):
            possibilities = self.distanceTable[availtasks,:][:,agentloc] + agenttime
            if self.use_activaiton:
                possibilities = np.maximum(possibilities, self.tasks[availtasks,-1, np.newaxis])
            best_lin_index = np.argmin(possibilities)
            best_choice = np.unravel_index(best_lin_index, possibilities.shape)
            task = np.arange(len(self.tasks))[availtasks][best_choice[0]]
            agent = best_choice[1]
            mintime = possibilities.flatten()[best_lin_index]
            if mintime > self.agents[agent].batteryTime:
                break
            schedule[agent].append(task)
            agentloc[agent] = task
            agenttime[agent] = mintime
            availtasks[task] = False
        return schedule
        
    def getExecutionTimes(self, schedule):
        return self.getLowToHighFeedBackData(schedule)[:,0]
        
    def getLowToHighFeedBackData(self, schedule):
        """returns an array of [te, w] for each task
            te: execution times
            w: weights (number of critical tasks after a task (including itself))
        (the first self.nFixedTasks rows should not count for the feedback calculation)
        """
        schedulewa = [[i+len(self.tasks)]+schedule[i] for i in range(len(schedule))]
        # execution times with weights
        executionww = np.array([[np.inf, 0] for i in self.tasks], np.float)
        for agent in range(len(self.agents)):
            t = 0
            for task in range(1, len(schedulewa[agent])):
                #first assume that no non-critical tasks are in the schedule
                executionww[schedulewa[agent][task]][1] = len(schedulewa[agent])-task
                t += self.distanceTable[schedulewa[agent][task]][schedulewa[agent][task-1]]
                if self.use_activaiton and t < self.tasks[task][-1]:
                    t = self.tasks[task][-1]
                    sub_value = executionww[schedulewa[agent][task]][1] - 1
                    executionww[schedulewa[agent][1:task]][1] -= sub_value
                executionww[schedulewa[agent][task]][0] = t
        return executionww
        
#    def getFeedback(self, schedule):
#        execution = (self.getExecutionTimes(schedule)[self.nFixedTasks:], np.float)[:,0])[:,np.newaxis]
#        fb = np.concatenate((self.tasks[self.nFixedTasks:], execution), axis=1)
#        return fb
        
    ''' The form of the feedback:
            fb[i]: a sorted array of rescue times with weights for possibly explored tasks by search task i
    '''
    def solveByFeedback(self, fb_resqtimesww_by_search):
        schedulewa = [[len(self.tasks)+i] for i in range(len(self.agents))]
        unseentask = np.array([True]*len(self.tasks), np.bool)
        agentdelay = np.array([0]*len(self.agents), np.float)
        while np.any(unseentask):
            task = np.argmin([fb_resqtimesww_by_search[i][0][0] if len(fb_resqtimesww_by_search[i])>0 and unseentask[i] else np.inf for i in range(len(fb_resqtimesww_by_search))])
            if not unseentask[task]:
                task = np.where(unseentask)[0][0]
            unseentask[task] = False
            minincrease = np.inf
            minloc = -1
            bestagent = -1
            for i in range(len(self.agents)):
                location, increase = self.findBestInsertion(i, schedulewa[i], task, agentdelay[i], fb_resqtimesww_by_search)
                if increase < minincrease:
                    minincrease = increase
                    minloc = location
                    bestagent = i
            if bestagent >= 0:
                if self.debug:
                    print(task, bestagent, minloc, minincrease)
                schedulewa[bestagent].insert(minloc, task)
                agentdelay[bestagent] += minincrease
        return [s[1:] for s in schedulewa], agentdelay
                
    def findBestInsertion(self, agent, schedule, task, agentdelay, fb_resqtimesww_by_search):
        delays = [self.calculateDelay(agent, schedule, pos, task, fb_resqtimesww_by_search) for pos in range(1, len(schedule)+1)]
        minpos = np.argmin(delays)
        return minpos+1, delays[minpos]-agentdelay
        
    def calculateDelay(self, agent, schedule, pos, task, fb_resqtimesww_by_search):
        sched = list(schedule)
        sched.insert(pos, task)
        t = 0.0
        delay = 0.0
        for i in range(1,len(sched)):
            t += self.distanceTable[sched[i]][sched[i-1]]
            if self.use_activaiton and t < self.tasks[sched[i]][-1]:
                t = self.tasks[sched[i]][-1]
            if t > self.agents[agent].batteryTime:
                return np.inf
            task = sched[i]
            delay += self.calculateIndividualDelay(t, fb_resqtimesww_by_search[task])
        return delay
        
    ''' fb_resqtimes must be increasingly sorted! '''
    @staticmethod
    def calculateIndividualDelay(time, fb_resqtimesww):
        delay = time
        for t in fb_resqtimesww:
            if time < t[0]:
                break
            delay += (time - t[0])*t[1]
        return delay
                
    def getGreedyInstructions(self):
        schedule = self.solveGreedily()
        dirs = np.array([[0,0]]*len(self.agents), np.float)
        taskID = np.array([-1]*len(self.agents), np.int)
        for i in (a for a in range(len(self.agents)) if schedule[a]):
            dirs[i] = direction(self.agents[i].loc, self.tasks[schedule[i][0]][:2]) * len(schedule[i])
            if schedule[i][0] < self.nFixedTasks:            
                taskID[i] = schedule[i][0]
        return dirs, taskID, schedule
        
    def getFeedbackSchedule(self, fb_resqtimesww_by_search):
        schedule, delay = self.solveByFeedback(fb_resqtimesww_by_search)
        return schedule
        
    def getTardinessList(self, schedule, fb_resqtimesww_by_search):
        """Computes how much later would rescues start after explorations given in an agent schedule
        
        Parameters
        ----------
        schedule: [taskid,taskid, ...]
            single agent schedule to compute the tardiness for
        fb_resqtimesww_by_search: [[(t, weight), (t, weight)], ...]
            feedback from rescue with weights grouped by search tasks
        
        Returns
        -------        
        tardyCount, nonTartdyDiffLst
        
        tardyCount: int
            the number of tasks already late according to schedule
        nonTartdyDiffList: [[dt, weight], [dt, weight], ...]
            the rest of tardinesses relative to the execution from the schedule with their weigths in the following form
    
    """
        t = 0.0
        tardyCount = 0
        nonTartdyDiffList = []
        for i in range(1,len(schedule)):
            t += self.distanceTable[schedule[i]][schedule[i-1]]
            if self.use_activaiton and t < self.tasks[schedule[i]][-1]:
                t = self.tasks[schedule[i]][-1]
            task = schedule[i]
            tardyTasks, tlist = self.getIndividualTardinessList(t, fb_resqtimesww_by_search[task])
            tardyCount += tardyTasks
            nonTartdyDiffList += tlist
        nonTartdyDiffList.sort()
        return tardyCount, nonTartdyDiffList
        
    @staticmethod
    def getIndividualTardinessList(time, fb_resqtimesww):
        tardyTasks = 1
        tlist = []
        for t in fb_resqtimesww:
            if time < t:
                tlist.append((t[0]-time, t[1]))
            else:
                tardyTasks += 1
        return tardyTasks, tlist
        
    def plotSolution(self, schedule, density=None):
        import matplotlib.pyplot as plt
        fig = plt.figure(num=1)
        ax=fig.add_subplot(111)
        if density is not None:
            density.plot(ax)
        sid = 0
        for s in schedule:
            locations = np.concatenate((self.agents[sid].loc[np.newaxis, :], self.tasks[s, :2]))
            ax.plot(locations[:,0], locations[:,1])
            sid += 1
        if self.use_activaiton:
            for t in self.tasks[:]:
                ax.text(t[0], t[1], '%.2f'%(t[3],))
        plt.show()
        
if __name__ == "__main__":
    import problemTestData
    import time
    from problem import SearchProblem
    #agents = [Attitude([2,1], 0, 100), Attitude([6,3], 0, 100)]
    agents = [Attitude(*params) for params in problemTestData.agents]
    #tasks = [[3,3], [3.9,2], [5,1], [7,1], [9,1]]
    tasks = np.asarray(problemTestData.tasks)
    t = time.time()
    rp = RescueProblem(agents, tasks, problemTestData.nFixed, problemTestData.speed)
    t_RCreation = time.time()-t
    t = time.time()
    sp = SearchProblem(agents, tasks, problemTestData.nFixed, problemTestData.speed)
    t_SCreation = time.time()-t
    t = time.time()
    rsol = rp.solveGreedily()
    t_RSolution = time.time()-t
    t = time.time()
    ssol = sp.solveGreedily()
    t_SSolution = time.time()-t
    print(rsol)
    print(ssol)
    print('\tcreation\tsolution\nR\t%f\t%f\nS\t%f\t%f' % (t_RCreation, t_RSolution, t_SCreation, t_SSolution))