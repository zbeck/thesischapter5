angleid = 5;
for angleid = 1:3:length(angles)
    angle = angles(angleid);
    sratio = 22*times(:, angleid)'./distances;
    upper_sratio = (distances + 2*(angle-sin(angle))*60)./distances;
    lower_sratio = (sqrt(60^2*(1-cos(angle))^2+(distances-60*sin(angle)).^2)+60*angle)./distances;
    %lower approx very close

    semilogx(distances, [sratio' upper_sratio' lower_sratio'])
    hold on
    legend('sratio', 'uppper approx', 'lower approx')
end
hold off