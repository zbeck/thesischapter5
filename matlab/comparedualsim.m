id = '12';
idmod = {'-0', '-1', '-4'};
rtd = {'','',''};
rtdlen = zeros(size(rtd));
figure(3)
hold off
load(['../simout/simoutput' id '.mat']);
plot(resqTasks(:,1)*10, resqTasks(:,2)*10, 'kx', 'linewidth', 2)
hold on
colour = jet(length(idmod));
for index = 1:length(idmod)
    load(['../simout/dual_poslog' id idmod{index} '.mat'], 'rtaskDone');
    rtd{index} = rtaskDone;
    rtdlen(index) = size(rtaskDone,1);
    scatter(rtaskDone(:,1), rtaskDone(:,2), 5, colour(index,:))
end
times = zeros(max(rtdlen), length(idmod));
tasklen = zeros(max(rtdlen), length(idmod));
for index = 1:length(idmod)
    times(1:rtdlen(index),index) = rtd{index}(:,4);
    tasklen(1:rtdlen(index),index) = rtd{index}(:,3);
end
figure(2)
hist(tasklen, 40)
figure(4)
bar(times)

%sort