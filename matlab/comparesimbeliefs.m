id = '29';
idmod = {'-0', '-1', '-2', '-3'};
beliefs = {'','','',''};
for index = 1:length(idmod)
    load(['../simout/poslog_ext_' id idmod{index} '.mat'], 'belief');
    beliefs{index} = belief;
end
areas = zeros(length([beliefs{1}.area]), length(idmod));
for index = 1:length(idmod)
    areas(:,index) = [beliefs{index}.area];
end
figure(3)
bar(areas)