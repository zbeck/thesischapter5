# -*- coding: utf-8 -*-
"""
Created on Tue Mar 01 17:06:41 2016

@author: Zoli
"""
import scipy.io

rlocs = scipy.io.loadmat('rescue_locations_haiti.mat')['resq_tasks']
#print([len(a[0]) for a in rlocs])
index = 0
for e in rlocs:
    e[0].tofile('haiti/rlocs%d.npa' % (index,))
    index += 1