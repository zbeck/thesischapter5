load('filtered_haiti_data.mat')
nbuildings = size(buildings, 1);
nsample = 60000;
chunk_size = 1000;
sampled_buildings = datasample(buildings, nsample, 'Replace', false);
mindistance = zeros([1, nsample]);
for chunk_start = 1:chunk_size:nsample
    chunk = chunk_start:(chunk_start+chunk_size-1);
    x_diff = repmat(buildings(:,1), [1, chunk_size]) - repmat(sampled_buildings(chunk,1)', [nbuildings, 1]);
    y_diff = repmat(buildings(:,2), [1, chunk_size]) - repmat(sampled_buildings(chunk,2)', [nbuildings, 1]);
    dist = sqrt(x_diff.^2 + y_diff.^2);
    maxdist = max(max(dist));
    dist(dist==0) = maxdist;
    mindistance(chunk) = min(dist);
end
hist(mindistance(mindistance<100), 100)
mean_min_dist = mean(mindistance);
%mean min dist is 9.07 m