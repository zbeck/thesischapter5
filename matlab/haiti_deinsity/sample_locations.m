function task_pos = sample_locations(expected_number, weights, random_task_number)
    load('filtered_haiti_data.mat', 'buildings')
    if (nargin < 2) || isempty(weights)
        factor = 2.5;
        weights = [1 factor factor^2 factor^3 factor^4];
    end
    if (nargin < 3)
        random_task_number = 1;
    end
    %nbuildings = size(buildings, 1);
    if random_task_number
        ntasks = random('Poisson', expected_number);
    else
        ntasks = expected_number;
    end
    w = weights(buildings(:,3));
    %sum(weights(buildings(buildings(:,3)==5,3)))/
    %expected_number/sum(weights(buildings(:,3)))*weights(5)
    mean_locs = datasample(buildings, ntasks, 'Weights', w);
    mean_locs = mean_locs(:,1:2);
    task_pos = mvnrnd([0,0], [1,1]*9.07, ntasks);
    task_pos = task_pos+mean_locs;