function visualisepaths(runID)
    if(nargin < 1)
        runID = 0;
    end
    offset = [0 1388];
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\v_18mresults\run11G1\';
    %% Read data
    currentDir = cd;
    load(['IRIDIS/poslog' int2str(runID) '.mat'])
    cd(simPath)
    eval(['Sim' int2str(runID)])
    cd(currentDir)
    %% plot path differences
    tswidth = size(TimeStepData,2);
    origpos = TimeStepData(:,setdiff(7:tswidth, 9:3:tswidth));
    origpos = origpos + repmat(offset, size(origpos).*[1 0.5]);
    newpos = pos(1:10:end, :);
    %make two position logs the same length
    if (size(newpos,1) > size(origpos,1))
        origpos = [origpos; repmat(origpos(end,:), [size(newpos,1)-size(origpos,1) 1])];
    else
        newpos = [newpos; repmat(newpos(end,:), [size(origpos,1)-size(newpos,1) 1])];
    end
%     origpos = origpos(1:2, :);
%     newpos = newpos(1:2, :);
%     int32(origpos')
%     int32(newpos')
    stpintrvl = 100;
    for uav = 1:(size(newpos,2)/2)
        plot([origpos(1:stpintrvl:end,uav*2-1)'; newpos(1:stpintrvl:end,uav)'],...
            [origpos(1:stpintrvl:end,uav*2)'; newpos(1:stpintrvl:end,uav+size(newpos,2)/2)'], 'k:')
        hold on
    end
    plot(origpos(:,1:2:end), origpos(:, 2:2:end), 'LineWidth', 2)
    plot(newpos(:,1:(size(newpos,2)/2)), newpos(:,(size(newpos,2)/2+1):end), 'LineWidth', 2)
    hold off
    axis equal
end