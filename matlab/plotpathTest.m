figure()
hold off
plot(taskDone(:,1), taskDone(:,2), 'rx')
hold on
plot(taskDone(:,3), taskDone(:,4), 'bo')
plot(squeeze(pos(:,:,1)), squeeze(pos(:,:,2)), 'g-')
axis equal