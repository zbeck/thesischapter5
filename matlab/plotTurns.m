load('optimiseTurn.mat')
figure(2)
hold off
for path = pos
    plot(path{1}(:,1), path{1}(:,2))
    hold on
end
plot(points(:,:,1), points(:,:,2),'rx')
plot(points(:,end,1), points(:, end,2),'g-')
p = pos{8};
range = p(end, 2);
steps = (p(2:end, :) - p(1:(end-1), :))./repmat(sqrt((p(2:end, 1) - p(1:(end-1), 1)).^2 + (p(2:end, 2) - p(1:(end-1), 2)).^2), [1 2]);
plot([p(1:(end-1), 1), p(1:(end-1), 1)-steps(:,2)*range]', [p(1:(end-1), 2), p(1:(end-1), 2)+steps(:,1)*range]', 'r--')
plot([p(1:(end-1), 1), p(1:(end-1), 1)+steps(:,2)*range]', [p(1:(end-1), 2), p(1:(end-1), 2)-steps(:,1)*range]', 'g--')
plot([0 2*range 2*range 0], [-2*range -2*range 2*range 2*range], 'k-', 'LineWidth', 2)
axis equal
%text(turns(:,end,1), turns(:,end,2), cellstr(num2str(turnlen')))