id = '14';
idmod = '-6';
% load(['../simout/test/dual_log' id idmod '.mat']);
% load(['../simout/dual_log' id idmod '.mat']);
load(['../simout/20160703B/dual_log' id idmod '.mat']);
figure(1)
hold off

plot(rpos(:,:,1), rpos(:,:,2))
hold on
plot(spos(:,:,1), spos(:,:,2), 'LineWidth', 2)
rposx = rpos(:,:,1);
rposy = rpos(:,:,2);
stopped = (diff(rposx)==0.) & (diff(rposy)==0.);
stoppos = [rposx(stopped), rposy(stopped)];
uspos = unique(stoppos,'rows');
plot(uspos(:,1), uspos(:,2), 'kx')
plot(rtaskStart(:,1), rtaskStart(:,2), 'mx')
[rt, Io, In] = unique(rtaskStart(:,5), 'last');
text(rtaskStart(Io,1), rtaskStart(Io,2), cellstr(num2str(rt)), 'VerticalAlignment','bottom', ...
                                              'HorizontalAlignment','right')

plot(resqTasks(:,1), resqTasks(:,2), 'ro', 'linewidth', 2)
text(resqTasks(:,1), resqTasks(:,2), cellstr(num2str([0:(length(resqTasks)-1)]')), 'VerticalAlignment','bottom', ...
                                              'HorizontalAlignment','right')
plot(searchTasks(:,1), searchTasks(:,2), 'gx', 'linewidth', 2)

mp = reshape([searchLog.belief.mean_position], 2, [])';
plot(mp(:,1), mp(:,2), 'b+')
axis equal
mean(rtaskDone(:,2))

figure(2)

subplot(211)
% plot(timing(:,2)-timing(1,2),timing(:,1), 'bo-')
selection = [diff(timing(:,2)) > 20; false];
plot(timing(selection,2)-timing(1,2), timing(selection,1), 'b.')
hold on
plot(timing(~selection,2)-timing(1,2), timing(~selection,1), 'r.')
hold off
ax = gca;
ax.YTick = 0:(length(timing_names)-1);
ax.YTickLabel = cellstr(timing_names);

subplot(212)
rdiff = diff(rpos);
rspeed = sum(rdiff.^2, 3);
plot(rspeed+repmat(0:11, [size(rspeed,1),1]))

figure(3)
plot(diff(timing(:,2)),timing(1:(end-1),1), 'rx')
ax = gca;
ax.YTick = 0:(length(timing_names)-1);
ax.YTickLabel = cellstr(timing_names);
% 
% figure(4)
% dt = diff(timing(:,2));
% time_spent = zeros(length(timing_names), 1);
% for tim = 1:length(timing_names)
%     time_spent(tim) = sum(dt(timing(1:(end-1),1) == tim-1));
% end
% plot(time_spent, 1:length(timing_names))
% ax = gca;
% ax.YTick = 1:length(timing_names);
% ax.YTickLabel = cellstr(timing_names);

figure(4)
clf
plot(repmat(rtaskStart(Io,5), [1,2])', [rtaskStart(Io,6) rtaskStart(Io,6)-rtaskStart(Io,3)]', 'rx-')
hold on
plot(repmat(rtaskStart(Io,5), [1,2])', [rtaskStart(Io,6) rtaskStart(Io,6)-rtaskStart(Io,4)]', 'bx-')
plot(rtaskDone(:,1), rtaskDone(:,2), 'go')

figure(5)
clf
sdist = sqrt((spos(:,1,1)-spos(:,2,1)).^2+(spos(:,1,2)-spos(:,2,2)).^2);
tooclose = sdist<100;
plot(spos(:,1,3), sdist)
figure(1)
plot(spos(tooclose,:,1), spos(tooclose,:,2), '.', 'markersize', 16)