hold off
d2 = repmat(distances', [1 length(angles)]);
semilogx(d2, d2./times)
legend(strread(num2str(angles/pi*180),'%s'))
hold on
% F = @(x, xdata)22./(1+x(1).*60./xdata+x(2).*60./xdata+x(3).*3600./xdata./xdata);
F = @(x, xdata)22./(1+x(1).*60./xdata+x(2).*3600./xdata./xdata);
x0 = [pi/2-1, 1/2];
x = zeros(length(angles), 2);
for index=1:length(angles)
    [x(index,:),resnorm,~,~,output] = lsqcurvefit(F,x0,distances',distances'./times(:, index));
    plot(distances, F(x(index,:), distances), 'k:')
end

x=[0 0; x(2:end, :)];
%weights according to timing accuracy
w = (max(sum(0.1./times, 2))+0.001 - sum(0.1./times ,2)).^3;

X1 = @(b,x) (sqrt(x)*b(1) + exp(x*b(2))*b(3) -b(3))*b(4);
x10 = [1 1 1 1];

X2 = @(b, x)(cos(x*b(1)+b(2))-cos(b(2)))*b(3);
x20=[2 -pi-1 1.1];

nlm1 = fitnlm(angles',x(:,1),X1,x10);
wnlm1 = fitnlm(angles',x(:,1),X1,x10,'Weight',w);
wnlm2 = fitnlm(angles',x(:,2),X2,x20,'Weight',w);

hold off
plot(angles/pi*180, [x predict(wnlm1, angles') predict(wnlm2, angles') predict(nlm1, angles')])