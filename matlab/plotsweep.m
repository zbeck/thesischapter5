load('sweeppattern.mat')
figure(2)
hold off
plot(pos(:,1), pos(:,2),'b-')
hold on
plot(points(:,1), points(:,2),'bo')
plot([points(:,1) points(:,1)+cos(points(:,3))*100]', [points(:,2) points(:,2)+sin(points(:,3))*100]','r-')

plot(nextTilePos(:,1), nextTilePos(:,2), 'g-', 'LineWidth', 2)

axis equal

p = pos;
range =p(1, 2);
if p(end,2)<=0
    area = p(end,1)+range;
else
    area = p(end,1);
end
steps = (p(2:end, :) - p(1:(end-1), :))./repmat(sqrt((p(2:end, 1) - p(1:(end-1), 1)).^2 + (p(2:end, 2) - p(1:(end-1), 2)).^2), [1 2]);
% plot([p(1:(end-1), 1), p(1:(end-1), 1)-steps(:,2)*range]', [p(1:(end-1), 2), p(1:(end-1), 2)+steps(:,1)*range]', 'r-')
% plot([p(1:(end-1), 1), p(1:(end-1), 1)+steps(:,2)*range]', [p(1:(end-1), 2), p(1:(end-1), 2)-steps(:,1)*range]', 'g-')
%plot([0 area area 0 0], [0 0 area area 0], 'k-', 'LineWidth', 2)

angle = 0:0.01:(2*pi);
%plot(sin(angle)*60+points(11,1)+60, cos(angle)*60+points(11,2), 'r-')
% plot(sin(angle)*60+points(6,1)-60/sqrt(2), cos(angle)*60+points(6,2)+60/sqrt(2), 'r-')
% plot(sin(angle)*60+points(4,1), cos(angle)*60+points(4,2)-60, 'r-')
% ppos = 12;
% plot([0,10]+points(ppos,1), [0,10]+points(ppos,2), 'r-')