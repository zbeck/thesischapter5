rect = [-62.5 -62.5; -62.5 62.5; 62.5 62.5; 62.5 -62.5; -62.5 -62.5];
simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\v_18mresults\run11G1\';
runID = 0;
currentDir = cd;
load(['simout/simoutput' int2str(runID) '.mat'])
cd(simPath)
eval(['Sim' int2str(runID)])
cd(currentDir)
xplorex = repmat(rect(:,1),[1 size(TaskTime,1)]) + repmat(TaskTime(:,1)', [size(rect, 1) 1]);
xplorey = repmat(rect(:,2),[1 size(TaskTime,1)]) + repmat(TaskTime(:,2)', [size(rect, 1) 1]);
plot(xplorex, xplorey)
axis equal