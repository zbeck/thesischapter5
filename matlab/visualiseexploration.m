function visualiseexploration(runID, showgrid)
    if(nargin < 2)
        runID = 0;
        showgrid = true;
    end
    %% Read data
    load(['simout/simoutput' int2str(runID) '.mat'])
    load(['IRIDIS/poslog' int2str(runID) '.mat'])

    if(showgrid)
        %% Visualise exploration grid
        gridSize = 30;
        xrange = min(resqTasks(:,1)) : ((max(resqTasks(:,1))-min(resqTasks(:,1)))/gridSize) : max(resqTasks(:,1));
        yrange = min(resqTasks(:,2)) : ((max(resqTasks(:,2))-min(resqTasks(:,2)))/gridSize) : max(resqTasks(:,2));
        [X,Y] = meshgrid(xrange, yrange);
        Z = griddata(resqTasks(:,1), resqTasks(:,2), resqTasks(:,4), X, Y);
        mesh(X, Y, Z)
        colormap([0 0 0])
        alpha(0)
    end

    %% Process rescue
    orig = sortrows(resqTasks(:,1:4), [1 2]);
    new = sortrows(taskDone, [1 2]);
    if(size(orig,1) ~= size(new,1))
        disp([int2str(runID) ' doesn''t mach (' int2str(size(orig,1)) ' -- ' int2str(size(new,1)) ')']);
        return
    end
    taskTimeDiff = new(:,3)-orig(:,3);
    poscolor = [.2 .25 1];
    negcolor = [1 .15 .1];
    ispos = taskTimeDiff > 0;
    c = ispos * poscolor + ~ispos * negcolor;

    %% Visualise rescue
    plot3([orig(:,1)';orig(:,1)'], [orig(:,2)';orig(:,2)'], [orig(:,4)';min([orig(:,3)'; new(:,3)'])], 'Color', [0 0 0])
    hold on
    for index = 1:size(orig, 1)
        plot3([orig(index,1)';orig(index,1)'], [orig(index,2)';orig(index,2)'], [new(index,3)';orig(index,3)'], 'Color', c(index,:), 'LineWidth', 3)
    end
end