load('mctsScerriTest.mat')
figure(2)
hold off
h = plot(samples(1:60,1),samples(1:60,2), 'r.', 'MarkerEdgeColor',[1,0,0], 'MarkerSize', 5);
hold on
plot(obs(:,1), obs(:,2), 'bo-')
plot(best_action(1), best_action(2),'k+')
actions = mctsPath;
%plot(actions(:,1),actions(:,2), 'gx')
plot([actions(:,1) actions(:,1)+100*cos(actions(:,3))]', [actions(:,2) actions(:,2)+100*sin(actions(:,3))]', 'g-')


axis equal