load('test.mat')
plot3(coordsx, coordsy, 1:length(coordsx), 'ro-')
hold on
for row=1:size(tasks, 1)
    plot3([tasks(row,1) tasks(row,1)], [tasks(row,2) tasks(row,2)], [1 length(coordsx)], 'g-')
end
hold off
