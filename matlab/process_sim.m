evalPath = 'd:\Python\dec_dualsar\';
simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\v_18mresults\run11G1\';

TimeConvertToSeconds = 10;
simIds = 0:127;
simIds = setdiff(simIds, [107 ]);
startPath = cd;

for simId = simIds
    cd(simPath)
    eval(strcat('Sim', int2str(simId)))
    stepdataW = size(TimeStepData, 2);
    stepdataL = size(TimeStepData, 1);

    maxTime = 2*stepdataL;
    undone = ~(TaskTime(:,4)+1);
    search = ~TaskTime(:,3);
    nresq = sum(~search);
    taskTimes = (TaskTime(:,[4 3])+undone*[1 0])./TimeConvertToSeconds;
    [~, sridx] = sort(search);

    resqTasks = [TaskTime(sridx(1:nresq), [1 2]) taskTimes(sridx(1:nresq), :)];
    [~, rtidx] = sort(resqTasks(:,3));
    resqTasks = resqTasks(rtidx, :);
    resqTasks = resqTasks*diag([1 -1 1 1]);

    searchTasks = [TaskTime(sridx((nresq+1):end), [1 2]) taskTimes(sridx((nresq+1):end), :)];
    [~, stidx] = sort(searchTasks(:,3));
    searchTasks = searchTasks(stidx, :);
    searchTasks = searchTasks*diag([1 -1 1 1]);

    save(['simoutput' int2str(simId) '.mat'], 'resqTasks', 'searchTasks')
end

cd(startPath)