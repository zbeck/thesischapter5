runIDs = 0:106;
%runIDs = setdiff(runIDs, [107 ]);
figure()
diff = zeros([length(runIDs), 1]);
for runID = runIDs
    %visualiseexploration(runID, false);
    %% Read data
    load(['simout/simoutput' int2str(runID) '.mat'])
    load(['IRIDIS/poslog' int2str(runID) '.mat'])
    %load(['IRIDIS/v03_flippedDensity/poslog' int2str(runID) '.mat'])
    diff(runID+1) = 100*(mean(resqTasks(:,3))-mean(taskDone(:,3)))/mean(resqTasks(:,3));
end
plot(sort(diff))
hold on
plot([1 length(diff)], [mean(diff) mean(diff)], 'r-')
axis tight
ax = axis();
axis([ax(1:2) min(0, ax(3)) max(0, ax(4))])
ylabel('Improvement [%]')
grid off
hold off