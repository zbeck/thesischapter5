load('sweeplen.mat')
hold off
% scatter3(data(:,1), data(:,2), data(:,3), 20, [1, .2, .2])
% hold on
% scatter3(data(:,1), data(:,2), data(:,4), 20, [.2, 1, .2])
% scatter3(data(:,1), data(:,2), data(:,5), 20, [.2, .2, 1])
% scatter3(data(:,1), data(:,2), data(:,6), 20, [0, 1, 1])
diff = [(data(:,4)-data(:,3))./data(:,3) (data(:,6)-data(:,5))./data(:,5)];
scatter3(data(:,1), data(:,2), diff(:,1), 10, [1, .2, .2])
hold on
scatter3(data(:,1), data(:,2), diff(:,2), 10, [.2, .2, 1])