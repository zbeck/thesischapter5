id = '11';
idmod = '-4';
load(['../simout/dual_poslog' id idmod '.mat']);
load(['../simout/simoutput' id '.mat']);
figure(1)
hold off

plot(spos(:,:,1), spos(:,:,2), 'LineWidth', 2)
hold on
plot(rpos(:,:,1), rpos(:,:,2))

plot(resqTasks(:,1)*10, -resqTasks(:,2)*10, 'ro', 'linewidth', 2)
axis equal
mean(rtaskDone(:,3))

figure(2)
phi = 0:0.1:2*pi;
ucircle = [cos(phi)', sin(phi)'];
csize = length(phi);
plot(rtaskDone(:,1), rtaskDone(:,2), 'kx')
hold on
for index = 1:size(rtaskDone,1)
    r = sqrt(rtaskDone(index, 3)/pi);
    cir = ucircle*r + repmat(rtaskDone(index, 1:2), [csize,1]);
    plot(cir(:,1), cir(:,2), 'b')
end
axis equal
plot(resqTasks(:,1)*10, resqTasks(:,2)*10, 'rx')