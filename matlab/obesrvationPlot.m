load('getObservationsTest.mat')
hold off
plot(pos(:,1), pos(:,2),'b-')
hold on
plot(points(:,1), points(:,2),'bo')
plot(obs(:,1), obs(:,2), 'rx')
axis equal