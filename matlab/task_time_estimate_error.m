%% load data
datafile = '../simout/all_rescuetimes.mat';
ids = 0:127;
ids = setdiff(ids, [99 41 61]);
idmods = {'-0', '-1', '-2', '-3'};%, '-4', '-5'};

loaded = false;
if exist(datafile, 'file')
    load(datafile)
    loaded = true;
end
if ~loaded || (size(all_rescuetimes,2) ~= length(idmods))
    all_rescuetimes = cell(length(ids), length(idmods));
    conf_id = 1;
    for idnum = ids
        id = num2str(idnum);
        mod_id = 1;
        for idmod = idmods
            fname = strcat('../simout/dual_log', id, idmod, '.mat');
            load(fname{1});
            all_rescuetimes{conf_id, mod_id} = unique(rtaskStart(:,3:4), 'rows');
        end
        conf_id = conf_id +1;
    end
    save(datafile, 'all_rescuetimes')
end

%% plot normalised distribution

xy = cat(1, all_rescuetimes{:});
rel_error = bsxfun (@rdivide, xy(:, 1), xy(:,2));
distvals = rel_error.^(1/9);
figure(1)
clf
hold on
[hist_vals, centers] = hist(distvals, 300);
hist(distvals, 300)

%% fit distribution

pd = fitdist(distvals, 'Normal')
% pd = makedist('Normal', 'sigma', sqrt(std(distvals)), 'mu', 1);
x_values = 0:0.01:2;
y_values = pdf(pd, x_values);
[~, oneindex] = min(abs(centers-1));
normalise_val = hist_vals(oneindex) / pdf(pd,1);
plot(x_values, y_values*normalise_val)
ax = axis;
axis([-5, 5, ax(3:end)])

%% plot dependency of estimate value
figure(2)
clf
plot(xy(:,2), distvals, 'bx')

figure(1)