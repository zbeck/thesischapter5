id = '29';
idmod = '-0';
load(['../simout/poslog_ext_' id idmod '.mat']);
load(['../simout/simoutput' id '.mat']);
figure(1)
hold off

plot(pos(:,:,1), pos(:,:,2))
hold on

obsloc = zeros(length(reward),2);
for i=1:length(reward)
    [~, bestpos] = min(abs(pos(:, 1, 3)-reward(i,end)));
    obsloc(i,:) = pos(bestpos, 1, 1:2);
end
colormap(jet)
nonzero = (reward(:,1) < reward(:,3));
%scatter(obsloc(:,1), obsloc(:,2), 25, log10(reward(:,1)+1), 'filled')
scatter(obsloc(nonzero,1), obsloc(nonzero,2), 25, log10(reward(nonzero,3)-reward(nonzero,1)+1), 'filled')
for i=1:length(reward)
    [~, bestpos] = min(abs(pos(:, 2, 3)-reward(i,end)));
    obsloc(i,:) = pos(bestpos, 2, 1:2);
end
%colormap(summer)
nonzero = (reward(:,2) < reward(:,4));
%scatter(obsloc(nonzero,1), obsloc(nonzero,2), 25, log10(reward(nonzero,2)+1), 'filled')
scatter(obsloc(nonzero,1), obsloc(nonzero,2), 25, log10(reward(nonzero,4)-reward(nonzero,2)+1), 'filled')
senspos = [belief.mean_position];
sensarea = [belief.area];
%scatter(senspos(1:2:end), -senspos(2:2:end), sensarea, sensarea, 'filled')
plot(resqTasks(:,1)*10, -resqTasks(:,2)*10, 'ro', 'linewidth', 2)
axis equal
figure(2)
subplot(211)
hold off
semilogy(reward(1:1698,1)+1, 'linewidth', 2)
a = axis;
hold on
semilogy(reward(1:1698,3)+1)
axis(a)
subplot(212)
hold off
semilogy(reward(1:1698,2)+1)
a = axis;
hold on
semilogy(reward(1:1698,4)+1)
axis(a)