id = '56';
idmods = {'-0', '-1', '-2'};%, '-3', '-4', '-5'};
folder = '../simout/new/';
load(strcat(folder, 'dual_log', id, idmods{1}, '.mat'))
rescuetimes = zeros(size(resqTasks,1), size(rtaskStart, 2), length(idmods));
runid = 1;
for idmod = idmods
    fname = strcat(folder, 'dual_log', id, idmod, '.mat');
    load(fname{1});
    [rt, Io, ~] = unique(rtaskStart(:,5), 'last');
    if rt(1) == -1
        rt = rt(2:end);
        Io = Io(2:end);
    end
    rescuetimes(:, :, runid) = rtaskStart(Io, :);
    runid = runid+1;
    %size(unique(rtaskDone(:,1:2),'rows'))
end
figure(1)
clf
bar(squeeze(mean(rescuetimes(:, 6, :), 1)), 'b')
ax = gca;
ax.XTick = 1:length(idmods);
ax.XTickLabel = idmods;
hold on
bar(squeeze(mean(rescuetimes(:, 6, :), 1))-squeeze(mean(rescuetimes(:, 3, :), 1)), 'g')
bar(squeeze(mean(rescuetimes(:, 3, :), 1)), 'r')

figure(2)
clf
loglog(rescuetimes(:,3,1), rescuetimes(:,4,1), 'kx')
hold on
loglog(rescuetimes(:,3,2), rescuetimes(:,4,2), 'rx')
loglog(rescuetimes(:,3,3), rescuetimes(:,4,3), 'gx')
if(size(rescuetimes, 3) > 3)
    loglog(rescuetimes(:,3,4), rescuetimes(:,4,4), 'bx')
end
loglog([1 10000], [1 10000], 'r')
axis equal
xlabel('real task times')
ylabel('estimated task times')
hold off

%% choosing multiplying factor for overestimation

%chosen value: 6.0 for only 5% of values being underestimated
% figure(3)
% clf
% ratio = rescuetimes(:,4,:)./rescuetimes(:,5,:);
% ratio = reshape(ratio, numel(ratio), 1);
% ratiopoints = exp(0:-0.01:-3);
% ratmat = (ratio * ratiopoints) > 1;
% plot(1./ratiopoints, sum(ratmat, 1)./numel(ratio)) 
%hist(ratio)

%% checking task time distribution
figure(3)
clf
ymax = 0;
% hist(squeeze(rescuetimes(:, 6, :)),30)
for mod = 1:length(idmods)
    subplot(1,length(idmods),mod)
    rangebar([rescuetimes(:,6,mod)-rescuetimes(:,3,mod), rescuetimes(:,6,mod)], .5);
    h=rangebar([rescuetimes(:,6,mod)-rescuetimes(:,4,mod), rescuetimes(:,6,mod)], .5);
    h.EdgeColor = [1 0 0];
    text(1:length(resqTasks), rescuetimes(:,6,mod), cellstr(num2str([0:(length(resqTasks)-1)]')), 'VerticalAlignment','bottom', ...
                                               'HorizontalAlignment','right')
    axs = axis;
    ymax = max(ymax, axs(4));
end

for mod = 1:length(idmods)
    subplot(1,length(idmods),mod)
    axs = axis;
    axis([axs(1:2), 0,ymax])
end


figure(1)