%% settings
%colours from http://www.colourlovers.com/palette/2707023/Cascion_Davi_CLAD?widths=0
colours = [[160/255, 21/255, 47/255]; [231/255, 78/255, 46/255]; ...
    [223/255, 241/255, 10/255]; [36/255, 224/255, 174/255]; [25/255, 7/255, 46/255]];

%% read data
safety_distance = 50;

folder = '../simout/20160706/';
runName = strsplit(folder, '/');
runName = runName(end-1);
datafile = [folder 'aggregated.mat'];
ids = 0:127;

idmods = {'-7', '-0', '-8', '-1', '-2', '-6', '-9', '-3', '-4', '-5'};
lineargs = {'-', ':', '-', ':', '--', '-', '-', ':', '--', '-'};

linecolor = colours([1 1 2 2 3 3 4 4 5 5], :);

modNames = containers.Map();
modNames('-0') = 'MCTS HOP';
modNames('-1') = 'Indep HOP';
modNames('-2') = 'CollabA';
modNames('-3') = 'Indep HOP Man';
modNames('-4') = 'CollabA Man';
modNames('-5') = 'Collab Man';
modNames('-6') = 'Collab';
modNames('-7') = 'MCTS Grad';
modNames('-8') = 'Indep Grad';
modNames('-9') = 'Indep Grad Man';

loaded = false;
if exist(datafile, 'file')
    load(datafile)
    loaded = true;
end
if ~loaded || (size(rescuetimes_mean, 2) ~= length(idmods)) ||...
        (size(rescuetimes_mean, 1) ~= length(ids)) ||...
        (safety_distance_save ~= safety_distance)
    sprintf('File "aggregated.mat" incomplete or missing. Please run compare_nosweep_performance.m to generate results.')
    return
end

correctids = all(rescuetimes_mean>0, 2);
stddiv = sqrt(sum(correctids))*1.959963984540/2;

%% filter data columns

modfilter = [true true true true false true true true false true];
idmods = idmods(modfilter);
lineargs = lineargs(modfilter);
linecolor = linecolor(modfilter, :);
rescuetimes_mean = rescuetimes_mean(:, modfilter);
tasktimes_mean = tasktimes_mean(:, modfilter);
all_rescuetimes = all_rescuetimes(:, :, modfilter);

%% plot absolute avg task time data
figure(1)
clf
hold off
x_values = mean(rescuetimes_mean(correctids, :), 1);
std_values = std(rescuetimes_mean(correctids, :), 1)/stddiv;
superbar(x_values, 'E', std_values, 'BarFaceColor', linecolor)
ax = gca;
ax.XTick = 1:length(idmods);
ax.XTickLabel = values(modNames, idmods);
ax.XTickLabelRotation = 45;
roundto = 100;
ymin = floor2(min(x_values)-max(std_values), roundto);
ymax = ceil2(max(x_values)+max(std_values), roundto);
axis([0.5 length(idmods)+0.5 ymin ymax])
ylabel('Time [s]')
xlabel('Approach')
set(gcf, 'PaperPosition', [-0.5 0 15 9]);
set(gcf, 'PaperSize', [13.1 8.5]);
saveas(gcf, 'haiti31-task_end.pdf');

figure(2)
clf
hold off
x_values = mean(tasktimes_mean(correctids, :), 1);
std_values = std(tasktimes_mean(correctids, :), 1)/stddiv;
superbar(x_values, 'E', std_values, 'BarFaceColor', linecolor)
ax = gca;
ax.XTick = 1:length(idmods);
ax.XTickLabel = values(modNames, idmods);
ax.XTickLabelRotation = 45;
roundto = 20;
ymin = floor2(min(x_values)-max(std_values), roundto);
ymax = ceil2(max(x_values)+max(std_values), roundto);
axis([0.5 length(idmods)+0.5 ymin ymax])
ylabel('Time [s]')
xlabel('Approach')
set(gcf, 'PaperPosition', [-0.5 0 15 6]);
set(gcf, 'PaperSize', [13.1 5.7]);
saveas(gcf, 'haiti31-task_len.pdf');

%% plot realtive data

figure(3)
clf
rt_ratio =  bsxfun (@rdivide, rescuetimes_mean(correctids,:), rescuetimes_mean(correctids,1));
x_values = mean(rt_ratio, 1);
std_values = std(rt_ratio,1)/stddiv;
superbar(x_values, 'E', std_values, 'BarFaceColor', linecolor)
ymin = floor2(min(x_values)-max(std_values), 0.05);
ymax = 1.05;
axis([0.5 length(idmods)+0.5 ymin ymax])

%% safe distance plot

% figure(4)
% clf
% bar(mean(within_safety_region, 1))
% 
% compareids = [6, 10];
% if max(compareids) <= size(rescuetimes_mean, 2)
%     figure(5)
%     clf
%     subplot(121)
%     xy = rescuetimes_mean(correctids, compareids);
%     plotPointDensity(xy, 15);
%     hold on
%     plot(xy(:,1), xy(:,2), 'ro');
%     text(xy(:,1), xy(:,2), cellstr(num2str(ids(correctids)')),...
%         'VerticalAlignment','bottom', 'HorizontalAlignment','right')
%     subplot(122)
%     rt_ratio2 =  bsxfun(@rdivide, rescuetimes_mean(correctids, compareids),...
%         rescuetimes_mean(correctids,compareids(1)));
%     bar(mean(rt_ratio2, 1))
%     hold on

%     errorbar(mean(rt_ratio2, 1), std(rt_ratio2,1)/stddiv)
%     ax = gca;
%     ax.XTickLabel = values(modNames, idmods(compareids));
% end
                                           
%% completition rate over time

x_vals = 0.01:30:7200;
y_vals = zeros([length(idmods), length(x_vals), 2]);
unfinished = sum(squeeze(all_rescuetimes(:, 1, :)==0),1);
ntasks = size(all_rescuetimes, 1);
for mod_id = 1:length(idmods)
    task_finish = sort(all_rescuetimes(:, 1, mod_id));
    task_start = sort(all_rescuetimes(:, 2, mod_id));
    for index = 1:length(x_vals)
        y_vals(mod_id,index,1) = bsearch(task_finish, x_vals(index))-unfinished(mod_id);
        y_vals(mod_id,index,2) = bsearch(task_start, x_vals(index))-unfinished(mod_id);
    end
end
y_vals = y_vals./ntasks;
figure(4)
clf
hold off
for mod_id = 1:length(idmods)
    plot(x_vals, squeeze(y_vals(mod_id,:,1)), lineargs{mod_id}, 'Color', linecolor(mod_id,:))
    hold on
end
battery_life = 1440; % 24 mins
plot([battery_life battery_life], [0 1], 'LineStyle', '--', 'Color', [0.6 0.6 0.6])
legend(values(modNames, idmods))
axis([0 max(x_vals) 0 1])
xlabel('Time [s]')
ylabel('Completion Ratio')
annotation('line', [0.285 0.43], [0.35 0.29], 'Color', [0.6 0.6 0.6],...
    'LineStyle', '-')
annotation('line', [0.285 0.43], [0.55 0.61], 'Color', [0.6 0.6 0.6],...
    'LineStyle', '-')

axes('Position',[.43 .29 .32 .32])
box on
[~, x_ind] = min(abs(x_vals-battery_life));
superbar(y_vals(:,x_ind,1), 'BarFaceColor', linecolor);
ax = gca;
ax.XTick = 1:length(idmods);
ax.XTickLabel = values(modNames, idmods);
ax.XTickLabelRotation = 45;
axvals = axis;
axis([0.5 length(idmods)+0.5 floor2(min(y_vals(:,x_ind,1)), 0.1) ceil2(max(y_vals(:,x_ind,1)), 0.05)])

set(gcf, 'PaperPosition', [-1.3 -0.2 20 13]);
set(gcf, 'PaperSize', [17 12]);
saveas(gcf, 'haiti31-task_progress.pdf');
