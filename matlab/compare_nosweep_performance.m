%% read data
safety_distance = 50;

folder = '../simout/';
datafile = [folder 'aggregated.mat'];
ids = 0:127;
% ids = 0:63;
% ids = setdiff(ids, [17 43]);
% ids = setdiff(ids, [84 ]);

idmods = {'-7', '-0', '-8', '-1', '-2', '-6', '-9', '-3', '-4', '-5'};

modNames = containers.Map();
modNames('-0') = 'Scerry HOP';
modNames('-1') = 'Indep HOP';
modNames('-2') = 'CollabA';
modNames('-3') = 'Indep HOP Man';
modNames('-4') = 'CollabA Man';
modNames('-5') = 'CollabB Man';
modNames('-6') = 'CollabB';
modNames('-7') = 'Scherry Grad';
modNames('-8') = 'Indep Grad';
modNames('-9') = 'Indep Grad Man';

loaded = false;
if exist(datafile, 'file')
    load(datafile)
    loaded = true;
end
if ~loaded || (size(rescuetimes_mean, 2) ~= length(idmods)) ||...
        (size(rescuetimes_mean, 1) ~= length(ids)) ||...
        (safety_distance_save ~= safety_distance)
    safety_distance_save = safety_distance;
    rescuetimes_mean = zeros(length(ids), length(idmods));
    tasktimes_mean = zeros(length(ids), length(idmods));
    within_safety_region = zeros(length(ids), length(idmods));
    all_rescuetimes = cell(length(ids), 1);
    conf_id = 1;
    for idnum = ids
        id = num2str(idnum);
        load(strcat(folder, 'dual_log', id, idmods{1}, '.mat'))
        rescuetimes = zeros(size(resqTasks,1), size(rtaskStart, 2), length(idmods));
        mod_id = 1;
        for idmod = idmods
            fname = strcat(folder, 'dual_log', id, idmod, '.mat');
            load(fname{1});
            within_safety_region(conf_id, mod_id) = sum(...
                sqrt((spos(:,1,1)-spos(:,2,1)).^2+(spos(:,1,2)-spos(:,2,2)).^2)...
                < safety_distance);
            [rt, Io, In] = unique(rtaskStart(:,5), 'last');
            if rt(1) == -1
                rt = rt(2:end);
                Io = Io(2:end);
            end
            if length(Io) ~= size(resqTasks,1)
                fprintf('%d%s\n', idnum, idmod{1})
            else
                rescuetimes(:, :, mod_id) = rtaskStart(Io, :);
            end
            mod_id = mod_id+1;
        end
        rescuetimes_mean(conf_id, :) = squeeze(mean(rescuetimes(:, 6, :), 1));
        tasktimes_mean(conf_id, :) = squeeze(mean(rescuetimes(:, 4, :), 1));
        all_rescuetimes{conf_id} = rescuetimes(:, [6,6], :);
        all_rescuetimes{conf_id}(:,2,:) = rescuetimes(:,6,:)-(rescuetimes(:, 4, :)); 
        conf_id = conf_id +1;
    end
    all_rescuetimes = cat(1, all_rescuetimes{:});
    save(datafile, 'rescuetimes_mean', 'tasktimes_mean', 'all_rescuetimes',...
        'within_safety_region', 'safety_distance_save')
end
%% plot absolute data
correctids = all(rescuetimes_mean>0, 2);
stddiv = sqrt(sum(correctids))*1.959963984540/2;
figure(1)
clf
bar(mean(rescuetimes_mean(correctids, :), 1),'b')
hold on
errorbar(mean(rescuetimes_mean(correctids, :), 1), std(rescuetimes_mean(correctids, :), 1)/stddiv)
ax = gca;
ax.XTick = 1:length(idmods);
ax.XTickLabel = values(modNames, idmods);
bar(mean(rescuetimes_mean(correctids, :), 1)-mean(tasktimes_mean(correctids, :), 1), 'g')
errorbar(mean(rescuetimes_mean(correctids, :), 1)-mean(tasktimes_mean(correctids, :), 1), std(rescuetimes_mean(correctids, :)-tasktimes_mean(correctids, :), 1)/stddiv)
bar(mean(tasktimes_mean(correctids, :), 1), 'r')
errorbar(mean(tasktimes_mean(correctids, :), 1), std(tasktimes_mean(correctids, :), 1)/stddiv)

%% plot realtive data

figure(2)
clf
rt_ratio =  bsxfun (@rdivide, rescuetimes_mean(correctids,:), rescuetimes_mean(correctids,1));
bar(mean(rt_ratio, 1))
hold on
errorbar(mean(rt_ratio, 1), std(rt_ratio,1)/stddiv)

%% safe distance plot

figure(4)
clf
bar(mean(within_safety_region, 1))

compareids = [6, 10];
if max(compareids) <= size(rescuetimes_mean, 2)
    figure(5)
    clf
    subplot(121)
    xy = rescuetimes_mean(correctids, compareids);
    plotPointDensity(xy, 15);
    hold on
    plot(xy(:,1), xy(:,2), 'ro');
    text(xy(:,1), xy(:,2), cellstr(num2str(ids(correctids)')),...
        'VerticalAlignment','bottom', 'HorizontalAlignment','right')
    subplot(122)
    rt_ratio2 =  bsxfun(@rdivide, rescuetimes_mean(correctids, compareids),...
        rescuetimes_mean(correctids,compareids(1)));
    bar(mean(rt_ratio2, 1))
    hold on
    errorbar(mean(rt_ratio2, 1), std(rt_ratio2,1)/stddiv)
    ax = gca;
    ax.XTickLabel = values(modNames, idmods(compareids));
end
                                           
%% completition rate over time

figure(3)
clf
x_vals = 0:30:7200;
y_vals = zeros([length(idmods), length(x_vals), 2]);
ntasks = size(all_rescuetimes, 1);
for mod_id = 1:length(idmods)
    task_finish = sort(all_rescuetimes(:, 1, mod_id));
    task_start = sort(all_rescuetimes(:, 2, mod_id));
    for index = 1:length(x_vals)
        y_vals(mod_id,index,1) = bsearch(task_finish, x_vals(index));
        y_vals(mod_id,index,2) = bsearch(task_start, x_vals(index));
    end
end
y_vals = y_vals./ntasks;
plot(repmat(x_vals', [1, length(idmods)]), squeeze(y_vals(:,:,1))')