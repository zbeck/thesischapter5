# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 22:11:10 2015

@author: Zoli
"""
import numpy as np
from agent import dist, direction, Attitude
import time

class SearchProblem:
    def __init__(self, agents, tasks, nFixed, speed, distance_fcn = None, configurations = [], task_distance_table = None, debug = False):
        """Represents an MRTA problem.
        
        Arguments
        --------
        agents: list of Attitude
            the attitudes of agents (robots)
        tasks: [[x,y,t,<a>],...]
            x, y are positions, t is the length of a task, a is the activation time of a task (if applicable).
            The array must start with fixed (not sampled) tasks and then have the sampled tasks
        nFixed: int
            number of tasks that are not a result of random sampling
        speed: float
            speed of the agents' motion
        distance_fcn: function
            may replace dist() for calculationg distance between tasks. Format:
            distance_fcn(startpos, endpos, startHeading, endHeading)
        agent_distance_fcn: function
            may replace Problem.defdist for calculationg distance between tasks and agents
        configurations: list of headings
            list of possible headings of waypoints in radians
        task_distance_table: ntasks x ntasks x nconfigurations size numpy array of (distance, new_configuration) tuples
            can be initialised with the given table, if None the table is built.
        debug: bool
            Debug switch"""
        self.best_distance_tolerance = 350./speed
        self.solve_with_tolerance = True
        self.agents = agents
        self.debug = debug
        self.tasks = np.array(tasks, np.float)
        #if tasks do not include activation there is no need to check activation
        if (len(tasks) == 0) or (self.tasks.shape[1] == 3):
            self.use_activaiton = False
        else:
            self.use_activaiton = True
        self.nFixedTasks = nFixed
        self.SPEED = speed
        if distance_fcn is None:
            self.dist_fcn = self.defdist
        else:
            self.dist_fcn = distance_fcn
        self.buildTable(configurations, task_distance_table)
        
    def __str__(self):
        return '%d agents, %d tasks (%d fixed)' % (len(self.agents), len(self.tasks), self.nFixedTasks)
        
    def minDistConfig(self, start, end, startHeading, waitTime, isAgent):
        bestdist, bestconfig = self.dist_fcn(start, end, startHeading, isAgent)
        besttime = bestdist/self.SPEED + waitTime
        return besttime, bestconfig
            
    @staticmethod
    def defdist(startpos, endpos, startHeading, isAgent):
        return dist(startpos, endpos), 0
        
    def buildTable(self, configurations, task_distance_table):
        if len(configurations) == 0:
            configurations = [0.]
        if self.debug:
            print('dist table size is %dx%dx%d and %dx%d' % (len(configurations), len(self.tasks), len(self.tasks), len(self.agents), len(self.tasks)))
            t = time.time()
        if task_distance_table is None:
            self.distanceTable = np.empty((len(configurations), len(self.tasks), len(self.tasks)), [('length',float), ('end_config',int)])
            for b in range(len(self.tasks)):
                for a in range(len(self.tasks)):
                    for conf in range(len(configurations)):
                        self.distanceTable[conf,a,b] = self.minDistConfig(self.tasks[a][:2],
                            self.tasks[b][:2], configurations[conf], self.tasks[a][2], False)
        else:
            assert(task_distance_table.shape == (len(configurations), len(self.tasks), len(self.tasks)))
            self.distanceTable = task_distance_table
        self.agentDistance = np.empty((len(self.agents), len(self.tasks)), [('length',float), ('end_config',int)])
        for b in range(len(self.tasks)):
            for a in range(len(self.agents)):
                self.agentDistance[a,b] = self.minDistConfig(self.agents[a].loc,
                    self.tasks[b][:2], self.agents[a].getHeading(), self.agents[a].waitTime, True)
        if self.debug:
            print('dist table built, took %d s' % (time.time()-t,))
            
    def getDistance(self, configuration, start, end):
        if start < len(self.tasks):
            return self.distanceTable[configuration,start,end]
        else:
            return self.agentDistance[start-len(self.tasks),end]
    
    def solveGreedily(self):
        schedule = [[] for i in range(len(self.agents))]
        agentloc = np.arange(len(self.agents)) + len(self.tasks)
        agenttime = np.array([0]*len(self.agents), np.float)
        agentconfig = np.array([0]*len(self.agents))
        availtasks = np.array([True]*len(self.tasks), np.bool)
        if self.solve_with_tolerance:
            while np.any(availtasks):
                task_list = np.arange(len(self.tasks))[availtasks]
                possibilities = np.zeros((len(task_list), len(self.agents)))
                for a in np.arange(len(self.agents)):
                    for t in np.arange(len(task_list)):
                        possibilities[t, a], _ = self.getDistance(agentconfig[a], agentloc[a], task_list[t])
                possibilities += agenttime
                if self.use_activaiton:
                    possibilities = np.maximum(possibilities, self.tasks[availtasks,-1, np.newaxis])
                min_time = np.min(possibilities)
                best_tid, best_aid = np.where(possibilities < min_time+self.best_distance_tolerance)
                chosen_tid = np.min(best_tid)
                chosen_task = task_list[chosen_tid]
                possible_agents = best_aid[np.where(best_tid == chosen_tid)]
                chosen_agent = possible_agents[np.argmin(possibilities[chosen_tid, :][possible_agents])]
                time_diff, end_config = self.getDistance(agentconfig[chosen_agent], agentloc[chosen_agent], chosen_task)
                
                schedule[chosen_agent].append(chosen_task)
                agentloc[chosen_agent] = chosen_task
                agenttime[chosen_agent] += time_diff
                agentconfig[chosen_agent] = end_config
                availtasks[chosen_task] = False
        else:
            while True:
                mintime = np.inf
                agenttask_choice = [-1, -1, -1]
                #choice = [agent, task, configuration]
                for a in range(len(self.agents)):
                    for t in (task for task in range(len(self.tasks)) if availtasks[task]):
                        dist, endconfig = self.getDistance(agentconfig[a], agentloc[a], t)
                        if not self.use_activaiton:
                            ttime = agenttime[a]+dist
                        else:
                            ttime = max(agenttime[a]+dist, self.tasks[t][-1])
                        if (ttime < mintime) and (ttime <= self.agents[a].batteryTime):
                            mintime = ttime
                            agenttask_choice = [a, t, endconfig]
                if np.isinf(mintime):
                    break
                schedule[agenttask_choice[0]].append(agenttask_choice[1])
                agentloc[agenttask_choice[0]] = agenttask_choice[1]
                agenttime[agenttask_choice[0]] = mintime
                agentconfig[agenttask_choice[0]] = agenttask_choice[2]
                availtasks[agenttask_choice[1]] = False
        return schedule
        
    def getExecutionTimes(self, schedule):
        return self.getLowToHighFeedBackData(schedule)[:,0]
        
    def getLowToHighFeedBackData(self, schedule):
        """returns an array of [te, w] for each task
            te: execution times
            w: weights (number of critical tasks after a task (including itself))
        (the first self.nFixedTasks rows should not count for the feedback calculation)
        """
        if len(self.tasks) == 0:
            return np.zeros((0,2))
        schedulewa = [[i+len(self.tasks)]+schedule[i] for i in range(len(schedule))]
        # execution times with weights
        executionww = np.array([[np.inf, 0] for _ in self.tasks], np.float)
        for agent in range(len(self.agents)):
            t = 0
            config = 0
            for task in range(1, len(schedulewa[agent])):
                #first assume that no non-critical tasks are in the schedule
                executionww[schedulewa[agent][task]][1] = len(schedulewa[agent])-task
                dist, config = self.getDistance(config, schedulewa[agent][task-1], schedulewa[agent][task])
                t += dist
                if self.use_activaiton and t < self.tasks[task][-1]:
                    t = self.tasks[task][-1]
                    sub_value = executionww[schedulewa[agent][task]][1] - 1
                    executionww[schedulewa[agent][1:task]][1] -= sub_value
                executionww[schedulewa[agent][task]][0] = t
        return executionww
        
#    def getFeedback(self, schedule):
#        execution = (self.getExecutionTimes(schedule)[self.nFixedTasks:], np.float)[:,0])[:,np.newaxis]
#        fb = np.concatenate((self.tasks[self.nFixedTasks:], execution), axis=1)
#        return fb
        
    def solveByFeedback(self, fb_resqtimesww_by_search):
        """ The form of the feedback:
            fb[i]: a sorted array of rescue times with weights for possibly explored tasks by search task i"""
        schedulewa = [[len(self.tasks)+i] for i in range(len(self.agents))]
        unseentask = np.array([True]*len(self.tasks), np.bool)
        agentdelay = np.array([0]*len(self.agents), np.float)
        while np.any(unseentask):
            task = np.argmin([fb_resqtimesww_by_search[i][0][0] if len(fb_resqtimesww_by_search[i])>0 and unseentask[i] else np.inf for i in range(len(fb_resqtimesww_by_search))])
            if not unseentask[task]:
                task = np.where(unseentask)[0][0]
            unseentask[task] = False
            minincrease = np.inf
            minloc = -1
            bestagent = -1
            for i in range(len(self.agents)):
                location, increase = self.findBestInsertion(i, schedulewa[i], task, agentdelay[i], fb_resqtimesww_by_search)
                if increase < minincrease:
                    minincrease = increase
                    minloc = location
                    bestagent = i
            if bestagent >= 0:
                if self.debug:
                    print(task, bestagent, minloc, minincrease)
                schedulewa[bestagent].insert(minloc, task)
                agentdelay[bestagent] += minincrease
#        import os
#        fname = 'feedbackTestData2.py'
#        if os.path.isfile(fname):
#            exit()
#        else:
#            fh = open(fname, 'w', 0)
#            text = 'feedback = %s\ndistanceTable = %s\nallocation = %s\ntasks = %s\n' % (
#                [list(e) for e in fb_resqtimesww_by_search],
#                [[list(e) for e in l] for l in self.distanceTable],
#                schedulewa, [list(e) for e in self.tasks])
#            fh.write(text)
#            fh.close()
#            exit()
        return [s[1:] for s in schedulewa], agentdelay
                
    def findBestInsertion(self, agent, schedule, task, agentdelay, fb_resqtimesww_by_search):
        delays = [self.calculateDelay(agent, schedule, pos, task, fb_resqtimesww_by_search) for pos in range(1, len(schedule)+1)]
        minpos = np.argmin(delays)
        return minpos+1, delays[minpos]-agentdelay
        
    def calculateDelay(self, agent, schedule, pos, task, fb_resqtimesww_by_search):
        sched = list(schedule)
        sched.insert(pos, task)
        t = 0.0
        delay = 0.0
        config = 0
        for i in range(1,len(sched)):
            dist, config = self.getDistance(config, sched[i-1], sched[i])
            t += dist
            if self.use_activaiton and t < self.tasks[sched[i]][-1]:
                t = self.tasks[sched[i]][-1]
            if t > self.agents[agent].batteryTime:
                return np.inf
            task = sched[i]
            delay += self.calculateIndividualDelay(t, fb_resqtimesww_by_search[task])
        return delay
        
    ''' fb_resqtimes must be increasingly sorted! '''
    @staticmethod
    def calculateIndividualDelay(time, fb_resqtimesww):
        delay = time
        for t in fb_resqtimesww:
            if time < t[0]:
                break
            delay += (time - t[0])*t[1]
        return delay
                
    def getGreedyInstructions(self):
        schedule = self.solveGreedily()
        dirs = np.array([[0,0]]*len(self.agents), np.float)
        taskID = np.array([-1]*len(self.agents), np.int)
        for i in (a for a in range(len(self.agents)) if schedule[a]):
            dirs[i] = direction(self.agents[i].loc, self.tasks[schedule[i][0]][:2]) * len(schedule[i])
            if schedule[i][0] < self.nFixedTasks:            
                taskID[i] = schedule[i][0]
        return dirs, taskID, schedule
        
    def getFeedbackSchedule(self, fb_resqtimesww_by_search):
        schedule, delay = self.solveByFeedback(fb_resqtimesww_by_search)
        return schedule
        
    def getTardinessList(self, schedule, fb_resqtimesww_by_search):
        """Computes how much later would rescues start after explorations given in an agent schedule
        
        Parameters
        ----------
        schedule: [taskid,taskid, ...]
            single agent schedule to compute the tardiness for
        fb_resqtimesww_by_search: [[(t, weight), (t, weight)], ...]
            feedback from rescue with weights grouped by search tasks
        
        Returns
        -------        
        tardyCount, nonTartdyDiffLst
        
        tardyCount: int
            the number of tasks already late according to schedule
        nonTartdyDiffList: [[dt, weight], [dt, weight], ...]
            the rest of tardinesses relative to the execution from the schedule with their weigths in the following form
    
    """
        t = 0.0
        config = 0
        tardyCount = 0
        nonTartdyDiffList = []
        for i in range(1,len(schedule)):
            dist, config = self.getDistance(config, schedule[i-1], schedule[i])
            t += dist
            if self.use_activaiton and t < self.tasks[schedule[i]][-1]:
                t = self.tasks[schedule[i]][-1]
            task = schedule[i]
            tardyTasks, tlist = self.getIndividualTardinessList(t, fb_resqtimesww_by_search[task])
            tardyCount += tardyTasks
            nonTartdyDiffList += tlist
        nonTartdyDiffList.sort()
        return tardyCount, nonTartdyDiffList
        
    @staticmethod
    def getIndividualTardinessList(time, fb_resqtimesww):
        tardyTasks = 1
        tlist = []
        for t in fb_resqtimesww:
            if time < t:
                tlist.append((t[0]-time, t[1]))
            else:
                tardyTasks += 1
        return tardyTasks, tlist
        
def testFeedbackSolver():
    from ProblemFactory import Distribution2D, Density
    import feedbackTestData2 as feedbackTestData
    from searchgroup import SearchGroup
    from constants import FW_CRUISE_SPEED
    import scipy.io
    import cProfile
    
    print('reading input')
    
    inputfile = 'pre_haiti20.mat'
    config_input = scipy.io.loadmat(inputfile)
    search_tasks = config_input['search_tasks']
    offset_to_density = config_input['offset'][0]
    scaler = config_input['scaler'][0,0]
    grid_size = config_input['grid_size'][0,0]
    offset_to_task = np.min(search_tasks, 0)
    search_task_coords = (search_tasks-offset_to_task) / grid_size
    density = config_input['density']
    dstr = Distribution2D(Density(density, scaler, offset_to_density))
    dstr.setExpectedVal(config_input['entasks'][0,0])
    density = dstr.getDensity()
    
    print('initialising searchgroup')
    
    cmd = SearchGroup(SearchGroup.SearchMode.NoMan, 8, [[768000,2052400,-0.2], [777500,2051750,np.pi+0.3]], search_task_coords, grid_size, offset_to_task, density,
                    port_start=8880, debug = 0)
    
    print('stopping searchrs')

    cmd.comm_stop()
    time.sleep(1)

    print('Searchrs stopped: %s' % (cmd.check_finished(),))

    atts = [s.attitude for s in cmd.agent_states]
    
    print('initialising search problem')
    
    dist_table = np.asarray(feedbackTestData.distanceTable, dtype='f8, i4')
    prob = SearchProblem(atts, np.asarray(feedbackTestData.tasks), len(feedbackTestData.tasks),
                         FW_CRUISE_SPEED, cmd.distance_fcn, cmd.orientation_set, dist_table)
    fb = feedbackTestData.feedback
    print(np.min([len(f) for f in fb]))
    
    print('running feedback solver')
    
    cProfile.runctx('prob.solveByFeedback(fb)', globals(), locals())
    
    
def testProblem():
    import feedbackTestData
    from constants import FW_STAY_TIME, FW_CRUISE_SPEED
    import time
    #agents = [Attitude([2,1], 0, 100), Attitude([6,3], 0, 100)]
    agents = [Attitude([1147,940], 0, 216000), Attitude([409,404], 0, 216000)]
    #tasks = [[3,3], [3.9,2], [5,1], [7,1], [9,1]]
    tasks = feedbackTestData.tasks
    tasks = np.concatenate((tasks, np.repeat([[FW_STAY_TIME]], len(tasks), 0)), axis=1)
    fb = feedbackTestData.feedback
    p = SearchProblem(agents, tasks, 15, FW_CRUISE_SPEED)
    #p.distanceTable = feedbackTestData.distanceTable
    print(feedbackTestData.allocation)
    before = time.time()
    alloc, delay = p.solveByFeedback(fb)
    after = time.time()
    print(alloc)
    print(delay)
    print(feedbackTestData.elapsedTime)
    print(after-before)
    # print(p.solveGreedily())
    # print(p.getGreedyInstructions())
        
if __name__ == "__main__":
    testFeedbackSolver()