#!/bin/bash 

export NAME=DEC_31_R06_
# export NAME=DEC_20_T28_
export PROG_DIR=/home/zb1f12/dec_dualsar
export SCRIPT=$PROG_DIR/commander_instance.sh
export SCRIPT16=$PROG_DIR/16runnerSleep.sh
export SCRIPT4=$PROG_DIR/4runnerSleep.sh
export BATCHSIZE="0-127"
export BATCH16="0-7"
export BATCH4="0-31"
export TSLEEP=6
# export TESTRUNBID=0


export MODE=0
qsub -l walltime=16:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=1
qsub -l walltime=10:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=2
qsub -l walltime=40:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=3
qsub -l walltime=8:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=4
qsub -l walltime=40:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=5
qsub -l walltime=40:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=6
qsub -l walltime=40:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=7
qsub -l walltime=16:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=8
qsub -l walltime=10:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4
export MODE=9
qsub -l walltime=6:00:00,nodes=1:ppn=16 -t $BATCH4 -N $NAME$MODE -v MODE=$MODE,TSLEEP=$TSLEEP $SCRIPT4


# qsub -l walltime=02:00:00,nodes=1:ppn=1 -N ${NAME}4 -v MODE=4,PBS_ARRAYID=40 -q test $PROG_DIR/commander_instance_test.sh
# qsub -l walltime=02:00:00,nodes=1:ppn=16 -N ${NAME}1 -v MODE=1,TSLEEP=$TSLEEP,PBS_ARRAYID=1 -q test $SCRIPT4

