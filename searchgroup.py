# -*- coding: utf-8 -*-
"""
Created on Mon Oct 05 13:09:33 2015

@author: Zoli
"""
from agentgroup import AgentGroup
from agent import Attitude, CommFactory, angle_rng_pi
from constants import FW_BATTERY_TIME, FW_CRUISE_SPEED, SIM_STEP, FW_HEIGHT, EXIT_EXP_VAL, FW_DROP_RADIUS, EPSILON, RW_SEARCH_SPEED, OBSERVATION_INTERVAL
from ProblemFactory import Density
from Searchr import searchr_process
from fixedsimulator import FixedWing
from manvre import ManoeuvrePlanner, FakeManPlanner
from mctsplanner import MCTSPlanner, densityMultipleObs
from sensor_sim import MultiTargetBeliefs, SensorSim, plot_particles
from sweep import SweepPlanner, NoSweepPlanner
import mcts.mcts
import mcts.tree_policies
import mcts.default_policies
import mcts.backups
import mcts.graph
import threading
from multiprocessing import Process
import numpy as np
import scipy.io
import sys
import time

import matplotlib.pyplot as plt

class SearchGroup(AgentGroup):
    def __init__(self, agent_coords_with_headings, comm, search_mode, manvre_mode, orientation_count, tasks, task_area_size, task_position_offset, density, rescue_states=[], sensor_simulator=None, nSamples=104, port_start=None, debug=False):
        attitudes = []
        for c in agent_coords_with_headings:
            speed = np.array([np.cos(c[-1]), np.sin(c[-1])], np.float)*FW_CRUISE_SPEED
            attitudes.append(Attitude(c[0:2], speed, FW_BATTERY_TIME))
        AgentGroup.__init__(self, attitudes, comm, topics=['update_search_attitudes'])
        for i in range(len(self.agent_states)): 
            self.agent_instructions[i] = [np.inf for _ in range(4)]
        self.log['wpReached'] = []
        self.log['reward'] = []
        self.max_fail_ratio = 0
        if not isinstance(density, Density):
            density = Density(density)
        self.resq_density = density
        self.init_density_map(8)
        self.sensor_sim = sensor_simulator
        self.beliefs = MultiTargetBeliefs()
        self.observation_locs = []
        self.rescue_tasks_in_plan = dict()  #taks_id : [x, y, area]
        self.globalmanplanner = FakeManPlanner(self.beliefs)
        self.distance_lookup = dict()
        self.lookup_count = 0
        if orientation_count % 4 != 0:
            orientation_count = int(orientation_count/4 + 1) * 4
        self.orientation_count = orientation_count
        self.orientation_set = np.arange(orientation_count) * 2*np.pi / orientation_count
        self.orientation_set = [angle_rng_pi(o) for o in self.orientation_set]
        self.sweepPlanner = NoSweepPlanner(self.orientation_set, task_area_size, areaOffset=task_position_offset)
        sweepTaskTime = self.sweepPlanner.getSweepLen()/FW_CRUISE_SPEED
        maxTaskDiff = np.max(tasks, 0) - np.min(tasks, 0)
        self.debug = debug
        if self.debug:
            print('computing distances', maxTaskDiff)
        self.compute_distances(maxTaskDiff)
        if self.debug:
            print('distances computed')
        self.SIM_STEP = SIM_STEP
        next_level_debug = max(0, self.debug-1)
        next_level_debug2 = max(0, self.debug-2)
        assert((search_mode >= 0) and (search_mode < self.SearchMode._size))
        self.search_mode = search_mode
        assert(isinstance(manvre_mode, self.ManvreMode))
        self.manvre_mode = manvre_mode
        self.manvres = []
        self.manvre_states = []
        self.manvre_disabled = np.zeros((len(self.agent_states),), np.bool)
        for i in range(len(self.agent_states)):
            rstates_txt = ''.join([s.tostring() for s in rescue_states])
            atts_txt = ''.join([a.tostring() for a in attitudes])
            args = (i, self.agent_states[i].tostring(), rstates_txt, atts_txt, tasks,
                self.orientation_set, comm.subscribe_addr, comm.response_addr,
                comm.context, sweepTaskTime, self.distance_fcn,
                self.sweepPlanner.cellCoord2World, next_level_debug)
            #print(args)
            proc = threading.Thread(target=searchr_process, args=args)
#            proc = Process(target=searchr_process, args=args)
            proc.start()
            self.agents.append(proc)
            self.platforms.append(FixedWing(self.agent_states[i].attitude, debug=next_level_debug2))
            mstate = self.comm.make_state(self.agent_states[i].attitude)
            self.manvres.append(ManoeuvrePlanner(nSamples, self.beliefs, task_position_offset, task_area_size,
                        mstate, self.comm.subscribe_addr, self.comm.response_addr, self.comm.context, debug=next_level_debug))
            self.manvre_states.append(mstate)
        if self.debug:
            print('starting processes')
        time.sleep(3)
        if self.debug:
            print('init ready')
        self.comm_start()
        
    def comm_stop(self):
        self.log['belief'] = self.beliefs.summary(1)
        AgentGroup.comm_stop(self)
        
    def refresh_agent_positions(self):
        for p in self.platforms:
            p.att.waitTime = p.get_path_length()/FW_CRUISE_SPEED
        for m in self.manvres:
            m.planUpdate()
        AgentGroup.refresh_agent_positions(self)
        
    def get_rescue_tasks(self):
        data = self.beliefs.summary(1)
        if len(data) == 0:
            self.rescue_tasks_in_plan = dict()
            return []
        self.rescue_tasks_in_plan = dict(zip(data.target_id, np.concatenate((data.mean_position, data.area[:, np.newaxis]), axis=1)))
        data = self.beliefs.summary(0)
        data.area /= RW_SEARCH_SPEED
        r_tasks = np.concatenate((data.mean_position, data.area[:, np.newaxis]), axis=1)
        return r_tasks
        
    def get_ongoing_tasks(self):
        '''Retrurns information about tasks where rescues started already
        
        Returns
        =======
        tasks:
            np.ndarray, [[pos_x, pos_y, rescue_time_estimate, target_id],...]'''
        data = self.beliefs.summary(2)
        if len(data) == 0:
            return []
        data.area /= RW_SEARCH_SPEED
        tasks = np.concatenate((data.mean_position, data.area[:, np.newaxis], data.target_id[:, np.newaxis]), axis=1)
        return tasks
        
    def init_density_map(self, resolution_scaler):
        self.resq_density_orig_shape = self.resq_density.d.shape
        scaled_shape = np.ceil(np.array(self.resq_density.d.shape, np.float)/resolution_scaler)
        extended_density_shape = scaled_shape * resolution_scaler
        #extend resq density with zeros
        self.resq_density.d = np.concatenate(
            (self.resq_density.d, np.zeros((self.resq_density_orig_shape[0], extended_density_shape[1]-self.resq_density_orig_shape[1]))), axis=1)
        self.resq_density.d = np.concatenate(
            (self.resq_density.d, np.zeros((extended_density_shape[0]-self.resq_density_orig_shape[0], extended_density_shape[1]))), axis=0)
        SCALED_MAP_DTYPE = [('center',(np.float,2)),
                            ('log_lik',np.float),
                            ('row_indices',(np.int,(resolution_scaler,resolution_scaler))),
                            ('col_indices',(np.int,(resolution_scaler,resolution_scaler)))]
        self.resq_density_scaler = np.recarray(scaled_shape, dtype=SCALED_MAP_DTYPE)
        row_indices, col_indices = np.indices((resolution_scaler, resolution_scaler))
        for x in np.arange(scaled_shape[0]):
            for y in np.arange(scaled_shape[1]):
                self.resq_density_scaler[x,y] = (
                    np.array([x*resolution_scaler+(resolution_scaler-1.)/2,
                              y*resolution_scaler+(resolution_scaler-1.)/2]) \
                        *self.resq_density.scaler + self.resq_density.offset,
                    0., row_indices+resolution_scaler*x, col_indices+resolution_scaler*y)
                
    def add_observations(self, observation_locs):
        self.observation_locs.extend(observation_locs)
        self.resq_density_scaler.log_lik += \
            self.beliefs.signal_model.unobserved_log_likelihood(
                self.resq_density_scaler.center.reshape(-1,2), observation_locs
                ).reshape(self.resq_density_scaler.shape)
        
    def get_discovery_mask(self, gridpoints):
        if self.sensor_sim == None:
            return np.ones_like(gridpoints[:,0])
        locations = np.asarray(self.observation_locs, np.float)
        return self.sensor_sim.signal_model.unobserved_likelihood(gridpoints, locations)
        
    def get_density(self):
        for row in self.resq_density_scaler:
            for d in row:
#                if np.max(self.resq_density.d[d.row_indices, d.col_indices]) > 0.1:
#                    plt.imshow(self.resq_density.d[d.row_indices, d.col_indices])
#                    plt.show()
                self.resq_density.d[d.row_indices, d.col_indices] *= np.exp(d.log_lik)
                d.log_lik = 0.
        return Density(self.resq_density.d[:self.resq_density_orig_shape[0], :self.resq_density_orig_shape[1]],
                       self.resq_density.scaler, self.resq_density.offset)
                       
    def compute_distances(self, size):
        maxdiff = np.max(np.asarray(np.round(size), np.int))
        for dx in np.arange(-maxdiff, maxdiff+1):
            for dy in np.arange(-maxdiff, maxdiff+1):
                if not (dx == 0 and dy == 0):
                    for soi in range(self.orientation_count):
                        paths = np.array([np.inf]*self.orientation_count)
                        for eoi in range(self.orientation_count):
                            startpos = self.sweepPlanner.cellCoord2World([0.,0.])
                            endpos = self.sweepPlanner.cellCoord2World([dx, dy])
                            _, path_length = FixedWing.navigateTo(np.concatenate((startpos, [self.orientation_set[soi]])),
                                                                  np.concatenate((endpos, [self.orientation_set[eoi]])))
                            paths[eoi] = path_length
                        best_index = np.argmin(paths)
                        self.distance_lookup[(dx, dy, soi)] = (paths[best_index], best_index)
                       
    def lookup_distance(self, diff, startOrientationIndex):
        #np.testing.assert_allclose(diff%1., 0)
        intdiff = np.asarray(np.round(diff), np.int)
        keyval = (intdiff[0], intdiff[1], startOrientationIndex)
        return self.distance_lookup[keyval]

    def distance_fcn(self, startpos, endpos, startHeading, isAgent):
        if isAgent:
            endpos = self.sweepPlanner.cellCoord2World(endpos)
            paths = np.array([np.inf]*self.orientation_count)
            for eoi in range(self.orientation_count):
                _, path_length = FixedWing.navigateTo(np.concatenate((startpos, [startHeading])),
                                                      np.concatenate((endpos, [self.orientation_set[eoi]])))
                paths[eoi] = path_length
                best_index = np.argmin(paths)
            return paths[best_index], best_index
        else:
            startHeadingIndex = np.where(self.orientation_set == startHeading)[0][0]
            diff = endpos-startpos
            if np.sum(np.abs(diff)) < 0.0001:
                return 0., startHeadingIndex
            return self.lookup_distance(endpos-startpos, startHeadingIndex)
        
    def run_negotiation(self, send2resq, send2manvre, scherryAlternate=False):
        if self.check_finished():
            return []
        self.refresh_agent_positions()
        self.manvre_disabled = np.zeros((len(self.platforms),), np.bool)
        if self.search_mode == self.SearchMode.Scherry:
            for i in range(len(self.platforms)):
                self.run_agent_scherri_search(i)
        else:
            responses = self.comm.bcast('decide_search', np.array([send2resq, send2manvre], np.int))
            for r in responses:
                agent_id = np.fromstring(r[CommFactory.topic_len:], np.int, 1)
                data = r[CommFactory.topic_len+len(agent_id.tostring()):]
                agent_id = agent_id[0]
                instruction = np.fromstring(data, np.float)
                debugtime = instruction[-1]
                instruction = instruction[:-1]
                if self.debug:
                    print('decision %s%d: %s %s' % (r[:CommFactory.topic_len],
                        agent_id, instruction, time.asctime(time.localtime(debugtime))))
                self.agent_instructions[agent_id] = instruction
                if np.all(np.isinf(instruction)) and scherryAlternate:
                    self.run_agent_scherri_search(agent_id, False)
        return self.agent_instructions
        
    def run_agent_manvre(self, agentId, observed, global_planner=True):
        if len(observed) > 0:
            fw = self.platforms[agentId]
            fwPosHeading = fw.att.getPosHeading()
            targ = fw.get_next_target()
            if targ is None:
                print('Empty TARGET')
                return
            if global_planner:
                manplanner = self.globalmanplanner
            else:
                manplanner = self.manvres[agentId]    
            rootState = ManoeuvrePlanner.DeviateRouteState(
                MCTSPlanner.MCTSState(fwPosHeading, branching=15),
                (manplanner, targ, observed))
            quit_rew = rootState.reward(None, None)
            #DEBUG
            self.rews[agentId] = quit_rew
            if self.search_mode == self.SearchMode.NoSrc:
                return
            #end DEBUG
            #print('Qreward:', quit_rew)
            #print('obs', rootState.allObservations())
            #if quit_rew < EPSILON:
            #    continue
                #quit_rew = 10.0            
            root = mcts.graph.StateNode(None, rootState)
            if self.search_mode == self.SearchMode.FlatTS:
                policy = mcts.tree_policies.flat
            else:
                policy = mcts.tree_policies.UCB1(c=1.41*quit_rew)
            ts = mcts.mcts.MCTS(tree_policy=policy,
                default_policy=mcts.default_policies.immediate_reward,
                backup=mcts.backups.monte_carlo)
            pars = self.manvre_mode.params
            best_action = ts(root, pars[0], pars[1], pars[2])
            manvre = []
            if not best_action.isQuit():
                best_seq = ts.best_sequence(root)
                end_state = rootState
                for a in best_seq:
                    if a.isQuit():
                        break
                    manvre.append(a.posheading)
                    end_state = end_state.perform(a)
                if len(manvre)>0:                        
                    best_rew = end_state.reward(None, None)
                    self.rews[agentId+len(self.platforms)] = best_rew
                    if best_rew < quit_rew:
                        print('search fail...')
                        fail_ratio = (quit_rew-best_rew)/quit_rew
                        print('ratio: {:.2%} (max: {:.2%})'.format(fail_ratio, self.max_fail_ratio))
                        if fail_ratio > self.max_fail_ratio:
                            self.max_fail_ratio = fail_ratio
                        print('q values:')
                        print([ch.q for ch in root.children.values()])
                        try:
                            print('rewards:')
                            print([ch.state.reward for ch in root.children.values()])
                        except:
                            pass
                        print('chosen action', root.children.keys().index(best_action))
                        manvre = []
            fw.set_manvre(manvre)
            
    def run_agent_scherri_search(self, agentId, fromCurrentPos=True):
        self.manvre_disabled[agentId] = True
        fw = self.platforms[agentId]
        if fromCurrentPos:
            fwPosHeading = fw.att.getPosHeading()
        else:
            fwPosHeading = fw.get_last_target()
        density = self.get_density()
        rootState = MCTSPlanner.ScerriState(
            MCTSPlanner.MCTSState(fwPosHeading, branching=20,
                obs_dist=10*OBSERVATION_INTERVAL*FW_CRUISE_SPEED+EPSILON),
            densityMultipleObs(density))
        root = mcts.graph.StateNode(None, rootState)
        if self.search_mode == self.SearchMode.FlatTS:
            policy = mcts.tree_policies.flat
        else:
            policy = mcts.tree_policies.UCB1(c=1.41*density.repr_value())
        ts = mcts.mcts.MCTS(tree_policy=policy,
            default_policy=mcts.default_policies.immediate_reward,
            backup=mcts.backups.monte_carlo)
        ts(root, stoptime=10.0, expansion_size=5)
        best_seq = ts.best_sequence(root)
        manvre = []
        end_state = rootState
        for a in best_seq:
            manvre.append(a.posheading)
            end_state = end_state.perform(a)
        if fromCurrentPos:
            fw.set_manvre(manvre)
        else:
            fw.append_targets(manvre)
        
    def run_manvre(self, observed, use_global_planner=True):
        if self.check_finished():
            return
        #DEBUG rewards record init:
        self.rews = np.array([0.]*(len(self.platforms)*2+1))
        summ = self.beliefs.summary(0)
        for i in range(len(self.platforms)):
            tasks_loc_area = []
            for t in observed[i]:
                items = summ[summ.target_id==t]
                if items:
                    item = items[0]
                    tasks_loc_area.append([item.target_id]+item.mean_position.tolist()+[item.area])
            self.manvres[i].updateBelief(tasks_loc_area)
            if not self.manvre_disabled[i]:
                self.run_agent_manvre(i, observed[i], use_global_planner)
        
        self.rews[-1] = self.time
        self.log['reward'].append(self.rews)
    
    def belief_update(self, debug=False):
        observed = [[] for _ in self.platforms]
        if self.sensor_sim is None or self.check_finished():
            return observed
        agentLoc3D = np.array([[a.att.loc[0], a.att.loc[1], FW_HEIGHT] for a in self.platforms])
        observations = self.sensor_sim.generate_observations(agentLoc3D)
        pos_obs = observations[observations.target_id >= 0]
        self.add_observations(agentLoc3D)
        for o in pos_obs:
            observed[o.uav_id].append(o.target_id)
        if len(observations) > 0:
            self.beliefs.observe(observations)
            if debug:
                for o in pos_obs:
                    plot_particles(self.beliefs, o.target_id, self.sensor_sim.target_locations[o.target_id])
                    plt.show() 
#                    plt.savefig('debug_particles_with_no_obs_%d.png' % (len(self.beliefs.target_beliefs),))
#                    plt.clf()
        return observed
            
        
    def run_simulation(self, next_decision_time, auto_negotiate=False):
        if self.check_finished():
            return []
        for i in range(len(self.platforms)):
            if ((self.platforms[i].get_path_length()-FW_DROP_RADIUS)/FW_CRUISE_SPEED) < next_decision_time-self.time:
                #if needs to allocate a task
                if auto_negotiate:
                    self.run_negotiation()
                    print('.')
                if not np.isinf(self.agent_instructions[i][0]):
                    if np.isinf(self.agent_instructions[i][2]):
                        plan = self.agent_instructions[i][:2]
                    else:
                        plan = self.agent_instructions[i]
                    tile = self.sweepPlanner.nextSweepTile(plan, self.platforms[i].get_last_target())
                    self.platforms[i].append_targets(tile)
                    self.platforms[i].att.currentTask = plan[:2]
                    print(self.platforms[i].att.waitTime)
                    self.platforms[i].att.waitTime = self.platforms[i].get_path_length()/FW_CRUISE_SPEED
                    print(self.platforms[i].att.waitTime,'/')
                    self.remove_task(plan[:2])
                    if self.logging:
                        self.log['taskDone'].append([plan[0], plan[1], self.time])
                    self.agent_instructions[i] = np.concatenate((self.agent_instructions[i][2:], [np.inf, np.inf]))
        while self.time < next_decision_time:
            for p in self.platforms:
                completed_tasks = p.step(self.SIM_STEP)
                if len(completed_tasks) > 0:
                    for t in completed_tasks:
                        if self.logging:
                            self.log['wpReached'].append([t[0], t[1], self.time])
            if self.logging and not self.check_finished():
                newline = np.array([np.concatenate((p.att.loc, [self.time])) for p in self.platforms], np.float)
                if self.debug>1:
                    for c in np.reshape(newline, 3*len(self.platforms)):
                        print("{:4.0f}".format(c)),
                    print()
                self.log['pos'].append(newline)
            self.time += self.SIM_STEP
        return completed_tasks
        
    def remove_task(self, task):
        self.comm.bcast('delete_search_task', task)
        
    def remove_rescue_task(self, task, only_from_plan=True):
        taskID = None
        for taskid in self.rescue_tasks_in_plan:
            if all(task[:2] == self.rescue_tasks_in_plan[taskid][:2]):
                taskID = taskid
        if not taskID is None:
            self.beliefs.remove_target(taskID, ~only_from_plan)
        elif len(self.rescue_tasks_in_plan) > 0:
            pass
#            print('could not remove rescue task')
#            print(task)
#            print('from')
#            print(self.rescue_tasks_in_plan)
            
    def check_finished(self):
        if AgentGroup.check_finished(self):
            if all(p.att.currentTask is None for p in self.platforms):
                return True
            if all([p.att.batteryTime <= 0. for p in self.platforms]):
                return True
        if np.sum(self.resq_density.d) < EXIT_EXP_VAL:
            return True
        return False
                
    def run_observe_manvre(self, use_global_planner=True):
        if self.check_finished():
            return
        if self.search_mode == self.SearchMode.NoMan:
            self.belief_update()
        elif self.search_mode == self.SearchMode.debug:
            self.belief_update(debug=True)
        else:
            self.run_manvre(self.belief_update(), use_global_planner)
                
    def __call__(self, next_decision_time):
        print(self.time)
        self.run_observe_manvre()
        self.run_simulation(next_decision_time, auto_negotiate=True)
                
    class SearchMode:
        _size = 6
        MCTS, FlatTS, NoSrc, NoMan, Scherry, debug = range(_size)
        
        @staticmethod        
        def is_manvre_on(mode):
            assert(mode in range(SearchGroup.SearchMode._size))
            if mode == SearchGroup.SearchMode.NoMan or mode == SearchGroup.SearchMode.Scherry:
                return False
            return True
        
    class ManvreMode:
        def __init__(self, is_timelimited, limit, expansion_size, send_only_first_tardiness=False):
            self.resend_tardiness = ~send_only_first_tardiness
            expansion_size = int(expansion_size)
            if is_timelimited:
                self.params = (1500, limit, expansion_size)
                self.desc = 't%.1f:%d' % (limit, expansion_size)
            else:
                nexpansions = int(limit/expansion_size)
                self.params = (nexpansions, None, expansion_size)
                self.desc = 'n%d×%d' % (nexpansions, expansion_size)
                
        def __str__(self):
            return self.desc
        

def test_distance_lookup(searchr, pos, start_ori, endpos_set):
    plt.cla()
    plt.hold(True)
    for endpoint in endpos_set:
        diff = np.array(np.round(np.asarray(endpoint) - np.asarray(pos)), np.int)
        if np.all(diff == 0):
            continue
        dist, end_ori = searchr.distance_lookup[(diff[0], diff[1], start_ori)]
        endpos = searchr.sweepPlanner.cellCoord2World(endpoint)
        endheading = searchr.orientation_set[end_ori]
        arrow_head = searchr.sweepPlanner.cellCoord2World(np.array([np.cos(endheading), np.sin(endheading)], np.float) * 0.5 + endpoint)
        plt.plot([endpos[0], arrow_head[0]], [endpos[1], arrow_head[1]])
        plt.text(endpos[0], endpos[1], '%d' % (dist,))
    plt.show()
    searchr.comm_stop()
        
if __name__ == "__main__":
    from ProblemFactory import Distribution2D#, Density
    import os
    #import time
    print('Current dir:')
    print(os.getcwd())
    if os.getcwd()[1:3].lower() != ':\\':
        os.chdir('/home/zb1f12/dec_dualsar/')
    
    if len(sys.argv)<2:
        runID = 0
    else:
        runID = int(sys.argv[1])
    if len(sys.argv)<3:
        mode = 0
    else:
        mode = int(sys.argv[2])
    if len(sys.argv)<4:
        inputfile = 'pre_haiti20.mat'
    else:
        inputfile = sys.argv[3]
        
    print('Parameters:\n\tID:%d, mode:%d, file:%s' % (runID, mode, inputfile))
        
    config_input = scipy.io.loadmat(inputfile)
    rescue_tasks = np.fromfile(config_input['outcome_folder'][0] + '/rlocs%d.npa' % (runID,), np.float64).reshape(-1,2)
    search_tasks = config_input['search_tasks']
    offset_to_density = config_input['offset'][0]
    scaler = config_input['scaler'][0,0]
    grid_size = config_input['grid_size'][0,0]
    offset_to_task = np.min(search_tasks, 0)
    search_task_coords = (search_tasks-offset_to_task) / grid_size
    density = config_input['density']
    dstr = Distribution2D(Density(density, scaler, offset_to_density))
    dstr.setExpectedVal(config_input['entasks'][0,0])
    density = dstr.getDensity()
    
    if mode == 0:
        smode = SearchGroup.SearchMode.NoMan
    elif mode == 1:
        smode = SearchGroup.SearchMode.MCTS        
    elif mode == 2:
        smode = SearchGroup.SearchMode.Scherry
    else:
        exit()
        
    print('Starting search init...')
    
    cmd = SearchGroup(smode, 8, [[768000,2052400,-0.2], [777500,2051750,np.pi+0.3]], search_task_coords, grid_size, offset_to_task, density,
#    cmd = SearchGroup(smode, 8, [[777500,2051750,np.pi+0.3]], search_task_coords, grid_size, offset_to_task, density,
#                    sensor_simulator=SensorSim(rescueTasks),
                    port_start=8880+10*runID+mode*1500, debug = 0)              
    print('Starting search simulation...')
    test_distance_lookup(cmd, [10,5], 6, search_task_coords) 
"""    t=1.0
    #for quicker debug
#    for _ in range(100):
    while not cmd.check_finished():
        cmd(t)
        t+=1.0
    cmd.comm_stop()
    cmd.log['searchTasks'] = search_tasks
    logfile = 'simout/poslog_search_%d-%d.mat' % (runID,mode)
    print('Finished. Writing into ' + logfile)
    scipy.io.savemat(logfile, cmd.log)

    print(cmd.beliefs.summary())
    print('sum area', np.sum(cmd.beliefs.summary().area))
    print('max fail', cmd.max_fail_ratio)
#    print('Actual range: %d' % (cmd.sweepPlanner.range,))
    X, Y = np.meshgrid(
        np.linspace(offset[0]*1, np.max(tasks,0)[0]*1, 150, dtype=np.float),
        np.linspace(offset[1]*1, np.max(tasks,0)[1]*1, 150, dtype=np.float))
    X = X.flatten()[:,np.newaxis]
    Y = Y.flatten()[:,np.newaxis]
    grid = np.hstack([X,Y])
    t=time.time()
    Z = cmd.get_discovery_mask(grid)
    dt = time.time()-t
    print('discovery mask calculation took %d s' % (dt,))
    print('observations:', len(cmd.observation_locs))
    print('gridpoints:', len(grid))
    grid_shape = [150,150]
    X.shape = grid_shape
    Y.shape = grid_shape
    Z.shape = grid_shape
    plt.subplot(1,3,1)
    plt.contourf(X,Y,Z)#,alpha=0.5)
    cbar = plt.colorbar()
    plt.grid(True)
    plt.title("Unobserved likelihood")
    plt.subplot(1,3,2)
    density = np.array(cmd.resq_density.d)
    plt.imshow(density)
    cbar = plt.colorbar()
    plt.grid(True)
    plt.subplot(1,3,3)
    plt.imshow(cmd.get_density().d)
    cbar = plt.colorbar()
    plt.grid(True)
    plt.show()"""
    