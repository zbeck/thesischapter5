# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 16:12:57 2015

@author: Zoli
"""
import numpy as np
import bisect
import threading
from constants import EPSILON, FW_CRUISE_SPEED, RW_SEARCH_SPEED, OBSERVATION_INTERVAL
from agent import CommFactory, CommAgent
from fixedsimulator import FixedWing
from mctsplanner import MCTSPlanner

class ManoeuvrePlanner(CommAgent):
    def __init__(self, nSamples, sensingModel, sweepOffset, sweepSize, state, bcast_addr, reply_addr, context=None, debug=False):
        CommAgent.__init__(self, state, bcast_addr, reply_addr, ['stop'], context)
        self.rtasks = dict()
        self.rtasks_in_plan = dict()
        self.nSamples = float(nSamples)
        self.sensingPredictionModel = sensingModel
        self.sweepOffset = np.asarray(sweepOffset)
        self.sweepSize = sweepSize
        self.feedbackLock = threading.RLock()
        self.resetFeedback()
        self.nonTartdyDiffList = np.array([]).reshape(0,2)
        self.criticalCounts = np.array([])
        self.tardyCount = 0
        
    def _sweepArea(self, task):
        center = np.round((task - self.sweepOffset)/self.sweepSize)*self.sweepSize + self.sweepOffset
        hs = self.sweepSize/2
        return (center[0]-hs, center[0]+hs, center[1]-hs, center[1]+hs)
        
    def updateBelief(self, update_rtask_loc_area):
        for t in update_rtask_loc_area:
            self.rtasks[t[0]] = t[1:]
            
    def planUpdate(self):
        self.rtasks_in_plan = dict(self.rtasks)
    
    def resetFeedback(self):
        with self.feedbackLock:
            self.criticalCountList = []
            self.awaitingFeedback = 0
        
    def processmessage(self, conn, data, msg_type):
        if msg_type == CommFactory.msg_id['manvre_tardiness']:
            tcarray = np.fromstring(data, np.int, 1)
            self.tardyCount = tcarray[0] + 1
            data = data[len(tcarray.tostring()):]
            self.nonTartdyDiffList = np.fromstring(data, np.float).reshape(-1, 2)
            addvalue = 0
            for line in self.nonTartdyDiffList:
                line[1] += addvalue
                addvalue = line[1]
        elif msg_type == CommFactory.msg_id['manvre_critical']:
            criticalChunk = np.fromstring(data, np.float)
            criticalChunk = criticalChunk.reshape(len(criticalChunk)/3, 3)
            with self.feedbackLock:
                self.criticalCountList.append(criticalChunk)
                self.awaitingFeedback -= 1
                if self.awaitingFeedback == 0:
                    self.criticalCounts = np.concatenate(self.criticalCountList)
                    self.resetFeedback()
            
    def tardinessFactor(self, t):
        pos = bisect.bisect_right(self.nonTartdyDiffList[:,0], t)
        if pos == 0:
            return self.tardyCount / self.nSamples
        return (self.nonTartdyDiffList[pos-1, 1] + self.tardyCount) / self.nSamples
        
    @staticmethod
    def taskMatching(loc, task):
        return np.all(loc == task)
        
    @staticmethod
    def taskInArea(area, task):
        """returns if a task is within an area quickly
        
        Parameters
        ----------
        area: numpy.ndarray([x_min, x_max, y_min, y_max])
            The area
        task: numpy.ndarray([x, y])
            The task"""
        if task[0] < area[0]:
            return False
        if task[0] > area[1]:
            return False
        if task[1] < area[2]:
            return False
        if task[1] > area[3]:
            return False
        return True
    
    def criticalityFactor(self, taskid):
        if taskid in self.rtasks_in_plan:
            match_fcn = self.taskMatching
            match_arg = self.rtasks_in_plan[taskid][:2]
        elif taskid in self.rtasks:
            match_fcn = self.taskInArea
            match_arg = self._sweepArea(self.rtasks[taskid][:2])
        else:
            return 1.
        factorlist = [line[2] for line in self.criticalCounts if match_fcn(match_arg, line[0:2])]
        if len(factorlist) == 0:
            return 1.
        return sum(factorlist)/len(factorlist)
        
    def improvementFromObservations(self, observations, targets=None):
        reductions = self.sensingPredictionModel.expected_area_reduction(observations, targets)
        improvement = sum([self.criticalityFactor(r[0])*r[1]/RW_SEARCH_SPEED for r in reductions])
        return improvement
        
    class DeviateRouteState:
        def __init__(self, mctsstate, settings):
            self.settings = settings
            self.baseState = mctsstate
            self.baseState.actions += [MCTSPlanner.MoveAction([np.inf, np.inf, 0])]
            self.terminal = False
            self.actions = self.baseState.actions
            if len(self.baseState.posheading) > 0:
                self.initFuture(self.baseState.posheading)
                self.planDelay = 0.0
                self.planTardiness = 0.0
                
        @property
        def manPlanner(self):
            return self.settings[0]
        @property
        def target(self):
            return self.settings[1]
        @property
        def targetIds(self):
            return self.settings[2]
            
        def is_terminal(self):
            return self.terminal
            
        def initFuture(self, pos_heading):
            go_to_target = FixedWing.navigateTo(pos_heading, self.target)
            self.futurePathLength = go_to_target[1]
            internalPoints = [p[:2] for p in go_to_target[0]]
            path_to_target = [pos_heading[0:2]] + internalPoints + [self.target[0:2]]
            self.futureObservations = MCTSPlanner.getObservations(path_to_target, pos_heading[2])
            
        def initTardiness(self, parent):
            incremental_delay = self.futurePathLength - (parent.futurePathLength - self.baseState.obs_dist)
            if incremental_delay < -5.0:
#                from fixedsimulator import follow_points
#                import matplotlib.pyplot as plt
#                parentPath = np.asarray(follow_points([parent.baseState.posheading] + FixedWing.navigateTo(parent.baseState.posheading, self.target)[0] + [self.target]))
#                selfPath = np.asarray(follow_points([self.baseState.posheading] + FixedWing.navigateTo(self.baseState.posheading, self.target)[0] + [self.target]))
#                plt.plot(parentPath[:,0], parentPath[:,1], 'k', selfPath[:,0], selfPath[:,1], 'r')
#                plt.axis('equal')
#                plt.show()
                print('WARNING incremental delay of plan came out to be ', incremental_delay)
            self.planDelay = parent.planDelay + incremental_delay
            if self.planDelay < -EPSILON:
                print('WARNING negative plan delay: ', self.planDelay)
            self.planTardiness = parent.planTardiness + incremental_delay * self.manPlanner.tardinessFactor(parent.planDelay)
            
        def perform(self, action):
            if action.isQuit():
                state = ManoeuvrePlanner.DeviateRouteState(
                    MCTSPlanner.MCTSState([], self.baseState.observations, self.baseState.pathLength, branching=0),
                    self.settings)
                state.futurePathLength = self.futurePathLength
                state.futureObservations = self.futureObservations
                state.planTardiness = self.planTardiness
                state.terminal = True
                return state
            state = ManoeuvrePlanner.DeviateRouteState(
                self.baseState.perform(action), self.settings)
            state.initTardiness(self)
            return state
            
        def reward(self, parent, action):
            observations = self.allObservations()
            rescue_improvement = self.manPlanner.improvementFromObservations(observations, self.targetIds)
            return rescue_improvement - self.planTardiness
            
        def allObservations(self):
            if len(self.futureObservations) == 0:
                return self.baseState.observations[1:]
            return np.concatenate((self.baseState.observations[1:], self.futureObservations))
            
        def __eq__(self, other):
            return self.baseState == other.baseState
            
        def __hash__(self):
            return hash(self.baseState)
        
            
      
    
def test_actions():
    import scipy.io
    posheading = [0,0,0]
    actions = []    
    currentState = ManoeuvrePlanner.MCTSState(posheading)
    for i in range(10):
        actions += currentState.actions
        chosen = np.random.choice(currentState.actions)
        currentState = currentState.perform(chosen)
    scipy.io.savemat('matlab/actionTest.mat', dict(ph=[a.posheading for a in actions]))
    
def getObservationsTest():
    import scipy.io
    from fixedsimulator import follow_points
    start = [0,0,-np.pi*5/6]
    end = [1, 0, np.pi/2]
    go_to_target = ManoeuvrePlanner.navigateTo(start, end)
    path_to_target = [start[0:2]] + go_to_target[0][:2] + [end[0:2]]
    observations = ManoeuvrePlanner.getObservations(np.array(path_to_target), start[2])
    pos = follow_points(path_to_target, start[2])
    scipy.io.savemat('matlab/getObservationsTest.mat', dict(points=path_to_target, obs=observations, pos=pos))
        
class FakeManPlanner:
    def __init__(self, beliefs):
        self.sensingmodel = beliefs
    def improvementFromObservations(self, observations, targets=None):
        if len(observations) == 0:
            return 0.
        reductions = self.sensingmodel.expected_area_reduction(observations, targets)
#        print('obs', observations)
#        print('red', reductions)
        improvement = sum([(10)*r[1]/RW_SEARCH_SPEED for r in reductions])
        return improvement
    def tardinessFactor(self, t):
        return 50+t/3
    
def test_manvre():
    from sensor_sim import SensorSim, MultiTargetBeliefs
    from sweep import SweepPlanner
    from agent import Attitude
    from fixedsimulator import follow_points
#    import matplotlib.pyplot as plt
    import mcts.mcts
    import mcts.tree_policies
    import mcts.default_policies
    import mcts.backups
    import mcts.graph
    import scipy.io
    
    
    MANVRE_STEP = 1.0
    FW_HEIGHT = 100.
    targets = np.array([[500, 500],[1500, 800],[2000, 1000]], np.float)
    sensor_simulator = SensorSim(targets)
    beliefs = MultiTargetBeliefs()
    manPlanner = FakeManPlanner(beliefs)
    sweeper = SweepPlanner(1300, 220, 3, False)
    spath = sweeper.planSweep(np.array([800, 800],np.float), [[0,0],[1,0]], [0,0,0])
    waypoints = spath.pathpoints
    fw = FixedWing(Attitude(waypoints[0][:2], [FW_CRUISE_SPEED*np.cos(waypoints[0][2]), FW_CRUISE_SPEED*np.sin(waypoints[0][2])], 10000), waypoints)
    fw.TARGET_JUMP = -1
    t=0.
    ts = mcts.mcts.MCTS(tree_policy=mcts.tree_policies.UCB1(c=1.41),
                        default_policy=mcts.default_policies.immediate_reward,
                        backup=mcts.backups.monte_carlo)
                        
    poslog = [fw.att.loc]
    manvlog = []
    futurePathLog = []
    while True:
        next_manvre_time = t+MANVRE_STEP
        found_something = False
        while t<next_manvre_time:
            next_obs_time = t+OBSERVATION_INTERVAL
            while t<next_obs_time:
                t+=0.1
                fw.step(0.1)
                poslog.append(fw.att.loc)
            observation = sensor_simulator.generate_observations(np.concatenate((fw.att.loc,[FW_HEIGHT]))[np.newaxis,:])
            if len(observation) > 0:
                found_something = True
                #print('observation', observation)
                beliefs.observe(observation)
            
        if fw.is_targets_finished():
            break
        
        if found_something:
            fwPosHeading = fw.att.getPosHeading()
            rootState = ManoeuvrePlanner.DeviateRouteState(MCTSPlanner.MCTSState(fwPosHeading), (manPlanner, fw.get_next_target(), None))
            quit_rew = rootState.perform(MCTSPlanner.MoveAction([np.inf, np.inf, 0])).reward(None, None)
            root = mcts.graph.StateNode(None, rootState)
            ts.tree_policy = mcts.tree_policies.UCB1(c=1.41*quit_rew)
#            ts.tree_policy = mcts.tree_policies.flat
            best_action = ts(root, stoptime=MANVRE_STEP)
            manvre = []
            if not best_action.isQuit():
                best_seq = ts.best_sequence(root)
                end_state = rootState
                for a in best_seq:
                    if a.isQuit():
                        break
                    manvre.append(a.posheading)
                    end_state = end_state.perform(a)
            manvlog.append([fwPosHeading] + manvre)
            fw.set_manvre(manvre)
            futurePath = np.asarray(follow_points([fwPosHeading] + fw.manvre_list + [fw.get_next_target()]))
            futurePathLog.append(futurePath)
    scipy.io.savemat('matlab/manoeuvreTest.mat', dict(manvres=manvlog, pos=poslog, targets=targets, sweepwp = waypoints, futurepath = futurePathLog))

if __name__ == "__main__":
    test_manvre()