# -*- coding: utf-8 -*-
"""
Created on Mon Oct 05 13:14:12 2015

@author: Zoli
"""

from agent import CommFactory

class AgentGroup:
    def __init__(self, agent_attitudes, comm, topics=['update_resq_attitudes']):
        assert(isinstance(comm, CommFactory))
        self.topics = topics
        self.comm = comm
        self.logging = True
        self.log = {'pos':[], 'taskDone':[]}
        self.time = 0.0
        self.agent_states = []
        self.agents = []
        self.platforms = []
        self.agent_instructions = []
        for a in agent_attitudes:
            self.agent_states.append(self.comm.make_state(a))
            self.agent_instructions.append(a.getDefaultInstruction())
        
    def comm_start(self):
        for state in self.agent_states:
            state.connectSocket()
        
    def check_finished(self):
        agents_closed = not any([a.is_alive() for a in self.agents])
        return agents_closed
            
    def refresh_agent_positions(self):
        attitudes = [p.att for p in self.platforms]
        self.comm.bcast(self.topics[0], attitudes)
        
    def comm_stop(self):
        self.comm.bcast('stop')