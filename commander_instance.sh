#!/bin/bash
module load python
module load numpy/1.9.1
export PROG_DIR=/home/zb1f12/dec_dualsar

export CMD="python $PROG_DIR/commander.py $PBS_ARRAYID $MODE"
echo "Running $CMD"
$CMD
