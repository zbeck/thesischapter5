"""Simulates Mobile phone sensor observations and posterior distributions.

   Classes:
      SensorSim: Simulates sensor observations given UAV and target locations.

      SingleTargetBeliefs: Maintains posterior over single target position
        given observations.

      AllTargetBeliefs: Maintains posterior for all target positions given
        observations.

"""
import math
import scipy.stats
import numpy as np
import numpy.random
import time
import matplotlib.pyplot as plt

OBSERVATION_DTYPE = [('uav_id',int), ('target_id',int),
                  ('uav_position',(float,3)), ('rssi',float)]

SUMMARY_DTYPE = [('target_id',int), ('mean_position',(float,2)),
                 ('area',float)]

REDUCTION_DTYPE = [('target_id',int), ('area_reduction',float)]

DEFAULT_SIGNAL_STDEV = 5.0
DEFAULT_SIGNAL_LOSS_EXPONENT = 40.0
DEFAULT_MIN_SIGNAL = -113.0
DEFAULT_MAX_DISTANCE = 300
DEFAULT_N_SAMPLES = 30
DEFAULT_N_PARTICLES = 1000
AREA_OVERESTIMATION_FACTOR = 1.0 #6.0

def calculate_distance(x, y):
    """Calculate Euclidean distance between on set of points and another.

       Applies formula:

       distance = sqrt( \sum_k (x_k - y_k)^2 )

       Args:
           x: Mxd matrix of positions
           y: Nxc matrix of positions to compare to pos1

           if d < c, x is padded with zeros for missing dimensions

       Returns:
           MxN matrix containing distance between corresponding
           points in input.
    """
    # deal with single example case
    if len(x.shape) < 2:
        x = x[np.newaxis,:]

    if len(y.shape) < 2:
        y = y[np.newaxis,:]

    # pad missing dimensions with zeros if necessary
    if x.shape[1] < y.shape[1]:
        pad_length = y.shape[1] - x.shape[1]
        x = np.pad(x,((0,0),(0,pad_length)),'constant')

    if y.shape[1] < x.shape[1]:
        pad_length = x.shape[1] - y.shape[1]
        y = np.pad(y,((0,0),(0,pad_length)),'constant')

    # align x and y to separate axis in preparation for broadcasting
    x = x[:, np.newaxis, :]
    y = y[np.newaxis, :, :]

    # calculate and return result
    distance = np.sum( (x-y)**2, axis=2 ) ** 0.5
    return distance

def particle_mean(particles):
    """Calculate mean location for specified particles
    
        Args:
            particles: (Nx3) numpy array such that
                particles[:,0] are the x positions
                particles[:,1] are the y positions
                particles[:,2] are the particle weights
                
        Returns: mean position as 1x2 numpy array 
    """
    # sanity check particle weights sum to 1
    total_prob = particles[:,2].sum()
    np.testing.assert_almost_equal(total_prob, 1.0)

    weighted_locations = particles[:,0:2] * (particles[:,2])[:,np.newaxis]
    return weighted_locations.sum(0)[np.newaxis,:]

def particle_variance(particles):
    """Calculate expected squared particle distance from the mean location
    
        Args:
            particles: (Nx3) numpy array such that
                particles[:,0] are the x positions
                particles[:,1] are the y positions
                particles[:,2] are the particle weights
                
        Returns: variance as scalar
    """
    mu = particle_mean(particles)
    sqdist = (particles[:,0:2] - mu)**2
    distance = particles[:,2] * sqdist.sum(1)
    return distance.sum()

def doughnut_sample(uav_location, distance, stdev, n_particles):
    """Samples target locations given distance from uav_location.

        Args:
            uav_location: single 1x3 vector specifying uav location.
            distance: mean distance of target from uav
            stdev: noise standard deviation to add to location
            n_samples: number of particles to generate

        Returns n_samples x 3 matrix of particles. weights are initially
        zero.
    """
    
    # sample noisy distances
    sample_distances = np.random.normal(loc=distance, scale=stdev,
        size=n_particles)

    # assume that any sampled distances shorter that uav altitude
    # lie directly beneath uav
    uav_height = uav_location[2]
    sample_distances[sample_distances < uav_height] = uav_height

    # assuming points lie on ground, find the distance along the ground
    # between the sampled points and uav position
    ground_distance = (sample_distances**2 - uav_height**2)**0.5

    # sample azimuth angle to sampled positions
    azimuth = np.random.rand(n_particles) * 2 * math.pi

    # convert from polar coordinates to get sampled positions
    locations = np.zeros([n_particles,2])
    locations[:,0] = ground_distance * np.cos(azimuth) 
    locations[:,1] = ground_distance * np.sin(azimuth)
    locations += uav_location[np.newaxis,0:2]

    # return sampled locations and their corresponding proposal probabilities
    log_pdf = np.zeros(n_particles) # TODO calculate these
    return (locations, log_pdf)


class Grid:
    """Representation of a 2D Grid."""
    
    def __init__(self, top_left=(-500.0,-500.0), bottom_right=(500.0,500.0),
                 shape=(10,100)):
        """Create a new Grid.

           Args:
               top_left [tuple (x,y)]: position of top left cell
               bottom_right [tuple (x,y)]: position of bottom right cell
               shape [tuple (x,y)]: width and height of grid in cells
        """
        
        x_positions = np.linspace(top_left[0], bottom_right[0], shape[0])
        y_positions = np.linspace(top_left[1], bottom_right[1], shape[1])
        
        self.top_left = np.array(top_left)
        self.bottom_right = np.array(bottom_right)
        self.shape = shape
        self.y, self.x = np.meshgrid(y_positions, x_positions)
        
    @property
    def size(self):
        return self.x.size

    def sample_particles(self, n_points):
        """Sample particles uniformly on grid"""
        length = (self.bottom_right - self.top_left)[np.newaxis,:]
        top_left = self.top_left[np.newaxis,:]
        particles = np.zeros([n_points, 3])
        particles[:,0:2] = top_left + np.random.rand(n_points, 2) * length
        return particles
    

DEFAULT_GRID = Grid()

class SignalModel:
    """Mobile Phone Signal Propagation Model.
    """

    def __init__(self, signal_stdev=DEFAULT_SIGNAL_STDEV,
            min_signal=DEFAULT_MIN_SIGNAL,
            gamma=DEFAULT_SIGNAL_LOSS_EXPONENT,
            max_distance=DEFAULT_MAX_DISTANCE):
        """Creates a new instance of the Signal Propogation Model.

           All signals measured in dBm.

           Args:
               signal_stdev: standard deviation of signal noise.
               min_signal: threshold at which signal becomes undetectable.
               gamma: path loss exponent (dBm)
               max_distance: measurements over this distance are ignored
        """
        self.signal_stdev = signal_stdev
        self.min_signal = min_signal
        self.gamma = gamma
        self.max_distance = max_distance
        # too far at (0.1% tail of distribution)
        self.far_distance = self.median_distance(self.min_signal-3*self.signal_stdev)

    def median_signal(self, distance):
        """Median signal strength expected at given distance.

            if distance is a numpy array, return value will have same shape.

            Only valid for distances > 0.
        """
        assert( np.all(0 < distance) )

        # Calculate median signal according to model.
        distance_ratio = distance / self.max_distance
        signal = self.min_signal - self.gamma * np.log10(distance_ratio)
        return signal

    def median_distance(self, signal):
        """Median distance expected at given signal strength"""
        distance_ratio_dBm = (self.min_signal - signal) / self.gamma
        distance_ratio = 10**distance_ratio_dBm
        distance = distance_ratio * self.max_distance
        return distance

    def distance_stdev(self, signal):
        """Estimate of distance standard deviation for given signal strength

           Just propagates signal standard deviation through distance function
        """
        median = self.median_distance(signal)
        deviation = self.median_distance(signal-self.signal_stdev)
        return deviation-median

    def log_likelihood(self, signal, distance, ignore_large_distances = False):
        """Log likelihood of receiving given signal at given distance from
           target.
           
           For thresholded signals (signals that fall below the minimum signal
           strength for detection, the likelihood is actually cdf(min_signal),
           i.e. the probability of the signal being less than min_signal.

           if ignore_large_distances is set:
               For distances over max_distance, returns zero (no observation)
        """
        # wrap scalars for consistent interface
        if np.isscalar(signal):
            signal = np.array([signal])

        if np.isscalar(distance):
            distance = np.array([distance])

        # calculate the median signal strength at this distance
        median = self.median_signal(distance)
        
        # added by Zoli 05/01/2015:
        # selector for distances over max_distance
        if ignore_large_distances:
            is_close = distance < self.far_distance

        # return the likelihood of the observed signal value
        detected_signal = signal
        detected_median = median
        if ignore_large_distances:
            result = np.zeros_like(detected_signal)
            result[is_close] = scipy.stats.norm.logpdf( \
                detected_signal[is_close], detected_median[is_close], self.signal_stdev)
        else:
            result = scipy.stats.norm.logpdf( \
                detected_signal, detected_median, self.signal_stdev)
        return result

    def unobserved_log_likelihood(self, target_locations, uav_locations):
        """Log likelihood for UNOBSERVED target locations.

           Args:
               target_locations Nx2 matrix of target locations on ground
               uav_locations Nx3 matrix of uav locations

           Returns:
               length-N vector s.t. the kth element is the log likelihood
               that a target exists at position target_locations[k,:], given
               that it has NOT been observed from any uav_locations[j,:]
        """
        # Calculate distance between target and uav location
        distance = calculate_distance(target_locations, uav_locations)
        
        # Make masked array for too high distances
        distance = np.ma.masked_greater(distance, self.far_distance)

        # return if no valid data, so np.all(distance>0) assert doesn't fail
        if np.sum(~distance.mask) == 0:
            return np.zeros(len(target_locations), np.float)
            
        # calculate the median signal strength at each distance
        median = self.median_signal(distance.compressed())

        # calculate loglikelihood of NO observation at each distance
        log_lik_values = scipy.stats.norm.logcdf(self.min_signal,
                median, self.signal_stdev)
                
        # convert back to matrix
        log_lik = np.zeros_like(distance)
        if isinstance(distance.mask, np.ndarray):
            log_lik[~distance.mask] = log_lik_values
        else:
            log_lik.flat = log_lik_values

        # return total log likelihood for each target location
        total_log_lik = log_lik.sum(axis=1)
        return total_log_lik.filled(0)

    def unobserved_likelihood(self, target_locations, uav_locations):
        """Likelihood for UNOBSERVED target locations.

           Args:
               target_locations Nx2 matrix of target locations on ground
               uav_locations Nx3 matrix of uav locations

           Returns:
               length-N vector s.t. the kth element is the likelihood
               that a target exists at position target_locations[k,:], given
               that it has NOT been observed from any uav_locations[j,:]
        """
        #split the large target*uav matrix and compute in small blocks
        
        # 2 GB max array
        max_array_size = 2**((21-3)/2)
        log_lik = np.zeros(len(target_locations))
        for t_block in range(len(target_locations)/max_array_size + 1):
            tloc = target_locations[(t_block*max_array_size): \
                ((t_block+1)*max_array_size)] 
            for u_block in range(len(uav_locations)/max_array_size + 1):
                uloc = uav_locations[(u_block*max_array_size): \
                    ((u_block+1)*max_array_size)]
                log_lik[(t_block*max_array_size): \
                    ((t_block+1)*max_array_size)] += \
                    self.unobserved_log_likelihood(tloc, uloc)
        return np.exp(log_lik)

    def random(self, distance):
        """Generates signal distributed according to the model for a given
           distance.

           Args:
               distance: distance in metres from source to destination at
               which signal is generated.
        """

        # wrap scalars for consistent interface
        if np.isscalar(distance):
            distance = np.array([distance])

        # get median signal for this distance
        median = self.median_signal(distance)

        # Generate normal zero-mean initially with unit variance.
        noise = numpy.random.randn(*distance.shape)

        # scale and translate the noise to the final distribution
        return noise * self.signal_stdev + median


class SensorSim:
    """Simulates sensor observations given UAV and target locations.

       Sensor observations are modelled as RSSI measurements (in decibels)
       from a given UAV location to a given target location.

       Targets are assumes to exist in a 2-D ground plane (z-axis = 0).
    """

    def __init__(self, target_locations, location_stdev=1.0,
            signal_model=SignalModel(), random_seed=None):
        """Initialises a new simulator.

           Arguments:
               target_locations (Nx2 numpy float array):
                   Each row gives the <x,y> ground truth position of a target
                   on the ground in metres.

               location_stdev (float):
                   standard deviation for Gaussian noise on sensed uav
                   locations.

               signal_model: Object providing log_likelihood function for 
                   signal given distance. Default is the SignalModel with
                   default parameters.
        """
        assert(target_locations.shape[1] == 2)
        np.random.seed(random_seed)
        self.signal_model = signal_model
        self.location_stdev = location_stdev
        self.target_locations = target_locations

    def generate_observations(self, uav_locations, uav_ids=None):
        """Called on each timestep to generate sensor observations.

           This is called at each timestep during simulation to generate
           sensor observations.

           Arguments
           ---------
               uav_locations (Nx3 numpy float array):
                   Each row gives the current <x,y,z> ground truth position
                   of a UAV.
               uav_ids (optional):
                   vector of length N, specifying the uav ids for each
                   corresponding location row in uav_locations. This may
                   contain duplicates, if mulitple observations are made by
                   same uav at different locations.

                   If uav_ids is not provided, [0,N] is assumed.

           Returns
           -------
              Nx5 record array where each row has the following fields:
               
              uav_id (int):
                  unique identifier for observing uav.              
              target_id (int):
                  unique identifier for target assigned by simulator.
              uav_position (3x float):
                  uav position (with Gaussian noise)
                  from which the measurement was observed.
              rssi (float):
                  observed single strength generated from
                  observation model with Gaussian noise.

           Note that to parse observation arrays using fromstring,
           you need to specify the record dtype. For example:

           import numpy as np
           import sensor_sim
           uav_locations = ...
           sim = sensor_sim.SensorSim(...)
           obs = sim.spin(uav_locations)
           msg = obs.tostring()
           parsed_obs = np.array.fromstring(msg,dtype=sensor_sim.OBSERVATION_DTYPE)
        """
        # validate input
        assert(uav_locations.shape[1] == 3)

        # validate uav ids, using default if non provided
        n_locations = uav_locations.shape[0]
        if uav_ids is None:
            uav_ids = np.arange(n_locations)
        assert(uav_ids.size == n_locations)

        # generate noisy estimates of actual uav locations
        shape = uav_locations.shape
        location_noise = np.random.randn(*shape) * self.location_stdev
        estimated_uav_locations = uav_locations + location_noise

        # calculate actual distance between each target and each uav
        distance = calculate_distance(self.target_locations, uav_locations)

        # generate observed signals
        signals = self.signal_model.random(distance)

        # generate uav and target indices for each observation
        (target_index, uav_index) = \
            np.mgrid[0:signals.shape[0],0:signals.shape[1]]
        target_index.shape = (target_index.size,)
        uav_index.shape = (uav_index.size,)

        # get the location of the observing uav for each observation
        detecting_uav_locations = estimated_uav_locations[uav_index,:]

        # remove observations for signals to weak to detect
        in_range = signals.flatten() >= self.signal_model.min_signal
        
        # return detected target ids, signals and estimated uav locations
        # as numpy record array
        # also add negative observations
        n_obs = np.sum(in_range) + n_locations
        observations = np.recarray((n_obs,), dtype=OBSERVATION_DTYPE)
        observations.uav_id = np.concatenate((uav_ids[uav_index.flatten()[in_range]], uav_ids))
        observations.target_id = np.concatenate((target_index.flatten()[in_range], [-1]*n_locations))
        observations.rssi = np.concatenate((signals.flatten()[in_range], [self.signal_model.min_signal-1]*n_locations))
        observations.uav_position = np.concatenate((detecting_uav_locations[in_range], estimated_uav_locations))
        return observations

class SingleTargetBeliefs:
    """Maintains beliefs over single target location using sequential
       importance resampling.
    """
    def __init__(self, target_id, pos_obs=None, neg_obs=[], location_stdev=1.0,
                 signal_model=SignalModel(), grid=DEFAULT_GRID,
                 n_samples=DEFAULT_N_SAMPLES,
                 n_particles=DEFAULT_N_PARTICLES):
        """Creates a new set of beliefs
        
           Args:
               target_id: the id of this target
               pos_obs: OBSERVATION_DTYPE record array, initial positive observations
               neg_obs: [[x,y,z], ...] negative observations before the first positive observation
               location_stdev: noise standard deviation for uav locations
               signal_model: signal model provided data likelihood
               grid: Grid object defining support for pdf
               n_samples: number of samples used to estimate expected reduction
                   in area.
               n_particles: number of particles used for importance sampling
        """
        #check valid id (not based on negative observation)
        assert(target_id >= 0)
        self.target_id = target_id
        self.location_stdev = location_stdev
        self.signal_model = signal_model
        self.grid = grid
        self.n_samples = n_samples
        self.n_particles = n_particles
        self.removed = False
        self.observations = pos_obs
        neg_obs = np.asarray(neg_obs)
        if pos_obs is None or len(pos_obs) == 0 or len(neg_obs) == 0:
            self.negative_obs_locations = neg_obs
        else:
            best_ob = np.argmax(pos_obs.rssi)
            dist = calculate_distance(neg_obs, pos_obs.uav_position[[best_ob]])
            self.negative_obs_locations = neg_obs[dist.flatten() < self.signal_model.far_distance*2]
        self.particles = None
        self._init_particles()

    def log_likelihood(self, loc):
        # if there are no observations, all particles just receive
        # equal weight
        n_loc = loc.shape[0]
        if self.observations is None:
            return -np.log(float(self.n_particles))
        
        # calculate likelihood of each observation for each particle
        distance = calculate_distance(loc, self.observations.uav_position)
        signal = self.observations.rssi[np.newaxis,:]
        signal = np.tile(signal,[n_loc,1])

        log_lik = self.signal_model.log_likelihood(signal, distance)
        return log_lik.sum(axis=1)


    def _update_weights(self, log_proposal_prob):
        """Update particle weights after resampling their positions

           Args:
               log_proposal_prob: proposal probabilities for each particle
                   log_proposal_prob[k] is the log probability of particle[k]'s
                   location being drawn from its respective proposal
                   distribution.
        """
        # if there are no observations, all particles just receive
        # equal weight
        if self.observations is None:
            self.particles[:,2] = 1 / self.n_particles
            return
        
        # calculate likelihood of each observation for each particle
        distance = calculate_distance(self.particles[:,0:2],
            self.observations.uav_position)

        signal = self.observations.rssi[np.newaxis,:]
        signal = np.tile(signal,[self.n_particles,1])

        pos_log_lik = self.signal_model.log_likelihood(signal, distance)
        neg_log_lik = self.signal_model.unobserved_log_likelihood(self.particles[:,0:2], self.negative_obs_locations)
        # calculate the total log weight for each particle
        # this is the typical Importance Sampling weighting regime:
        # we divide by (subtract the log of) the proposal probabilties
        # to calibrate for difference between the proposal and the target
        # distributions
        total_log_lik = pos_log_lik.sum(axis=1) + neg_log_lik

        total_log_weight = total_log_lik - log_proposal_prob

        # adjust by constant to prevent overflow
        # (does not affect end result)
        total_log_weight -= total_log_weight.max() 
        
        # calculate final weights from the logs
        self.particles[:,2] = np.exp(total_log_weight)

        # calculate sum of weights for normalisation
        weight_sum = self.particles[:,2].sum()

        # sanity check for overflow 
        if not (0 < weight_sum):
            print "Total log likelihood:", total_log_weight.sum()
            print "max log likelihood: ", total_log_weight.max()
        assert(0 < weight_sum)
        
        # normalise the weights
        self.particles[:,2] /= weight_sum

        # sanity check particle weights sum to 1
        total_prob = self.particles[:,2].sum()
        np.testing.assert_almost_equal(total_prob, 1.0)

    def _resample_particles(self):
        """Resamples particle locations, but does not update weights"""

        assert(self.particles is not None)
        
        # decide how much noise to add to resampled particles
        # TODO consider replacing with adaptive mechanism
        mean_signal = self.observations.rssi.max()
        distance_sigma = self.signal_model.distance_stdev(mean_signal)
        particle_sigma  = particle_variance(self.particles)**0.5
        noise_stddev = min(distance_sigma, particle_sigma) * 4.0

        # choose parent particles according to their probability
        n_particles = self.particles.shape[0]
        parents = np.random.choice(n_particles, size=self.n_particles, 
                p = self.particles[:,2])

        self.particles = self.particles[parents,:]
        
        # generate isotropic gaussian noise to add to particles
        noise = np.random.randn(self.n_particles,2) * noise_stddev

        # calculate the log proposal probability for each particle
        # this is just the noise pdf summed over each location dimension
        noise_logpdf = scipy.stats.norm.logpdf(noise, 0.0, noise_stddev)
        log_proposal_prob = noise_logpdf.sum(axis=1)

        # use generated noise to update particle locations and weights
        self.particles[:,0:2] += noise
        self._update_weights(log_proposal_prob)
        
    def _init_particles(self):
        
        # if there are no positive observations, sample particles uniformly
        # over search area
        no_sighting = self.observations is None or \
                        len(self.observations) == 0
            
        if no_sighting:
            self.particles = self.grid.sample_particles(self.n_particles)
            # uniform proposal in this case, so proposal pdf is constant,
            # and ultimately has no effect on weights. Therefore, we
            # can safely set it to zero.
            log_proposal_pdf  = 0.0;
            self._update_weights(log_proposal_pdf)
            return

        # otherwise seed particles using doughnut around uav position
        # when strongest signal was observed
        self.particles = np.zeros((self.n_particles,3))
        best_ob = np.argmax(self.observations.rssi)
        strongest_signal = self.observations.rssi[best_ob]
        uav_location = self.observations.uav_position[best_ob]
        noise_sigma = self.signal_model.signal_stdev
        median_distance = self.signal_model.median_distance(strongest_signal)
        (locations, location_logpdf) = doughnut_sample(uav_location,
                median_distance, noise_sigma, self.n_particles)
        self.particles[:,0:2] = locations

        # resample once to get sensible weights and positions
        self._update_weights(location_logpdf)
        self._resample_particles()
        
    def expected_location(self):
        return particle_mean(self.particles)

    def expected_area(self, particles=None):

        # if no alternative particles are specified, use the current ones
        if particles is None:
            particles = self.particles
            
        # return the calculated area
        return math.pi * particle_variance(particles) * AREA_OVERESTIMATION_FACTOR
    
    def observed(self):
        """Returns true iff at least one in range non-thresholded observation
           has been made of this target previously.
        """
        if self.observations is None:
            return False

        detected = self.observations.rssi >= self.signal_model.min_signal
        return np.any(detected)
    
    def observe(self, observations):
        
        # ensure all observations are of this target
        assert( np.all(observations.target_id==self.target_id) )

        # before observing, check if we've been observed before
        previously_observed = self.observed()

        # concatenate to previous observations
        if self.observations is None:
            self.observations = observations
        else:
            # ugly but stacking record arrays breaks them
            # would nearly put me of using them in future :-(
            new_size = self.observations.size + observations.size
            new_obs = np.recarray((new_size,), dtype=OBSERVATION_DTYPE)
            new_obs.uav_id = np.hstack((self.observations.uav_id,
                                          observations.uav_id))
            new_obs.target_id = np.hstack((self.observations.target_id,
                                          observations.target_id))
            new_obs.uav_position = np.vstack((self.observations.uav_position,
                                             observations.uav_position))
            new_obs.rssi = np.hstack((self.observations.rssi,
                                     observations.rssi))
            self.observations = new_obs

        # if new observations contains our first positive sighting, reseed
        # particles using doughnut around best sighting
        if not previously_observed and self.observed():
            self._init_particles()

        # resample particles
        self._resample_particles()

    def _sample_signals(self, uav_locations):
        """Utility function for sampling observed signals from posterior."""
        
        # draw actual target location from posterior
        n_particles = self.particles.shape[0]
        target_index = np.random.choice(n_particles, size=self.n_samples,
                p = self.particles[:,2])

        target_locations = self.particles[target_index,0:2]

        # calculate distances
        distances = calculate_distance(target_locations, uav_locations)

        # draw and return signals
        return self.signal_model.random(distances)
    
    def expected_area_reduction(self, uav_locations):
        #because np.tile breaks with 0 locations
        if len(uav_locations) == 0:
            return 0.
        
        # sample observed signals
        sampled_signals = self._sample_signals(uav_locations)

        # calculate distance between uavs and each particle
        distances = calculate_distance(self.particles[:,0:2], uav_locations)

        # align corresponding distance and signal axis for broadcasting
        sampled_signals = sampled_signals[np.newaxis,:,:]
        distances = distances[:,np.newaxis,:]

        n_particles = self.particles.shape[0]
        sampled_signals = np.tile(sampled_signals,[n_particles,1,1])
        distances = np.tile(distances,[1, self.n_samples, 1])

        # calculate total likelihood for each particle for each hypothesised
        # set of signal observations
        log_lik = self.signal_model.log_likelihood(sampled_signals, distances, ignore_large_distances=True)
        total_log_lik = log_lik.sum(axis=2)
        total_log_lik += np.log(self.particles[:,2])[:,np.newaxis] 
        total_lik = np.exp(total_log_lik)

        # calculate hypothesised particles with weights
        sampled_weights = total_lik / total_lik.sum(axis=0)
        np.testing.assert_almost_equal(sampled_weights.sum(axis=0), 1.0)
        particles_with_weights = \
            np.hstack([self.particles[:,0:2], sampled_weights])
        
        # calculate sampled reductions in area
        expected_area = self.expected_area()
        expected_reduction = 0.0
        for k in range(self.n_samples):
            sampled_area = self.expected_area( \
                particles_with_weights[:,[0,1,2+k]])
            sampled_reduction = np.max([expected_area - sampled_area, 0.0])
            expected_reduction += sampled_reduction / self.n_samples

        # return result
        return expected_reduction


class MultiTargetBeliefs:
    """Maintains beliefs over all targets.
    
       Attributes:
           location_cov: noise covariance for observed uav locations
           signal_model: signal model provided data likelihood
           target_beliefs: dictionary of individual target beliefs mapped
               to their target ids.
    """
    
    def __init__(self, location_stdev=1.0, signal_model=SignalModel(), 
                 grid=DEFAULT_GRID):
        """Creates a new set of beliefs
        
           Args:
               location_cov: noise covariance for observed uav locations
               signal_model: signal model provided data likelihood
        """
        self.location_stdev = location_stdev
        self.signal_model = signal_model
        self.grid = grid
        self.target_beliefs = dict()
        self.negative_obs = []
        self.ignored_targets = set()

    def remove_target(self, target_id, remove_completely):
        """Removes beliefs about specified target.

           If target is not known, function does nothing.

           Args
           ====
           
           target_id: id of target to forget
           
           remove_completely: completely forget about target if true
               
        """
        if target_id in self.target_beliefs:
            if remove_completely:
                del self.target_beliefs[target_id]
                self.ignored_targets.add(target_id)
            else:
                self.target_beliefs[target_id].removed = True
        
    def observe(self, observations):
        """Updates beliefs based on observations generated by simulator"""
        
        self.negative_obs.extend(
            observations.uav_position[observations.target_id < 0])
        
        # for each observed target
        observed_targets = np.unique(observations.target_id)
        for target in observed_targets:
            #skip negative observations
            if target < 0:
                continue
            
            target_indices = observations.target_id==target
            target_observations = observations[target_indices]
            # if we've never seen this target before, initial beliefs about it
            if target not in self.ignored_targets:
                if target not in self.target_beliefs:
                    self.target_beliefs[target] = \
                    SingleTargetBeliefs(target, target_observations, self.negative_obs, self.location_stdev,
                        self.signal_model, self.grid)
                else:
                    # update our beliefs given observations of this target
                    self.target_beliefs[target].observe(target_observations)

    def expected_area_reduction(self, uav_locations, target_ids=None):
        """Expected Area reduction for all previously observed targets

           Raises an assertion error if areas are requested for unknown
           targets.
        
           Args:
                uav_locations: proposed locations from which to 
                    observe UAV in next round.
                target_ids: (optional)
                    if specified, we only calculate and return results for
                    targets in this list.
                    
            Returns:
                Record array with fields:
                    target_id: id of target
                    area_reduction: expected area reduction from observing
                        target at specified locations.
        """
        # if target ids are not specified, use all known ids
        if target_ids is None:
            target_ids = self.target_beliefs.keys()

        # initialise array to hold results
        result = np.recarray((sum([tid in self.target_beliefs for tid in target_ids]),), dtype=REDUCTION_DTYPE)

        k = 0
        for target in target_ids:
            if target not in self.target_beliefs:
                continue
            beliefs = self.target_beliefs[target]
            reduction = beliefs.expected_area_reduction(uav_locations)
            result.target_id[k] = target
            result.area_reduction[k] = reduction
            k += 1

        return result
    
    def summary(self, include_mode=0):
        """Summarises current beliefs
        
        Arguments
        ---------
        include_mode:
            0: removed tasks are excluded from the summary,
            1: everything is included,
            2: only removed tasks are in the summary
        
        Returns
        -------
        record array with fields
        
        target_id:
            id of target
        mean_position:
            2-D mean location of target
        area:
            estimated size of search area required to find
            target's exact location"""
        if include_mode == 0:
            include_removed = False
            include_others = True
        elif include_mode == 1:
            include_removed = True
            include_others = True
        else:
            include_removed = True
            include_others = False
        removed_len = sum([self.target_beliefs[b].removed for b in self.target_beliefs])
        other_len = sum([not self.target_beliefs[b].removed for b in self.target_beliefs])
        result = np.recarray((include_removed*removed_len + include_others*other_len,), dtype=SUMMARY_DTYPE )

        k = 0
        for target, beliefs in self.target_beliefs.iteritems():
            if beliefs.removed and not include_removed:
                continue
            if not beliefs.removed and not include_others:
                continue
            result.target_id[k] = target
            result.mean_position[k,:] = beliefs.expected_location()
            result.area[k] = beliefs.expected_area()
            k += 1

        return result

def plot_likelihood(beliefs,target,target_loc):
        if target not in beliefs.target_beliefs:
            return
        beliefs = beliefs.target_beliefs[target]
        x = np.linspace(-1000,1000,100)+target_loc[0]
        y = np.linspace(-1000,1000,100)+target_loc[1]
        X, Y = np.meshgrid(x, y)
        X = X.flatten()[:,np.newaxis]
        Y = Y.flatten()[:,np.newaxis]
        loc = np.hstack([X,Y])
        Z = np.exp(beliefs.log_likelihood(loc)+
            beliefs.signal_model.unobserved_log_likelihood(loc, beliefs.negative_obs_locations))
        grid_shape = [100,100]
        X.shape = grid_shape
        Y.shape = grid_shape
        Z.shape = grid_shape
        plt.cla()
        plt.contourf(X,Y,Z,alpha=0.5)
        plt.hold(True)
        plt.plot(beliefs.negative_obs_locations[:,0], beliefs.negative_obs_locations[:,1], 'kx')
        cbar = plt.colorbar()
        plt.xlim([-1000,1000]+target_loc[0])
        plt.ylim([-1000,1000]+target_loc[1])
        plt.grid(True)

def plot_unobserved(origin,uav_locations):
        x = np.linspace(-800,800,50)+origin[0]
        y = np.linspace(-800,800,50)+origin[1]
        X, Y = np.meshgrid(x, y)
        X = X.flatten()[:,np.newaxis]
        Y = Y.flatten()[:,np.newaxis]
        target_locations = np.hstack([X,Y])

        model = SignalModel()
        start_time = time.time()
        Z = model.unobserved_likelihood(target_locations, uav_locations)
        compute_time = time.time()-start_time

        grid_shape = [50,50]
        X.shape = grid_shape
        Y.shape = grid_shape
        Z.shape = grid_shape
        plt.cla()
        plt.contourf(X,Y,Z,alpha=0.5)
        cbar = plt.colorbar()
        plt.grid(True)
        plt.title("Unobserved likelihood")
        plt.hold(True)

        # plot UAV location
        plt.plot(uav_locations[:,0], uav_locations[:,1], 'kx', mew=2.0)
        return compute_time

def plot_particles(beliefs,target,target_loc):
        if target not in beliefs.target_beliefs:
            return
        x = beliefs.target_beliefs[target].particles[:,0]
        y = beliefs.target_beliefs[target].particles[:,1]
        a = beliefs.target_beliefs[target].particles[:,2]
        plt.cla()
        plot_likelihood(beliefs,target,target_loc)
        plt.hold(True)
        plt.scatter(x,y,s=a*4000,c=a,alpha=0.5)
        plt.xlim([-1000,1000]+target_loc[0])
        plt.ylim([-1000,1000]+target_loc[1])
        plt.hold(True)
        plt.plot(target_loc[0],target_loc[1],'k+',mew=2.0)
        text_loc = target_loc + 2
        plt.text(text_loc[0],text_loc[1],'Target %d' % (target,))
        plt.grid(True)

def test_target_removal():

    print "---------------------------------------"
    print "Running target removal test"
    uav_location = np.array([0.0, 0.0, 0.0])
    beliefs = MultiTargetBeliefs()

    # try to remove non existent target
    # should not be in beliefs before or after removing
    if 0 in beliefs.target_beliefs:
        print "FAILED: non-existent target in beliefs"
        return

    beliefs.remove_target(0)

    if 0 in beliefs.target_beliefs:
        print "FAILED: non-existent target in beliefs"
        return

    # observe target 0 then test again
    observations = np.recarray((1,), dtype=OBSERVATION_DTYPE)
    observations.uav_id = np.array([0])
    observations.target_id = np.array([0])
    observations.rssi = np.array([DEFAULT_MIN_SIGNAL])
    observations.uav_position = uav_location
    beliefs.observe(observations)

    # try (again) to remove non existent target
    # should not be in beliefs before or after removing
    if 10 in beliefs.target_beliefs:
        print "FAILED: non-existent target in beliefs"
        return

    beliefs.remove_target(10)

    if 10 in beliefs.target_beliefs:
        print "FAILED: non-existent target in beliefs"
        return

    # try to remove existing target 0
    # should not be in beliefs after removing
    if not 0 in beliefs.target_beliefs:
        print "FAILED: existing target not in beliefs"
        return

    beliefs.remove_target(0)

    if 0 in beliefs.target_beliefs:
        print "FAILED: target 0 was not removed"
        return

    print "OK"
    print "---------------------------------------"
    return


def test_weak_observation():
    """Test for no observations bug fix"""

    print "---------------------------------------"
    print "Running no observations test"
    uav_location = np.array([0.0, 0.0, 0.0])
    beliefs = MultiTargetBeliefs()

    observations = np.recarray((1,), dtype=OBSERVATION_DTYPE)
    observations.uav_id = np.array([0])
    observations.target_id = np.array([0])
    observations.rssi = np.array([DEFAULT_MIN_SIGNAL])
    observations.uav_position = uav_location

    beliefs.observe(observations)

    print beliefs.target_beliefs[0].observations

    # calculate reduction a few extra times as a quick test of 
    # target_id passing
    reduction = beliefs.expected_area_reduction(uav_location,[0,0,0])
    print "reduction (calculated a few times): ", reduction
    print "weak observation test appears to pass."
    print "---------------------------------------"


if __name__ == "__main__":
    # Run unit tests
    test_weak_observation()
    test_target_removal()

    origin = np.array([1500,10000,10])[np.newaxis,:]

    # Run simulation
    uav_locations = np.linspace(-300,50,40)[:,np.newaxis] * np.array([1,1,0]) + \
        origin
    uav_locations[:,0:2] += np.random.randn(40,2)*5
    target_locations = np.array([[-20,15],[5,-5],[0,0],[10,5],[500,800]]) + \
        origin[:,0:2]
    simulator = SensorSim(target_locations)
    beliefs = MultiTargetBeliefs()

    # plot likelihood function
    signal_model = SignalModel()
    distance = np.linspace(1,2*DEFAULT_MAX_DISTANCE-1,500)
    min_signal = signal_model.min_signal
    max_signal = signal_model.median_signal(1.0)
    signal = np.linspace(1.5*min_signal, max_signal, 500)
    mu = signal_model.median_signal(distance)
    plt.subplot(1,2,1)
    plt.plot(distance,mu)
    plt.title("Close Figure to continue.")
    plt.ylabel('signal (dBm)')
    plt.xlabel('distance')
    plt.ylim([1.5*min_signal, max_signal])

    plt.subplot(1,2,2)
    distance, signal = np.meshgrid(distance, signal)
    lik = np.exp(signal_model.log_likelihood(signal,distance))
    plt.contourf(distance,signal,lik)
    cbar = plt.colorbar()
    plt.ylim([min_signal, max_signal])
    plt.title("Likelihood Function.")
    plt.xlabel('distance')
    plt.ion()
    plt.show()
    

    plt.figure(figsize=[20,8])
    plt.ion()
    plt.show()

    # test observing and beliefs
    expected_area_reduction = np.zeros([uav_locations.shape[0]-1,5])
    actual_area = np.zeros([uav_locations.shape[0]-1,5])
    for k in range(0,uav_locations.shape[0]-1):

        uav_id = np.array([10])
        current_location = uav_locations[k,:][np.newaxis,:]
        next_location = uav_locations[k+1,:][np.newaxis,:]
        observations = simulator.generate_observations(current_location, uav_id)
        t=time.time()
        beliefs.observe(observations)
        observe_time = time.time()-t

        # calculate statistics
        t=time.time()
        reduction = beliefs.expected_area_reduction(next_location)
        summary = beliefs.summary()
        summary_time = time.time()-t
        print reduction
        expected_area_reduction[k,reduction.target_id] = reduction.area_reduction
        actual_area[k,summary.target_id] = summary.area
        
        # plot results
        plt.clf()
        for target in range(4):
            idx = (target+1)
            if idx > 2:
                idx +=2
            plt.subplot(2,4,idx)

            # plot particles
            plot_particles(beliefs, target, target_locations[target,:])
            plt.hold(True)

            # plot UAV location
            plt.plot(current_location[0,0], current_location[0,1],
                     'kx', mew=2.0)
            text_loc = current_location + 1
            plt.text(text_loc[0,0], text_loc[0,1], 'UAV')


        # plot negative information plot
        plt.subplot(1,2,2)
        unobserved_time = plot_unobserved(origin[:,0:2].flatten(),
                uav_locations[0:k+1])
        plt.draw()
        
        print "ITERATION: ", k
        print "Observation: ", observations
        print "reduction: ", reduction
        print "summary: ", summary
        print "observation time ", observe_time
        print "reduction + summary time ", summary_time 
        print "unobserved likelihood time ", unobserved_time
        print ""

    plt.show()
    print "ploting area reduction."

    # plot area reduction
    plt.ioff()
    plt.figure()
    plt.subplot(2,2,1)
    plt.plot(actual_area)
    plt.title("Actual Area.")
    plt.ylabel('area')
    plt.xlabel('time step')

    plt.subplot(2,2,2)
    plt.plot(expected_area_reduction)
    plt.title("Expected Area Reductions.")
    plt.ylabel('reduction')
    plt.xlabel('time step')

    plt.subplot(2,2,3)
    actual_area_reduction = actual_area[0:-1,:]-actual_area[1:,:]
    actual_area_reduction[actual_area_reduction<0.0] = 0.0
    plt.plot(actual_area_reduction)
    plt.title("Actual Area Reductions.")
    plt.ylabel('reduction')
    plt.xlabel('time step')

    plt.subplot(2,2,4)
    reduction_error = np.abs(expected_area_reduction[0:-1]-actual_area_reduction)
    reduction_error /= np.abs(actual_area_reduction)
    plt.plot(reduction_error)
    plt.title("Relative Reduction Error.")
    plt.ylabel('reduction')
    plt.xlabel('time step')
    plt.show()
    print "Done."


