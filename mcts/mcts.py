from __future__ import print_function

import random
import time
from . import utils


class MCTS(object):
    """
    The central MCTS class, which performs the tree search. It gets a
    tree policy, a default policy, and a backup strategy.
    See e.g. Browne et al. (2012) for a survey on monte carlo tree search
    CHANGE by Zoltan Beck 27/10/2015: tree growth can be stopped at a given time. Example:
        mcts = MCTS(tree_policy, default_policy, backup)
        best_action = mcts(root, stoptime = time.time()+20)
        #this will run the tree search for 20 seconds from now
    CHANGE by Zoltan Beck 01/11/2015: best_sequence function added. Returns a sequence of actions till there are no children
    CHANGE by Zoltan Beck 02/11/2015: expansion_size added to tree search, when new nodes added, this number of elements are added
    """
    def __init__(self, tree_policy, default_policy, backup):
        self.tree_policy = tree_policy
        self.default_policy = default_policy
        self.backup = backup

    def __call__(self, root, n=1500, stoptime=None, expansion_size = 1, debug = False):
        """
        Run the monte carlo tree search.

        :param root: The StateNode
        :param n: The number of roll-outs to be performed
        :param stoptime: The time the tree growth can take in seconds (if given n is ignored)
        :return: best action
        """
        if root.parent is not None:
            raise ValueError("Root's parent must be None.")
            
        if debug:
            perc5 = int(n/20)
            start = time.time()

        if stoptime == None:
            for i in range(n):
                if debug and i%perc5 == 0:
                    plen = len(self.best_sequence(root))
                    print('%2d%%: %5d long - %5ds' % (5*i/perc5,plen,time.time()-start))#, end="\r")
                nodes = _get_next_node(root, self.tree_policy, expansion_size)
                for node in nodes:
                    node.reward = self.default_policy(node)
                    self.backup(node)
        else:
            i=0
            stoptime = time.time()+stoptime
            while time.time() < stoptime:
                i+=expansion_size
                nodes = _get_next_node(root, self.tree_policy, expansion_size)
                for node in nodes:
                    node.reward = self.default_policy(node)
                    self.backup(node)
            print (i,)

        return utils.rand_max(root.children.values(), key=lambda x: x.q).action
        
    def best_sequence(self, root):
        seq = []
        current = root
        while True:
            #stop finding best action if not fully explored
            if current.untried_actions:
                break
            actionNode = utils.rand_max(current.children.values(), key=lambda x: x.q)
            seq.append(actionNode.action)
            if len(actionNode.children) > 0:
                current = actionNode.sample_state()
        return seq

def _expand(state_node, expansion_size=1):
    if expansion_size > 1:
        actions = random.sample(state_node.untried_actions, expansion_size)
    else:
        actions = [random.choice(state_node.untried_actions)]
    return [state_node.children[action].sample_state() for action in actions]


def _best_child(state_node, tree_policy):
    best_action_node = utils.rand_max(state_node.children.values(),
                                      key=tree_policy)
    return best_action_node.sample_state()


def _get_next_node(state_node, tree_policy, expansion_size=1):
    while not state_node.state.is_terminal():
        if state_node.untried_actions:
            return _expand(state_node)
        else:
            state_node = _best_child(state_node, tree_policy)
    return [state_node]
