# -*- coding: utf-8 -*-
"""
Created on Sat Apr 04 20:26:26 2015

@author: Zoli
"""
import numpy as np
from ProblemFactory import ProblemFactory, Density
from agent import CommFactory, CommAgent, AgentState, Attitude
from constants import RW_STAY_TIME, RW_MAX_SPEED
import time
import threading
import sys

def resqr_process(ID, agentStates, bcast_addr, reply_addr, context=None, gradient_txt='', debug=False):
    astates = AgentState.fromstring(agentStates, context)
    gradient = Density.fromstring(gradient_txt)
    context = astates[0].context
    resqr = ResQr(ID, astates, bcast_addr, reply_addr, context, gradient, debug)
    resqr.initSockets()
#    print('sockets open')
#    cntr = 0
    while resqr.run:
        time.sleep(0.1)
#        if resqr.plot_solution_signal.acquire(False):
#            resqr.latest_problem.plotSolution(resqr.latest_solution)
        

class ResQr(CommAgent):
    def __init__(self, ID, agentStates, bcast_addr, reply_addr, context=None, gradient=None, debug=False):
        CommAgent.__init__(self, agentStates[ID], bcast_addr, reply_addr,
            ['stop', 'init_comm_search', 'init_comm_manvre', 'delete_rescue_task', 'add_task',
            'update_tasks', 'update_density', 'update_resq_attitudes', 'decide_resq'], context, debug)
        self.INSTRUCTION_TASK_BEST_NUMBER = 3
        self.attitudes = [a.attitude for a in agentStates]
        self.pf = None
        self.nagents = len(agentStates)
        self.id = ID
        self.agentStates = []
        for i in range(len(agentStates)):
            self.agentStates.append(AgentState.copy(agentStates[i]))
        self.state = self.agentStates[ID]
        self.agentStates[ID] = None
        self.instructionLock = threading.RLock()
        self.agent_instr_dir = []
        self.agent_instr_task_raw = []
        self.agent_instr_task_best = []
        self.self_instr_task_dict = dict()
        self.self_instr_nsamples = 0
        self.samples_processed = 0
        self.feedback_for_search = []
        self.feedback_for_manouvre = []
        self.feedback_for_search_processed = np.array([])
        self.feedback_for_manouvre_processed = np.array([])
        self.search_states = []
        self.use_feedback = False
        self.resetInstructions()
        self.fix_tasks = []
        self.density = None
        self.gradient = gradient
        self.tasks_updated = False
        self.density_updated = False
#        self.latest_problem = None
#        self.latest_solution = None
#        self.plot_solution_signal = threading.Semaphore(0)
        
    def initSockets(self):
        for st in self.agentStates:
            if st:
                st.connectSocket()
                
    def initSearchComm(self, search_states):
        self.search_states = search_states
        self.use_feedback = (len(self.search_states) != 0)
        for st in self.search_states:
            st.connectSocket()
            
    def initManvreComm(self, manvre_states):
        self.manvre_states = manvre_states
        for st in self.manvre_states:
            st.connectSocket()
        
    def resetInstructions(self):
        with self.instructionLock:
            self.agent_instr_dir = np.array([[0,0]]*self.nagents, np.float)
            self.agent_instr_task_raw = [[] for i in range(self.nagents)]
            self.agent_instr_task_best = np.array([[[-1,0]]*self.INSTRUCTION_TASK_BEST_NUMBER]*self.nagents, np.int)
            self.self_instr_task_dict = dict()
            self.self_instr_nsamples = 0
            self.feedback_for_search = []
            self.feedback_for_manouvre = []
        self.samples_processed = 0
            
    def processInstructions(self, usefeedback):
        with self.instructionLock:
            for a in range(self.nagents):
                taskCount = np.bincount(self.agent_instr_task_raw[a])
                taskno = min(len(taskCount), self.INSTRUCTION_TASK_BEST_NUMBER)
                bestID = np.argpartition(taskCount, -taskno)[-taskno:]
                if a==self.id:
                    for bid in bestID:
                        if bid in self.self_instr_task_dict:
                            self.self_instr_task_dict[bid] += taskCount[bid]
                        else:
                            self.self_instr_task_dict[bid] = taskCount[bid]
                else:
                    for i in range(taskno):
                        self.agent_instr_task_best[a][i] = [bestID[i], taskCount[bestID[i]]];
            self.self_instr_nsamples += self.samples_processed
            if usefeedback:
                if len(self.feedback_for_manouvre) > 0:
                    self.feedback_for_manouvre_processed = np.concatenate(self.feedback_for_manouvre)
                if len(self.feedback_for_search) > 0:
                    self.feedback_for_search_processed = np.concatenate(self.feedback_for_search)
        
    def solveSample(self, usefeedback, debug_plot=False):
        problem = self.pf.getProblem(self.id)
        if self.debug:
            print(problem)
        vec, taskID, schedule = problem.getGreedyInstructions()
        if usefeedback and len(problem.tasks) > 0:
            tasks = problem.tasks[:,:2]
            execTimeww = problem.getLowToHighFeedBackData(schedule)
            fullData = np.concatenate((tasks, execTimeww), axis = 1)
            with self.instructionLock:
                self.feedback_for_manouvre.append(fullData[:,[0,1,-1]])
                self.feedback_for_search.append(fullData[problem.nFixedTasks:])
        if self.debug:
            print('Problem solved.')
        with self.instructionLock:
            self.samples_processed += 1
            self.agent_instr_dir += vec
            for a in range(self.nagents):
                if taskID[a] >= 0:
                    self.agent_instr_task_raw[a].append(taskID[a])
        return problem, schedule
        
    def mainCycleFixedSample(self, nsample, overall_sample):        
        self.resetInstructions()
        if self.debug:
            print('Cycle starts (%d/%d samples)...' % (nsample, overall_sample))
        for i in range(nsample):
            t = time.time()
#            self.latest_problem, self.latest_solution = 
            self.solveSample(self.use_feedback)
#            if i == nsample-1 and self.id == 0:
#                self.plot_solution_signal.release()
            dt = time.time()-t
            if dt > 2:
                print('%.2f-%d %s' % (dt, self.id, time.asctime()))
        if self.debug:
            print('...cycle finished, '+str(self.samples_processed)+' samples processed')
        self.processInstructions(self.use_feedback)
        #send instructions to other agetns if necessary
        if nsample < overall_sample:
            self.pushInstructions(self.use_feedback)
        
        connErrTime = 10.0
        while self.self_instr_nsamples < overall_sample:
            if connErrTime < 0:
                print('Connection timeout!%d %d/%d %s' % (self.id, self.self_instr_nsamples, overall_sample, time.asctime()))
                break
            time.sleep(0.1)
            connErrTime -= 0.1
        target = self.determineNextTarget()
        if self.debug:
            print('Next target:')
            print(target)
        return target
    
    def retrieveAgentInstruction(self, ID):
        assert(ID != self.id)
        return self.agent_instr_dir[ID], self.agent_instr_task_best[ID], self.samples_processed
    
    def retrieveSelfInstruction(self):
        vect = self.agent_instr_dir[self.id]
        if not self.self_instr_task_dict:
            assigned_task = -1
        else:
            best_task_assigned = max(self.self_instr_task_dict.values())
            if self.self_instr_nsamples == 0:
                assigned_task = -1
            elif best_task_assigned >= (self.self_instr_nsamples/2):
                assigned_task = [key for key, val in self.self_instr_task_dict.items()
                    if val == best_task_assigned][0]
            else:
                assigned_task = -1
        return vect, assigned_task
        
    def pushInstructions(self, usefeedback):
        for a in range(self.nagents):
            if self.agentStates[a]:    #for other agents only
                vec, ids, nsample = self.retrieveAgentInstruction(a)
                self.agentStates[a].send(vec.tostring() +
                    ids.tostring() +
                    np.array([nsample], np.int).tostring(),
                    CommFactory.msg_id['resq2resq'])
        if usefeedback:
            for st in self.search_states:
                st.send(self.feedback_for_search_processed, CommFactory.msg_id['resq2search'])
        for st in self.manvre_states:
            st.send(self.feedback_for_manouvre_processed, CommFactory.msg_id['manvre_critical'])
        
    def determineNextTarget(self):
        vec, taskID = self.retrieveSelfInstruction()
        if self.debug:
            print('Final intruction:')
            print(vec)
            print(taskID)
        if taskID < 0:
            target = self.pf.makeTargetFromVector(vec, self.id)
        else:
            target = self.pf.makeTargetFromTask(taskID)
        return target
        
    def processmessage(self, conn, data, msg_type):
        if msg_type == CommFactory.msg_id['resq2resq']:
            self.process_rescue_message(data)
        elif msg_type == CommFactory.msg_id['search2resq']:
            self.process_search_plan_message(data)
        elif msg_type == CommFactory.bcast_topics['decide_resq']:
            conn.send(CommFactory.msg_id['busy']+chr(ord('a')+self.id))
            args = np.fromstring(data, np.int, 2)
            target = self.mainCycleFixedSample(args[0], args[1])
            conn.send(CommFactory.bcast_topics['decide_resq'] +
#                np.array([self.id],np.int).tostring() + target.tostring())
                np.array([self.id],np.int).tostring() + np.concatenate((target.flatten(),[time.time()])).tostring())
        elif msg_type == CommFactory.bcast_topics['delete_rescue_task']:
            self.remove_task(np.fromstring(data, np.float))
        elif msg_type == CommFactory.bcast_topics['update_resq_attitudes']:
            self.attitudes = Attitude.fromstring(data)
            if self.attitudes[self.id].batteryTime <= 0:
                self.commstop()
            self.update_agent_pos(self.attitudes)
        elif msg_type == CommFactory.bcast_topics['add_task']:
            self.add_task(np.fromstring(data, np.float))
        elif msg_type == CommFactory.bcast_topics['update_tasks']:
            self.fix_tasks = np.fromstring(data).reshape(-1,3)
            self.tasks_updated = True
        elif msg_type == CommFactory.bcast_topics['update_density']:
            self.density = Density.fromstring(data)
            self.density_updated = True
        elif msg_type == CommFactory.bcast_topics['init_comm_search']:
            search_states = AgentState.fromstring(data, self.context)
            self.initSearchComm(search_states)
        elif msg_type == CommFactory.bcast_topics['init_comm_manvre']:
            manvre_states = AgentState.fromstring(data, self.context)
            self.initManvreComm(manvre_states)
                        
        if self.tasks_updated and self.density_updated:
            self.update_problem(self.fix_tasks, self.density)
            self.tasks_updated = False
            self.density_updated = False
            
    def process_search_plan_message(self, data):
        if self.debug:
            print('Recieved ', len(data))
        discovery = np.fromstring(data, np.float)
        discovery = discovery.reshape(len(discovery)/3, 3)
        if self.debug:
            print('New search plan recieved:')
            print(discovery)
        self.pf.setDiscoveryActivation(discovery)
        
    def process_rescue_message(self, data):
        vec = np.fromstring(data, np.float, 2)
        firstlen = len(vec.tostring())
        ids = np.fromstring(data[firstlen:], np.int, 6).reshape(3,2)
        seclen = len(ids.tostring())
        nsample = np.fromstring(data[(firstlen+seclen):], np.int, 1)
        if self.debug:        
            #print(data)
            print('v:')
            print(vec)
            print('ids')
            print(ids)
            print('samples: '+str(nsample))
        with self.instructionLock:
            #print('\t\tunlock \tunlock \tlock 251')
            self.agent_instr_dir[self.id] += vec
            for pair in ids:
                if pair[0] in self.self_instr_task_dict:
                    self.self_instr_task_dict[pair[0]] += pair[1]
                else:
                    self.self_instr_task_dict[pair[0]] = pair[1]
            self.self_instr_nsamples += nsample
            #print('\t\tunlock \tunlock \tunlock 251')

    def remove_task(self, task):
        return self.pf.remove_task(task)
        
    def add_task(self, task):
        self.pf.add_task(task)
        
    def update_problem(self, tasks, density):
        self.pf = ProblemFactory(self.attitudes, density, RW_STAY_TIME,
                                 RW_MAX_SPEED, tasks, self.gradient)
        
    def update_agent_pos(self, attitudes):
        if self.pf:
            self.pf.agents = attitudes
            

if __name__ == "__main__":
    testID = int(sys.argv[1])
    #rq = ResQr(0, [AgentState(Attitude([0,0], 0, 1), 'localhost', 8888)], [[1, 1],[0, 1]])
    states = [AgentState(Attitude([2,1], [0,0], 100), 'localhost', 8888),
              AgentState(Attitude([6,3], [0,0], 100), 'localhost', 8889)]
    
    time.sleep(2)
    tasks = []
    testcase = 2
    if testcase == 0:
        tasks = [[3,3], [3.9,2], [5,1], [7,1], [9,1]]
    elif testcase == 1:
        density[0][2] = 1
        density[8][3] = 1
    elif testcase == 2:
        density[0][2] = 1
        tasks = [[7,1], [9,1]]
    rq = ResQr(testID, states, density, tasks, True)
    rq.initSockets()
    rq.mainCycle(5,2)
    rq.commstop()
    time.sleep(5)
        